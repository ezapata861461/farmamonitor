import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';
import * as _ from "lodash";

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { EmpleadoAuthoService } from "../shared/empleado-autho.service";
import { EmpleadoService } from "../shared/empleado.service";

import { ErrorLogService } from '../../core/shared/error-log.service';
import { NotifyService } from '../../core/shared/notify.service';
import { LoadingService } from '../../core/shared/loading.service';
import { AuthService } from '../../auth/shared/auth.service';

@Component({
  selector: 'win-empleado-form',
  templateUrl: './empleado-form.component.html',
  styleUrls: ['./empleado-form.component.scss']
})
export class EmpleadoFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'empleados';

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  // Form parts show/hide flags
  parts = {
    instancias: false,
    registro: false
  }

  constructor(
    protected router: Router,
    public mainService: EmpleadoService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    protected afs: AngularFirestore,
    protected fb: FormBuilder,
    protected auth: AuthService,
    public autho: EmpleadoAuthoService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = (this.autho.canCreate && '0' === this.mainService.id) || (this.autho.canEdit && '0' !== this.mainService.id);
    this.isSuperUser = this.autho.hasRol(['webmaster', 'admin']);
    this.isWebmaster = this.autho.hasRol(['webmaster']);

    // Get form config and hide/show form parts
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        const conf = config.modulos['empleados'].formParts;
        this.parts = {
          instancias: this.isWebmaster || conf.instancias,
          registro: this.isWebmaster || conf.registro
        }
      })
    )

    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      apellido: [{ value: '', disabled: !this.canCreateOrEdit }],
      nombre: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required,
        Validators.minLength(3),
      ]],
      fCreado: [{ value: null, disabled: !this.isSuperUser }],
      creadorId: [{ value: '', disabled: !this.isSuperUser }],
      fModificado: [{ value: null, disabled: !this.isSuperUser }],
      modificadorId: [{ value: '', disabled: !this.isSuperUser }],
      instancia: [{ value: '', disabled: !this.isSuperUser }],
      instancias: this.fb.array([])
    });
  }

  /**
   * Load controls
   */
  loadControls(): void {}

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.apellido = data.apellido;
      this.nombre = data.nombre;

      this.fCreado = data.fCreado;
      this.creadorId = data.creadorId;
      this.fModificado = data.fModificado;
      this.modificadorId = data.modificadorId;
      
      this.instancia = data.instancia;

      this.instancias.clear();
      if (data.instancias && data.instancias.length > 0) {
        data.instancias.forEach(element => {
          this.addInstancia(element);
        });
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get apellido() {
    return this.form.get('apellido');
  }

  set apellido(apellido) {
    this.apellido.setValue(apellido);
  }

  get nombre() {
    return this.form.get('nombre');
  }

  set nombre(nombre) {
    this.nombre.setValue(nombre);
  }

  get instancia() {
    return this.form.get('instancia');
  }

  set instancia(instancia) {
    this.instancia.setValue(instancia);
  }

  get instancias(): FormArray {
    return this.form.get('instancias') as FormArray;
  }

  addInstancia(instancia = '') {
    const element = this.fb.group({
      instancia: [instancia, [
        Validators.required
      ]]
    });
    this.instancias.push(element);
  }

  deleteInstancia(i) {
    this.instancias.removeAt(i);
    this.form.markAsTouched();
  }
}
