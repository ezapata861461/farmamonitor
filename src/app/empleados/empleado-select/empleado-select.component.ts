import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, Output, EventEmitter, Optional, Self, OnDestroy } from '@angular/core';
import { FormBuilder, NgControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldControl } from '@angular/material/form-field';

import { AbsSelectComponent } from '../../core/abstract/abs-select.component';
import { EmpleadoGridComponent } from '../empleado-grid/empleado-grid.component';
import { EmpleadoService } from '../shared/empleado.service';
import { EmpleadoId } from '../shared/empleado';

@Component({
  selector: 'win-empleado-select',
  templateUrl: './empleado-select.component.html',
  styleUrls: ['./empleado-select.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: EmpleadoSelectComponent }],
  host: {
    '[class.label-floating]': 'shouldLabelFloat',
    '[id]': 'id',
    '[attr.aria-describedby]': 'describedBy',
  }
})
export class EmpleadoSelectComponent extends AbsSelectComponent implements OnDestroy {

  // "description" field name (to extract it from data)
  descrFieldName = 'nombreCompleto';
  // Current selected document
  document: EmpleadoId;

  // Emited events
  @Output() load = new EventEmitter<EmpleadoId>();
  @Output() select = new EventEmitter<EmpleadoId>();

  // Custom input type and id
  controlType = 'win-empleado-select';
  id = `win-empleado-select-${EmpleadoSelectComponent.nextId++}`;

  constructor(
    fb: FormBuilder,
    protected mainService: EmpleadoService,
    protected fm: FocusMonitor,
    protected elRef: ElementRef<HTMLElement>,
    public dialog: MatDialog,
    @Optional() @Self() public ngControl: NgControl,
  ) {
    super(fb, mainService, fm, elRef, ngControl);
  }

  /**
   * Open select popup and populate fields on select
   */
  openSelect() {
    const dialog = this.dialog.open(EmpleadoGridComponent, {data: {}, width: '80%', height: '80%'});
    dialog.componentInstance.mode = 'select';
    dialog.componentInstance.isPopup = true;
    dialog.disableClose = true;
    this.subscriptions.push(
      this.dialogCloseSub = dialog.componentInstance.close.subscribe(() => { dialog.close() })
    );

    this.subscriptions.push(
      this.dialogSub = dialog.componentInstance.select.subscribe(document => {
        // Keep selected document
        this.document = document;
  
        // Load data into form fields
        this.inputId = document.id;
        this.inputDescr = document[this.descrFieldName];
  
        // Emit selected document
        this.select.emit(document);
  
        // Notify change and run change detection
        this.onChange(document.id);
        this.stateChanges.next();
        
        // Close select dialog
        dialog.close();
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
