import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmpleadosComponent } from "./empleados.component";
import { EmpleadoListComponent } from "./empleado-list/empleado-list.component";
import { EmpleadoComponent } from "./empleado/empleado.component";
import { EmpleadoDetailsComponent } from "./empleado-details/empleado-details.component";

import { RoleGuard } from './shared/role.guard';

const routes: Routes = [
  {
    path: '',
    component: EmpleadosComponent,
    canActivate: [RoleGuard],
    children: [
      {
        path: '',
        component: EmpleadoListComponent,
      },
      {
        path: ':id',
        component: EmpleadoComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles',
          },
          {
            path: 'detalles',
            component: EmpleadoDetailsComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpleadosRoutingModule { }
