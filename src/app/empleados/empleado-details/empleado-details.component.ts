import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { EmpleadoAuthoService } from "../shared/empleado-autho.service";
import { AuthService } from 'src/app/auth/shared/auth.service';

@Component({
  selector: 'app-empleado-details',
  templateUrl: './empleado-details.component.html',
  styleUrls: ['./empleado-details.component.scss']
})
export class EmpleadoDetailsComponent {

  // Route slug
  slug: string = 'empleados';

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: EmpleadoAuthoService,
  ) { }

  /**
   * On form creation event
   * @param $event 
   */
  onCreated($event) {
    this.router.navigate([this.auth.instancia, this.slug, $event]);
  }

  /**
   * On form update event
   * @param $event 
   */
  onUpdated($event) {}

  /**
   * On form delete event
   * @param $event 
   */
  onDeleted() {
    this.router.navigate([this.auth.instancia, this.slug]);
  }
}
