import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule } from "@angular/forms";

import { EmpleadosRoutingModule } from './empleados-routing.module';
import { EmpleadosComponent } from './empleados.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { EmpleadoDetailsComponent } from './empleado-details/empleado-details.component';
import { EmpleadoFormComponent } from './empleado-form/empleado-form.component';
import { EmpleadoGridComponent } from './empleado-grid/empleado-grid.component';
import { EmpleadoListComponent } from './empleado-list/empleado-list.component';
import { EmpleadoSelectComponent } from './empleado-select/empleado-select.component';

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { InstanciasModule } from "../instancias/instancias.module";

import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { UsuarioGridComponent } from '../usuarios/usuario-grid/usuario-grid.component';
import { InstanciaGridComponent } from "../instancias/instancia-grid/instancia-grid.component";

@NgModule({
  declarations: [
    EmpleadosComponent, 
    EmpleadoComponent, 
    EmpleadoDetailsComponent, 
    EmpleadoFormComponent, 
    EmpleadoGridComponent, 
    EmpleadoListComponent, 
    EmpleadoSelectComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    EmpleadosRoutingModule,
    CoreModule,
    SharedModule,
    AuthModule,
    UsuariosModule,
    InstanciasModule
  ],
  exports: [
    EmpleadoGridComponent,
    EmpleadoFormComponent,
    EmpleadoSelectComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    UsuarioGridComponent,
    InstanciaGridComponent
  ]
})
export class EmpleadosModule { }
