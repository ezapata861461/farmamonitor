import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth/shared/auth.service';
import { EmpleadoAuthoService } from "../shared/empleado-autho.service";
import { LoadingService } from "../../core/shared/loading.service";

@Component({
  selector: 'app-empleado-list',
  templateUrl: './empleado-list.component.html',
  styleUrls: ['./empleado-list.component.scss']
})
export class EmpleadoListComponent {

  // Route slug
  slug: string = 'empleados';

  // Sidebar menu
  sidebar = [
    { name: 'Escritorio', icon: 'arrow_back', link: '../admin', disabled: false },
    { name: 'Lista de Empleados', icon: 'face', link: './' , disabled: false }
  ]

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: EmpleadoAuthoService,
    private loading: LoadingService
  ) { }

  /**
   * Grid create event handler
   * @param $event 
   */
  onCreate() {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, 0]);
  }

  /**
   * Grid row click event handler
   * @param $event 
   */
  onSelect($event) {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, $event.id]);
  }
}
