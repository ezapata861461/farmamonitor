import { TestBed } from '@angular/core/testing';

import { EmpleadoAuthoService } from './empleado-autho.service';

describe('EmpleadoAuthoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmpleadoAuthoService = TestBed.get(EmpleadoAuthoService);
    expect(service).toBeTruthy();
  });
});
