import { map, take, debounceTime } from "rxjs/operators";
import { FormGroup, AbstractControl } from "@angular/forms";
import { AngularFirestore } from "@angular/fire/firestore";

import { AuthService } from 'src/app/auth/shared/auth.service';
import { EmpleadoId } from "../shared/empleado";

/**
 * Validates 'empleado' is unique by email
 */
export class EmpleadoExistsValidator {
  static email(auth: AuthService, afs: AngularFirestore, form: FormGroup) {
    return (control: AbstractControl) => {
      const email = control.value.toLowerCase();
      /**
       * @todo: Change by unique field
       */
      return afs.collection('empleados', ref => ref.where('email', '==', email).where('instancias', 'array-contains', auth.instancia))
        .snapshotChanges().pipe(
          debounceTime(500),
          take(1),
          map(items => {
            let data = items.map(a => {
              const data = a.payload.doc.data() as EmpleadoId;
              const id = a.payload.doc.id;
              return { id, ...data };
            });

            if (data.length && '' !== form.value.email && (0 === form.value.id || data[0].id !== form.value.id)) {
              return { empleadoExists: data[0].nombreCompleto };
            }
            return null;
          })
        );
    }
  }
}