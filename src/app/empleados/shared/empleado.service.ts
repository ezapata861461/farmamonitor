import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { Empleado, EmpleadoId } from "./empleado";

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService extends AbsFirestoreService implements OnDestroy {

  // Collection prefix
  collectionPrefix = '';
  // Collection name
  collectionName = 'empleados';
  // Current loaded collection
  col$: Observable<EmpleadoId[]>;
  // Current loaded document
  doc$: BehaviorSubject<EmpleadoId | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
  ) {
    super(afs, auth);

    // Init current collection and current object
    this.initCol();
    this.initDoc();
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as EmpleadoId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<EmpleadoId>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): EmpleadoId {
    return {
      id: '0',
      apellido: '',
      nombre: '',
      nombreCompleto: '',
      fCreado: null,
      creadorId: '',
      fModificado: null,
      modificadorId: '',
      instancia: this.auth.instancia,
      instancias: [ this.auth.instancia ],
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): Empleado {
    let data: Empleado;
    data = {
      apellido: (fd.apellido) ? _.trim(fd.apellido) : '',
      nombre: (fd.nombre) ? _.trim(fd.nombre) : '',
      nombreCompleto: ((fd.nombre) ? _.trim(fd.nombre) : '') +' '+ ((fd.apellido) ? _.trim(fd.apellido) : ''),
      fCreado: (fd.fCreado && '' !== fd.fCreado) ? fd.fCreado : this.timestamp,
      creadorId: (fd.creadorId && '' !== fd.creadorId) ? fd.creadorId : this.auth.getUid(),
      fModificado: this.timestamp,
      modificadorId: this.auth.getUid(),
      instancia: (fd.instancia) ? fd.instancia : this.auth.instancia,
      instancias: (fd.instancias && '' !== fd.instancias && 0 < fd.instancias.length) ? fd.instancias.map(el => { el = el.instancia; return el; }) : [ this.auth.instancia ]
    }

    return data;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
