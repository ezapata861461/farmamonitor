import { OwnerAwareInterface } from 'src/app/shared/interface/ownerAwareInterface';

/**
 * Representa un empleado de la empresa
 */
export interface Empleado extends OwnerAwareInterface {
  /** Apellidos de la persona. */
  apellido: string;
  /** Nombre de la persona. */
  nombre: string;
  /** Nombre y apellido concatenados (busquedas de texto full.) */
  nombreCompleto: string;
}

export interface EmpleadoId extends Empleado {
  id: string;
}