import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

import { AbsGridComponent } from "../../core/abstract/abs-grid.component";
import { ConfirmDialogComponent } from "../../core/confirm-dialog/confirm-dialog.component";

import { EmpleadoAuthoService } from "../shared/empleado-autho.service";
import { EmpleadoService } from "../shared/empleado.service";
import { EmpleadoId } from "../shared/empleado";
import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'win-empleado-grid',
  templateUrl: './empleado-grid.component.html',
  styleUrls: ['./empleado-grid.component.scss']
})
export class EmpleadoGridComponent extends AbsGridComponent implements OnInit, OnDestroy {

  // Mat table definition
  dataSource: MatTableDataSource<EmpleadoId>;
  selection = new SelectionModel<EmpleadoId>(true, []);

  // Selected ítems emmiter
  @Output() 
  select = new EventEmitter<EmpleadoId[] | EmpleadoId>();

  constructor(
    public mainService: EmpleadoService,
    public autho: EmpleadoAuthoService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService
  ) {
    super(mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    super.ngOnInit();

    // Mat table column definition
    this.columns = [ 'mobileView', 'nombre' ];

    // Setup main service
    this.mainService.pageSize = 50;
    if (null === this.mainService.sort) { this.mainService.sort = 'nombreCompleto' }
    if (null === this.mainService.sortDir) { this.mainService.sortDir = 'asc' }
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  /**
   * Set grid filter
   * @param filter 
   */
  setFilter(filter) {
    if (undefined !== filter) {
      this.mainService.filterColBy('empresaId', filter);
    }
  }

  /**
   * Delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmDialogComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar estas empleados?'},
    });

    this.subscriptions.push(
      confirm.afterClosed().subscribe(result => {
        if (result) {
          this.onLoading();
          this.mainService.deleteCollection(this.selection.selected)
            .then(x => {
              this.selection.clear();
              this.onLoaded('Borrado!');
              this.deleted.emit(true);
            })
            .catch(error => {
              this.onError(error, 'No fue posible borrar. Por favor intente de nuevo.');
              this.onLoaded();
            });
        }
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
