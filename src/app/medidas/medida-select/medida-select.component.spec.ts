import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedidaSelectComponent } from './medida-select.component';

describe('MedidaSelectComponent', () => {
  let component: MedidaSelectComponent;
  let fixture: ComponentFixture<MedidaSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedidaSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedidaSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
