import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedidaGridComponent } from './medida-grid.component';

describe('MedidaGridComponent', () => {
  let component: MedidaGridComponent;
  let fixture: ComponentFixture<MedidaGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedidaGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedidaGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
