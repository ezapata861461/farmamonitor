/**representa un equipo instalado en alguna parte */

export interface Medida { 
    /**seraila del procesador del equipo*/
    serial: string;   
}

export interface MedidaId extends Medida {
    id: string;
}
