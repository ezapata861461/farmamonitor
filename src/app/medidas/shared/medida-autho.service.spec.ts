import { TestBed } from '@angular/core/testing';

import { MedidaAuthoService } from './medida-autho.service';

describe('MedidaAuthoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MedidaAuthoService = TestBed.get(MedidaAuthoService);
    expect(service).toBeTruthy();
  });
});
