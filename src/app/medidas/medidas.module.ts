import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** componentes utilizable en cualquier componente */
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule }      from "@angular/forms";

/** */
import { MedidasRoutingModule } from './medidas-routing.module';
import { MedidasComponent } from './medidas.component';
import { MedidaComponent } from './medida/medida.component';

/** componentes de otros modulos, abstrat, para manejo de usu,auth,instacias */

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { InstanciasModule } from '../instancias/instancias.module';

/**estas tambien */
import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { UsuarioGridComponent } from '../usuarios/usuario-grid/usuario-grid.component';
import { InstanciaGridComponent } from '../instancias/instancia-grid/instancia-grid.component';

import { MedidaDetailsComponent } from './medida-details/medida-details.component';
import { MedidaFormComponent } from './medida-form/medida-form.component';
import { MedidaGridComponent } from './medida-grid/medida-grid.component';
import { MedidaListComponent } from './medida-list/medida-list.component';
import { MedidaSelectComponent } from './medida-select/medida-select.component';


@NgModule({
  declarations: [MedidasComponent, MedidaComponent, MedidaDetailsComponent, MedidaFormComponent, MedidaGridComponent, MedidaListComponent, MedidaSelectComponent],
  imports: [
    CommonModule,
    MedidasRoutingModule,
 /**desde aca */
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    CoreModule,
    SharedModule,
    AuthModule,
    UsuariosModule,
    InstanciasModule
  ],
  exports:[
  
  ],

  /** componentes de entrada */
  
  entryComponents: [
    ConfirmDialogComponent,
    UsuarioGridComponent,
    InstanciaGridComponent
  ]
})
export class MedidasModule { }
