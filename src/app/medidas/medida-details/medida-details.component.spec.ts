import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedidaDetailsComponent } from './medida-details.component';

describe('MedidaDetailsComponent', () => {
  let component: MedidaDetailsComponent;
  let fixture: ComponentFixture<MedidaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedidaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedidaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
