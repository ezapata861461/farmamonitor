# Empresas Module

Empresas module for Angular App.

## Install

Include module into an angular app as a subtree like this (Must be in app root folder. Right outside src folder.):

```
git remote add empresas https://JabbarSahid@bitbucket.org/webintegral/empresas.git
git subtree add --prefix=src/app/empresas empresas master
```

## Pull or Push Changes from/to Repository

After a commit, push changes to this repository like this:

```
git subtree push --prefix=src/app/empresas empresas master
```

To pull changes to local app, use:

```
git subtree pull --prefix=src/app/empresas empresas master
```

To push changes in parent project, just do it as usual:

```
git push -u origin master
```