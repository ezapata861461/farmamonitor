import { map, take, debounceTime } from "rxjs/operators";
import { FormGroup, AbstractControl } from "@angular/forms";
import { AngularFirestore } from "@angular/fire/firestore";

import { AuthService } from 'src/app/auth/shared/auth.service';
import { EmpresaId } from "../shared/empresa";

/**
 * Validates 'empresa' is unique by 'documento'
 */
export class EmpresaExistsValidator {
  static documento(auth: AuthService, afs: AngularFirestore, form: FormGroup) {
    return (control: AbstractControl) => {
      const documento = control.value.replace(/[^\d\-]/g,'');
      return afs.collection('empresas', ref => ref.where('documento', '==', documento).where('instancias', 'array-contains', auth.instancia))
        .snapshotChanges().pipe(
          debounceTime(500),
          take(1),
          map(items => {
            let data = items.map(a => {
              const data = a.payload.doc.data() as EmpresaId;
              const id = a.payload.doc.id;
              return { id, ...data };
            });
            if (data.length && (0 === form.value.id || data[0].id !== form.value.id)) {
              return { empresaExists: data[0].razonSocial };
            }
            return null;
          })
        );
    }
  }
}
