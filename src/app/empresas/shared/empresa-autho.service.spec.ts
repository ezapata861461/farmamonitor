import { TestBed } from '@angular/core/testing';

import { EmpresaAuthoService } from './empresa-autho.service';

describe('AuthorizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmpresaAuthoService = TestBed.get(EmpresaAuthoService);
    expect(service).toBeTruthy();
  });
});
