import { OwnerAwareInterface } from '../../shared/interface/ownerAwareInterface';
import { Telefono } from '../../shared/interface/telefono';

/**
 * Representa una empresa de la aplicación
 */
export interface Empresa extends OwnerAwareInterface {
  /** Ciudad donde está ubicada la empresa. */
  ciudad: string;
  /** Dirección (nomenclatura) de la empresa. */
  direccion: string;
  /** Número de documento de la empresa. */
  documento: string;
  /** @deprecated Email principal de la empresa (Reemplazado por emails). */
  email: string;
  /** Array con todos los emails de la empresa. */
  emails: Array<string>;
  /** Si la empresa actúa como cliente o no. */
  esCliente: boolean;
  /** Si la empresa actúa como proveedor o no. */
  esProveedor: boolean;
  /** Fecha de la última compra recibida por esta empresa. */
  fUltimaCompra: Fecha;
  /** @deprecated Fecha de la última reserva hecha. (Reemplazado por fUltimaCompra) */
  fUltimaReserva: Fecha;
  /** Fecha de la última visita comercial realizada. */
  fUltimaVisita: Fecha;
  /** Observaciones generales sobre la empresa. */
  observaciones: string;
  /** Nombre de la empresa. */
  razonSocial: string;
  /** Array con los teléfonos de la empresa. */
  telefonos: Array<Telefono>;
  /** Tipo de documento. En este caso debería permitir solo 'NIT'. */
  tipoDocumento: TipoDocumento;

  /** @deprecated Fecha de Creación. */
  creado: Fecha;
  /** @deprecated Id del usuario creador. */
  creador: string;
  /** @deprecated Fecha de de la última modificación */
  modificado: Fecha;
  /** @deprecated ID del último usuario que modificó. */
  modificador: string;
}

export interface EmpresaId extends Empresa {
  id: string;
}
