export const telefonoOptions = [
  { value: 'movil', viewValue: 'Móvil', icon: 'smartphone' },
  { value: 'fijo', viewValue: 'Fijo', icon: 'phone' },
];