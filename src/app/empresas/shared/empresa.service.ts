import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { Empresa, EmpresaId } from "./empresa";


@Injectable({
  providedIn: 'root'
})
export class EmpresaService extends AbsFirestoreService implements OnDestroy {

  // Collection prefix
  collectionPrefix = '';
  // Collection name
  collectionName = 'empresas';
  // Current loaded collection
  col$: Observable<EmpresaId[]>;
  // Current loaded document
  doc$: BehaviorSubject<EmpresaId | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
  ) {
    super(afs, auth);

    // Init current collection and current object
    this.initCol();
    this.initDoc();
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as EmpresaId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<EmpresaId>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): EmpresaId {
    return {
      id: '0',
      ciudad: 'Medellín',
      direccion: '',
      documento: '',
      email: '',
      emails: [],
      esCliente: false,
      esProveedor: false,
      fUltimaCompra: null,
      fUltimaReserva: null,
      fUltimaVisita: null,
      observaciones: '',
      razonSocial: '',
      telefonos: [],
      tipoDocumento: 'NIT',
      fCreado: null,
      creadorId: '',
      fModificado: null,
      modificadorId: '',
      instancia: this.auth.instancia,
      instancias: [ this.auth.instancia ],
      /** @deprecated */
      creado: null,
      /** @deprecated */
      creador: '',
      /** @deprecated */
      modificado: null,
      /** @deprecated */
      modificador: '' 
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): Empresa {
    let data: Empresa;
    data = {
      ciudad: (fd.ciudad) ? _.trim(fd.ciudad) :  '',
      direccion: (fd.direccion) ? _.trim(fd.direccion) : '',
      documento: (fd.documento) ? fd.documento.replace(/[^\d\-]/g,'') : '',
      email: (undefined == fd.email) ? '' : fd.email.toLowerCase(),
      emails: (fd.emails && 0 <= fd.emails.length) ? fd.emails.map(el => { el = el.email.toLowerCase(); return el; }) : [],
      esCliente: fd.esCliente ? true : false,
      esProveedor: fd.esProveedor ? true : false,
      fUltimaCompra: (fd.fUltimaCompra && '' !== fd.fUltimaCompra) ? fd.fUltimaCompra : null,
      fUltimaReserva: (fd.fUltimaReserva && '' !== fd.fUltimaReserva) ? fd.fUltimaReserva : null,
      fUltimaVisita: (fd.fUltimaVisita && '' !== fd.fUltimaVisita) ? fd.fUltimaVisita : null,
      observaciones: (fd.observaciones) ? fd.observaciones : '',
      razonSocial: (fd.razonSocial) ? _.trim(fd.razonSocial) : '',
      telefonos: (fd.telefonos && 0 <= fd.telefonos.length) ? fd.telefonos.map(el => { el.numero = el.numero.replace(/[^\d]/g,''); return el; }) : [],
      tipoDocumento: (fd.tipoDocumento && '' !== fd.tipoDocumento) ? fd.tipoDocumento : 'NIT',
      fCreado: (fd.fCreado && '' !== fd.fCreado) ? fd.fCreado : this.timestamp,
      creadorId: (fd.creadorId && '' !== fd.creadorId) ? fd.creadorId : this.auth.getUid(),
      fModificado: this.timestamp,
      modificadorId: this.auth.getUid(),
      instancia: (fd.instancia) ? fd.instancia : this.auth.instancia,
      instancias: (fd.instancias && '' !== fd.instancias && 0 < fd.instancias.length) ? fd.instancias.map(el => { el = el.instancia; return el; }) : [ this.auth.instancia ],
      /** @deprecated */
      creado: (fd.creado && '' !== fd.creado) ? fd.creado : this.timestamp,
      /** @deprecated */
      creador: (fd.creador && '' !== fd.creador) ? fd.creador : this.auth.getUid(),
      /** @deprecated */
      modificado: this.timestamp,
      /** @deprecated */
      modificador: this.auth.getUid(),
    }
    return data;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
