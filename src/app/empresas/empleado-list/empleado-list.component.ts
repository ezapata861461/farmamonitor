import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { PersonaAuthoService } from "../../personas/shared/persona-autho.service";
import { PersonaService } from 'src/app/personas/shared/persona.service';
import { EmpresaId } from '../shared/empresa';
import { EmpresaService } from '../shared/empresa.service';
import { EmpleadoFormComponent } from '../empleado-form/empleado-form.component';
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'app-empleado-list',
  templateUrl: './empleado-list.component.html',
  styleUrls: ['./empleado-list.component.scss'],
  providers: []
})
export class EmpleadoListComponent implements OnInit {

  idEmpresa: string = '';
  parentEmpresa: EmpresaId;

  subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private mainService: PersonaService,
    private empresaService: EmpresaService,
    public autho: PersonaAuthoService,
    private dialog: MatDialog,
    private loading: LoadingService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      combineLatest(this.route.params)
        .pipe(map(([params]) => params['id']))
        .subscribe(id => {
          this.idEmpresa = id;
          this.subscriptions.push(
            this.empresaService.getDoc(id).subscribe(empresa => {
              this.parentEmpresa = empresa as EmpresaId;
            })
          )
        })
    );
  }

  /**
   * Grid create event handler
   * @param $event 
   */
  onCreate() {
    this.mainService.id = '0';
    this.openDialog(this.idEmpresa);
  }

  /**
   * Grid row click event handler
   * @param $event 
   */
  onSelect($event) {
    this.mainService.id = $event.id;
    this.openDialog();
  }

  /**
   * Open form dialog
   * @param parent 
   */
  private openDialog(idEmpresa: string = ''): any {
    const dialog = this.dialog.open(EmpleadoFormComponent, {width: '90%', height: '90%'});

    if ('' !== idEmpresa) { dialog.componentInstance.parentEmpresa = this.parentEmpresa }
    dialog.componentInstance.isPopup = true;
    dialog.disableClose = true;
    dialog.componentInstance.canCreate = this.autho.canCreate;
    dialog.componentInstance.canEdit = this.autho.canEdit;
    dialog.componentInstance.canDelete = this.autho.canDelete;

    this.subscriptions.push(
      dialog.componentInstance.close.subscribe(e => { 
        dialog.close(); 
        this.loading.loaded();
      })
    );
    this.subscriptions.push(
      dialog.componentInstance.deleted.subscribe(e => {
        dialog.close();
        this.loading.loaded();
      })
    )
    return dialog;
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
