import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmpresasComponent } from "./empresas.component";
import { EmpresaListComponent } from "./empresa-list/empresa-list.component";
import { EmpresaComponent } from "./empresa/empresa.component";
import { EmpresaDetailsComponent } from "./empresa-details/empresa-details.component";
import { EmpleadoListComponent } from './empleado-list/empleado-list.component';

import { RoleGuard } from './shared/role.guard';
import { SedeListComponent } from './sede-list/sede-list.component';

const routes: Routes = [
  {
    path: '',
    component: EmpresasComponent,
    canActivate: [RoleGuard],
    children: [
      {
        path: '',
        component: EmpresaListComponent,
      },
      {
        path: ':id',
        component: EmpresaComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles'
          },
          {
            path: 'detalles',
            component: EmpresaDetailsComponent
          },
          {
            path: 'empleados',
            component: EmpleadoListComponent
          },
          {
            path: 'sedes',
            component: SedeListComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpresasRoutingModule { }
