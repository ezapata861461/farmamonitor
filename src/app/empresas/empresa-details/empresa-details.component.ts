import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { EmpresaAuthoService } from "../shared/empresa-autho.service";
import { AuthService } from 'src/app/auth/shared/auth.service';

@Component({
  selector: 'app-empresa-details',
  templateUrl: './empresa-details.component.html',
  styleUrls: ['./empresa-details.component.scss']
})
export class EmpresaDetailsComponent {

  // Route slug
  slug: string = 'empresas';

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: EmpresaAuthoService,
  ) { }

  /**
   * On form creation event
   * @param $event 
   */
  onCreated($event) {
    this.router.navigate([this.auth.instancia, this.slug, $event]);
  }

  /**
   * On form update event
   * @param $event 
   */
  onUpdated($event) {}

  /**
   * On form delete event
   * @param $event 
   */
  onDeleted() {
    this.router.navigate([this.auth.instancia, this.slug]);
  }
}
