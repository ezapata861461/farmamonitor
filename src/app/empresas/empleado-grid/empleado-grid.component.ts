import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonaGridComponent } from 'src/app/personas/persona-grid/persona-grid.component';
import { PersonaService } from 'src/app/personas/shared/persona.service';
import { PersonaAuthoService } from 'src/app/personas/shared/persona-autho.service';
import { MatDialog } from '@angular/material/dialog';
import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'win-empleado-grid',
  templateUrl: './empleado-grid.component.html',
  styleUrls: ['./empleado-grid.component.scss']
})
export class EmpleadoGridComponent extends PersonaGridComponent implements OnInit, OnDestroy {

  constructor(
    public mainService: PersonaService,
    public autho: PersonaAuthoService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService
  ) {
    super(mainService, autho, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
