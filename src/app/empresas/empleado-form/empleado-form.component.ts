import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonaFormComponent } from 'src/app/personas/persona-form/persona-form.component';
import { Router } from '@angular/router';
import { PersonaService } from 'src/app/personas/shared/persona.service';
import { MatDialog } from '@angular/material/dialog';
import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/auth/shared/auth.service';
import { PersonaAuthoService } from 'src/app/personas/shared/persona-autho.service';

@Component({
  selector: 'app-empleado-form',
  templateUrl: './empleado-form.component.html',
  styleUrls: ['./empleado-form.component.scss']
})
export class EmpleadoFormComponent extends PersonaFormComponent implements OnInit, OnDestroy {

  constructor(
    protected router: Router,
    public mainService: PersonaService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    protected afs: AngularFirestore,
    protected fb: FormBuilder,
    protected auth: AuthService,
    public autho: PersonaAuthoService
  ) { 
    super(router, mainService, confirm, errorLog, notify, loading, afs, fb, auth, autho);
  }

  ngOnInit() { super.ngOnInit() }

  ngOnDestroy() { super.ngOnDestroy() }
}
