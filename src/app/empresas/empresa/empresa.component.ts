import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { map } from "rxjs/operators";

import { EmpresaService }  from "../shared/empresa.service";
import { NotifyService } from 'src/app/core/shared/notify.service';
import { AuthService } from 'src/app/auth/shared/auth.service';
import { EmpresaAuthoService } from '../shared/empresa-autho.service';
import { PersonaAuthoService } from "../../personas/shared/persona-autho.service";
import { SedeAuthoService } from "../../sedes/shared/sede-autho.service";

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss']
})
export class EmpresaComponent implements OnInit, OnDestroy {

  // Route slug
  slug: string = 'empresas';
  id: string = '0';

  // Sidebar menu definition
  sidebar = [
    { name: 'Empresas', icon: 'arrow_back', link: '../', disabled: true, allow: this.empresaAutho },
    { name: 'Detalles', icon: 'create', link: './detalles' , disabled: true, allow: this.empresaAutho },
    { name: 'Empleados', icon: 'face', link: './empleados' , disabled: true, allow: this.personaAutho },
    { name: 'Sedes', icon: 'place', link: './sedes' , disabled: true, allow: this.sedeAutho },
    /**
     * @todo: Add visitas after menu construction
     * { name: 'Visitas', icon: 'vpn_key', link: './visitas' , disabled: true, allow: this.empresaAutho },
     */
  ];

  // Subscriptions
  idSub: Subscription;
  docSub: Subscription;
  userSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private empresaAutho: EmpresaAuthoService,
    private personaAutho: PersonaAuthoService,
    private sedeAutho: SedeAuthoService,
    private mainService: EmpresaService,
    private notify: NotifyService,
  ) { }

  ngOnInit() {
    // Watch id parameter changes in route
    this.idSub = combineLatest(this.route.params)
      .pipe(map(([params]) => params['id']))
      .subscribe(id => {
        this.id = id;
        this.mainService.id = id;
        this.setSidebar(id);
      });
    // Navigate to list view if non existing document
    this.docSub = this.mainService.doc$.subscribe(doc => {
      if (undefined === doc) {
        this.notify.update('El registro solicitado no existe o ha sido borrado.', 'error');
        this.router.navigate([this.auth.instancia, this.slug]);
      }
    });
  }

  /**
   * Sets sidebar permissions depending if creation or edition
   * @param id
   */
  setSidebar(id) {
    this.userSub = this.auth.user.subscribe(() => {
      for (let i = 0; i < this.sidebar.length; i++) {
        if ('0' === id) {
          if (i > 1) {
            this.sidebar[i].disabled = true;
          } else {
            this.sidebar[i].disabled = !this.sidebar[i].allow.canRead;
          }
        } else {
          this.sidebar[i].disabled = !this.sidebar[i].allow.canRead;
        }
      }
    });
  }

  ngOnDestroy() {
    this.idSub.unsubscribe();
    this.docSub.unsubscribe();
    this.userSub.unsubscribe();
  }
}
