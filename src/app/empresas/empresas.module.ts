import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule }      from "@angular/forms";

import { EmpresasRoutingModule } from './empresas-routing.module';
import { EmpresasComponent } from './empresas.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { EmpresaListComponent } from './empresa-list/empresa-list.component';
import { EmpresaDetailsComponent } from './empresa-details/empresa-details.component';
import { EmpresaFormComponent } from './empresa-form/empresa-form.component';
import { EmpresaGridComponent } from './empresa-grid/empresa-grid.component';
import { EmpresaSelectComponent } from './empresa-select/empresa-select.component';

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from "../auth/auth.module";
import { UsuariosModule } from "../usuarios/usuarios.module";
import { InstanciasModule } from "../instancias/instancias.module";
import { SedesModule } from '../sedes/sedes.module';

import { ConfirmDialogComponent } from "../core/confirm-dialog/confirm-dialog.component";
import { UsuarioGridComponent } from '../usuarios/usuario-grid/usuario-grid.component';
import { InstanciaGridComponent } from "../instancias/instancia-grid/instancia-grid.component";
import { EmpleadoListComponent } from './empleado-list/empleado-list.component';
import { EmpleadoGridComponent } from './empleado-grid/empleado-grid.component';
import { EmpleadoFormComponent } from './empleado-form/empleado-form.component';
import { SedeGridComponent } from '../sedes/sede-grid/sede-grid.component';
import { SedeListComponent } from './sede-list/sede-list.component';
import { SedeFormComponent } from '../sedes/sede-form/sede-form.component';

@NgModule({
  declarations: [
    EmpresasComponent, 
    EmpresaComponent, 
    EmpresaListComponent, 
    EmpresaDetailsComponent, 
    EmpresaFormComponent, 
    EmpresaGridComponent, 
    EmpresaSelectComponent, 
    EmpleadoListComponent, 
    EmpleadoGridComponent, 
    EmpleadoFormComponent,
    SedeListComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    EmpresasRoutingModule,
    CoreModule,
    SharedModule,
    AuthModule,
    UsuariosModule,
    InstanciasModule,
    SedesModule
  ],
  exports: [
    EmpresaGridComponent,
    EmpresaFormComponent,
    EmpresaSelectComponent,
  ],
  entryComponents: [
    ConfirmDialogComponent,
    UsuarioGridComponent,
    InstanciaGridComponent,
    EmpleadoFormComponent,
    EmpresaGridComponent,
    SedeGridComponent,
    SedeFormComponent
  ]
})
export class EmpresasModule { }
