import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { EmpresaAuthoService } from "../shared/empresa-autho.service";
import { EmpresaService } from "../shared/empresa.service";
import { EmpresaExistsValidator } from '../shared/empresa-exists.validator';
import { telefonoOptions } from "../shared/empresa-options";

import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';
import { AuthService } from 'src/app/auth/shared/auth.service';

@Component({
  selector: 'win-empresa-form',
  templateUrl: './empresa-form.component.html',
  styleUrls: ['./empresa-form.component.scss']
})
export class EmpresaFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'empresas'

  // Options
  telefonoOpts = telefonoOptions;

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  // Form parts show/hide flags
  parts = {
    deprecated: false,
    relacion: false,
    interacciones: false,
    registro: false,
    instancias: false
  }

  constructor(
    protected router: Router,
    public mainService: EmpresaService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    private afs: AngularFirestore,
    private fb: FormBuilder,
    private auth: AuthService,
    public autho: EmpresaAuthoService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = (this.autho.canCreate && '0' === this.mainService.id) || (this.autho.canEdit && '0' !== this.mainService.id);
    this.isSuperUser = this.autho.hasRol(['webmaster', 'admin']);
    this.isWebmaster = this.autho.hasRol(['webmaster']);

    // Get form config and hide/show form parts
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        const conf = config.modulos['empresas'].formParts;
        this.parts = {
          deprecated: this.isWebmaster || conf.deprecated,
          instancias: this.isWebmaster || conf.instancias,
          interacciones: this.isWebmaster || conf.interacciones,
          registro: this.isWebmaster || conf.registro,
          relacion: this.isWebmaster || conf.relacion
        }
      })
    );

    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      ciudad: [{ value: 'Medellín', disabled: !this.canCreateOrEdit }],
      direccion: [{ value: '', disabled: !this.canCreateOrEdit }],
      documento: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required
      ]],
      /** @deprecated */
      email: [{ value: '', disabled: !this.canCreateOrEdit }],
      emails: this.fb.array([]),
      esCliente: [{ value: false, disabled: !this.canCreateOrEdit }],
      esProveedor: [{ value: false, disabled: !this.canCreateOrEdit }],
      fUltimaCompra: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      fUltimaReserva: [{ value: null, disabled: !this.isSuperUser }],
      fUltimaVisita: [{ value: null, disabled: !this.isSuperUser }],
      observaciones: [{ value: '', disabled: !this.canCreateOrEdit }],
      razonSocial: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required,
        Validators.minLength(3),
      ]],
      telefonos: this.fb.array([]),
      tipoDocumento: [{ value: 'NIT', disabled: !this.canCreateOrEdit }],
      fCreado: [{ value: null, disabled: !this.isSuperUser }],
      creadorId: [{ value: '', disabled: !this.isSuperUser }],
      fModificado: [{ value: null, disabled: !this.isSuperUser }],
      modificadorId: [{ value: '', disabled: !this.isSuperUser }],
      instancia: [{ value: '', disabled: !this.isSuperUser }],
      instancias: this.fb.array([]),
      /** @deprecated */
      creado: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      creador: [{ value: '', disabled: !this.isSuperUser }],
      /** @deprecated */
      modificado: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      modificador: [{ value: '', disabled: !this.isSuperUser }],
    });
  }

  /**
   * Load controls
   */
  loadControls(): void { }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { 
    super.ngOnDestroy();
  }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.ciudad = data.ciudad;
      this.direccion = data.direccion;
      this.documento = data.documento;
      /** @deprecated */
      this.email = data.email;

      this.emails.clear();
      if (data.emails && data.emails.length > 0) {
        data.emails.forEach(element => {
          this.addEmail(element);
        });
      }

      this.esCliente = data.esCliente;
      this.esProveedor = data.esProveedor;
      this.fUltimaCompra = data.fUltimaCompra;
      /** @deprecated */
      this.fUltimaReserva = data.fUltimaReserva;
      this.fUltimaVisita =  data.fUltimaVisita;
      this.observaciones = data.observaciones;
      this.razonSocial = data.razonSocial;

      this.telefonos.clear();
      if (data.telefonos && data.telefonos.length > 0) {
        data.telefonos.forEach(element => {
          this.addTelefono(element);
        });
      }
      
      this.tipoDocumento = data.tipoDocumento;

      this.fCreado = data.fCreado;
      this.creadorId = data.creadorId;
      this.fModificado = data.fModificado;
      this.modificadorId = data.modificadorId;

      /** @deprecated */
      this.creado = data.creado;
      /** @deprecated */
      this.creador = data.creador;
      /** @deprecated */
      this.modificado = data.modificado;
      /** @deprecated */
      this.modificador = data.modificador;

      this.instancia = data.instancia;
      
      this.instancias.clear();
      if (data.instancias && data.instancias.length > 0) {
        data.instancias.forEach(element => {
          this.addInstancia(element);
        });
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get ciudad() {
    return this.form.get('ciudad');
  }

  set ciudad(ciudad) {
    this.ciudad.setValue(ciudad);
  }

  get direccion() {
    return this.form.get('direccion');
  }

  set direccion(direccion) {
    this.direccion.setValue(direccion);
  }

  get documento() {
    return this.form.get('documento');
  }

  set documento(documento) {
    this.documento.setAsyncValidators(EmpresaExistsValidator.documento(this.auth, this.afs, this.form));
    this.documento.setValue(documento);
  }

  get esCliente() {
    return this.form.get('esCliente');
  }

  set esCliente(esCliente) {
    this.esCliente.setValue(esCliente);
  }

  /** @deprecated */
  get email() {
    return this.form.get('email');
  }

  /** @deprecated */
  set email(email) {
    this.email.setValue(email);
  }

  get emails(): FormArray {
    return this.form.get('emails') as FormArray;
  }

  addEmail(email = '') {
    const element = this.fb.group({
      email: [{ value: email, disabled: !this.canCreateOrEdit }, [
        Validators.email,
        Validators.required
      ]],
    });
    this.emails.push(element);
  }

  deleteEmail(i) {
    this.emails.removeAt(i);
    this.form.markAsTouched();
  }

  get fUltimaCompra() {
    return this.form.get('fUltimaCompra');
  }

  set fUltimaCompra(fUltimaCompra: any) {
    if (null != fUltimaCompra && '' != fUltimaCompra) {
      const d = new Date(fUltimaCompra.seconds * 1000);
      this.fUltimaCompra.setValue(d);
    } else {
      this.fUltimaCompra.setValue(null);
    }
  }

  /** @deprecated */
  get fUltimaReserva() {
    return this.form.get('fUltimaReserva');
  }

  /** @deprecated */
  set fUltimaReserva(fUltimaReserva: any) {
    if (null != fUltimaReserva && '' != fUltimaReserva) {
      const d = new Date(fUltimaReserva.seconds * 1000);
      this.fUltimaReserva.setValue(d);
    } else {
      this.fUltimaReserva.setValue(null);
    }
  }

  get fUltimaVisita() {
    return this.form.get('fUltimaVisita');
  }

  set fUltimaVisita(fUltimaVisita: any) {
    if (null != fUltimaVisita && '' != fUltimaVisita) {
      const d = new Date(fUltimaVisita.seconds * 1000);
      this.fUltimaVisita.setValue(d);
    } else {
      this.fUltimaVisita.setValue(null);
    }
  }
  
  get esProveedor() {
    return this.form.get('esProveedor');
  }

  set esProveedor(esProveedor) {
    this.esProveedor.setValue(esProveedor);
  }
  
  get observaciones() {
    return this.form.get('observaciones');
  }

  set observaciones(observaciones) {
    this.observaciones.setValue(observaciones);
  }
  
  get razonSocial() {
    return this.form.get('razonSocial');
  }

  set razonSocial(razonSocial) {
    this.razonSocial.setValue(razonSocial);
  }
  
  get telefonos(): FormArray {
    return this.form.get('telefonos') as FormArray;
  }

  addTelefono(telefono = {tipo: '', numero: ''}) {
    const element = this.fb.group({
      tipo: [telefono.tipo, [
        Validators.required
      ]],
      numero: [this.formatPhone(telefono.numero), [
        Validators.required
      ]]
    });
    this.telefonos.push(element);
  }

  deleteTelefono(i) {
    this.telefonos.removeAt(i);
    this.form.markAsTouched();
  }

  /**
   * Format phone (With spaces for readability)
   */
  formatPhone(string: string): string {
    const onlyD = string.replace(/[^\d]/g,'');
    const l = onlyD.length;
    // Country code + Celphone
    if (l > 10) {
      return onlyD.slice(0, l-10) +' '+ onlyD.slice(l-10, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Cellphone or phone with country and city code
    if (l >= 9) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone with city code
    if (l >= 8) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone
    if (l > 4) {
      return onlyD.slice(0, l-4) +' '+ onlyD.slice(l-4, l);
    } else {
      return onlyD;
    }
  }

  get tipoDocumento() {
    return this.form.get('tipoDocumento');
  }

  set tipoDocumento(tipoDocumento) {
    this.tipoDocumento.setValue(tipoDocumento);
  }

  get instancia() {
    return this.form.get('instancia');
  }

  set instancia(instancia) {
    this.instancia.setValue(instancia);
  }

  get instancias(): FormArray {
    return this.form.get('instancias') as FormArray;
  }

  addInstancia(instancia = '') {
    const element = this.fb.group({
      instancia: [instancia, [
        Validators.required
      ]]
    });
    this.instancias.push(element);
  }

  deleteInstancia(i) {
    this.instancias.removeAt(i);
    this.form.markAsTouched();
  }

  /** @deprecated */
  get creado() {
    return this.form.get('creado');
  }

  /** @deprecated */
  set creado(creado: any) {
    if (null != creado) {
      const d = new Date(creado.seconds * 1000);
      this.creado.setValue(new Date(creado.seconds * 1000));
    } else {
      this.creado.setValue(null);
    }
  }

  /** @deprecated */
  get creador() {
    return this.form.get('creador');
  }

  /** @deprecated */
  set creador(creador) {
    this.creador.setValue(creador);
  }

  /** @deprecated */
  get modificado() {
    return this.form.get('modificado');
  }

  /** @deprecated */
  set modificado(modificado: any) {
    if (null != modificado) {
      const d = new Date(modificado.seconds * 1000);
      this.modificado.setValue(d);
    } else {
      this.modificado.setValue(null);
    }
  }

  /** @deprecated */
  get modificador() {
    return this.form.get('modificador');
  }

  /** @deprecated */
  set modificador(modificador) {
    this.modificador.setValue(modificador);
  }
}
