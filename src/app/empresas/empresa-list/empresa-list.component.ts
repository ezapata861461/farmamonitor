import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/auth/shared/auth.service';
import { EmpresaAuthoService } from "../shared/empresa-autho.service";
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'app-empresa-list',
  templateUrl: './empresa-list.component.html',
  styleUrls: ['./empresa-list.component.scss']
})
export class EmpresaListComponent {

  // Route slug
  slug: string = 'empresas';

  // Sidebar menu
  sidebar = [
    { name: 'Escritorio', icon: 'arrow_back', link: '../admin', disabled: false },
    { name: 'Lista de Empresas', icon: 'business', link: './' , disabled: false }
  ]

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: EmpresaAuthoService,
    private loading: LoadingService
  ) { }

  /**
   * Grid create event handler
   * @param $event 
   */
  onCreate() {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, 0]);
  }

  /**
   * Grid row click event handler
   * @param $event 
   */
  onSelect($event) {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, $event.id]);
  }
}