import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, Output, EventEmitter, Optional, Self, OnDestroy } from '@angular/core';
import { FormBuilder, NgControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldControl } from '@angular/material/form-field';

import { AbsSelectComponent } from '../../core/abstract/abs-select.component';
import { EmpresaGridComponent } from '../empresa-grid/empresa-grid.component';
import { EmpresaService } from '../shared/empresa.service';
import { EmpresaId } from '../shared/empresa';

@Component({
  selector: 'win-empresa-select',
  templateUrl: './empresa-select.component.html',
  styleUrls: ['./empresa-select.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: EmpresaSelectComponent }],
  host: {
    '[class.label-floating]': 'shouldLabelFloat',
    '[id]': 'id',
    '[attr.aria-describedby]': 'describedBy',
  }
})
export class EmpresaSelectComponent extends AbsSelectComponent implements OnDestroy {

  // "description" field name (to extract it from data)
  descrFieldName = 'razonSocial';
  // Current selected document
  document: EmpresaId;

  // Emited events
  @Output() load = new EventEmitter<EmpresaId>();
  @Output() select = new EventEmitter<EmpresaId>();

  // Custom input type and id
  controlType = 'win-empresa-select';
  id = `win-empresa-select-${EmpresaSelectComponent.nextId++}`;

  constructor(
    fb: FormBuilder,
    protected mainService: EmpresaService,
    protected fm: FocusMonitor,
    protected elRef: ElementRef<HTMLElement>,
    public dialog: MatDialog,
    @Optional() @Self() public ngControl: NgControl,
  ) {
    super(fb, mainService, fm, elRef, ngControl);
  }

  /**
   * Open select popup and populate fields on select
   */
  openSelect() {
    const dialog = this.dialog.open(EmpresaGridComponent, {data: {}, width: '80%', height: '80%'});
    dialog.componentInstance.mode = 'select';
    dialog.componentInstance.isPopup = true;
    dialog.disableClose = true;
    this.subscriptions.push(
      this.dialogCloseSub = dialog.componentInstance.close.subscribe(() => { dialog.close() })
    );

    this.subscriptions.push(
      this.dialogSub = dialog.componentInstance.select.subscribe(document => {
        // Keep selected document
        this.document = document;
  
        // Load data into form fields
        this.inputId = document.id;
        this.inputDescr = document[this.descrFieldName];
  
        // Emit selected document
        this.select.emit(document);
  
        // Notify change and run change detection
        this.onChange(document.id);
        this.stateChanges.next();
        
        // Close select dialog
        dialog.close();
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
