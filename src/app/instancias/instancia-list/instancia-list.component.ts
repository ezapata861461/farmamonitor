import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth/shared/auth.service';
import { InstanciaAuthoService } from "../shared/instancia-autho.service";
import { LoadingService } from "../../core/shared/loading.service";

@Component({
  selector: 'app-instancia-list',
  templateUrl: './instancia-list.component.html',
  styleUrls: ['./instancia-list.component.scss']
})
export class InstanciaListComponent {

  // Route slug
  slug: string = 'instancias';

  // Sidebar menu
  sidebar = [
    { name: 'Escritorio', icon: 'arrow_back', link: '../admin', disabled: false },
    { name: 'Lista de Instancias', icon: 'developer_board', link: './' , disabled: false }
  ]

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: InstanciaAuthoService,
    private loading: LoadingService
  ) { }

  /**
   * Grid create event handler
   * @param $event 
   */
  onCreate() {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, 0]);
  }

  /**
   * Grid row click event handler
   * @param $event 
   */
  onSelect($event) {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, $event.id]);
  }
}
