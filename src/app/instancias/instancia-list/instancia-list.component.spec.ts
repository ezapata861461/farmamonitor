import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanciaListComponent } from './instancia-list.component';

describe('InstanciaListComponent', () => {
  let component: InstanciaListComponent;
  let fixture: ComponentFixture<InstanciaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanciaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanciaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
