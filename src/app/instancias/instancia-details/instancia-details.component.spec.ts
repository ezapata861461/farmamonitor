import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanciaDetailsComponent } from './instancia-details.component';

describe('InstanciaDetailsComponent', () => {
  let component: InstanciaDetailsComponent;
  let fixture: ComponentFixture<InstanciaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanciaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanciaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
