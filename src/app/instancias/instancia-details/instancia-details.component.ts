import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { InstanciaAuthoService } from "../shared/instancia-autho.service";
import { AuthService } from 'src/app/auth/shared/auth.service';

@Component({
  selector: 'app-instancia-details',
  templateUrl: './instancia-details.component.html',
  styleUrls: ['./instancia-details.component.scss']
})
export class InstanciaDetailsComponent {

  // Route slug
  slug: string = 'instancias';

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: InstanciaAuthoService,
  ) { }

  /**
   * On form creation event
   * @param $event 
   */
  onCreated($event) {
    this.router.navigate([this.auth.instancia, this.slug, $event]);
  }

  /**
   * On form update event
   * @param $event 
   */
  onUpdated($event) {}

  /**
   * On form delete event
   * @param $event 
   */
  onDeleted() {
    this.router.navigate([this.auth.instancia, this.slug]);
  }
}
