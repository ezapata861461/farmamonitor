import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { InstanciaAuthoService } from '../shared/instancia-autho.service';
import { ApiService } from '../shared/api.service';
import { InstanciaId } from '../shared/instancia';
import { ApiFormComponent } from '../api-form/api-form.component';
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'app-api-list',
  templateUrl: './api-list.component.html',
  styleUrls: ['./api-list.component.scss']
})
export class ApiListComponent implements OnInit {

  idInstancia: string = '';
  parentInstancia: InstanciaId;

  subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private mainService: ApiService,
    public autho: InstanciaAuthoService,
    private dialog: MatDialog,
    private loading: LoadingService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      combineLatest(this.route.params)
        .pipe(map(([params]) => params['id']))
        .subscribe(id => { this.idInstancia = id; })
    );
  }

  /**
   * Grid create event handler
   * @param $event 
   */
  onCreate() {
    this.mainService.id = '0';
    this.openDialog();
  }

  /**
   * Grid row click event handler
   * @param $event 
   */
  onSelect($event) {
    this.mainService.id = $event.id;
    this.openDialog();
  }

  /**
   * Open form dialog
   * @param parent 
   */
  private openDialog(): any {
    const dialog = this.dialog.open(ApiFormComponent, {width: '90%', height: '90%'});

    dialog.componentInstance.idInstancia = this.idInstancia
    dialog.componentInstance.isPopup = true;
    dialog.disableClose = true;
    dialog.componentInstance.canCreate = this.autho.canCreate;
    dialog.componentInstance.canEdit = this.autho.canEdit;
    dialog.componentInstance.canDelete = this.autho.canDelete;

    this.subscriptions.push(
      dialog.componentInstance.close.subscribe(e => { 
        dialog.close(); 
        this.loading.loaded();
      })
    );
    this.subscriptions.push(
      dialog.componentInstance.deleted.subscribe(e => {
        dialog.close();
        this.loading.loaded();
      })
    )
    return dialog;
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
