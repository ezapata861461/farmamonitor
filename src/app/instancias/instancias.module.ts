import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule }      from "@angular/forms";

import { InstanciasRoutingModule } from './instancias-routing.module';
import { InstanciasComponent } from './instancias.component';
import { InstanciaComponent } from './instancia/instancia.component';
import { InstanciaDetailsComponent } from './instancia-details/instancia-details.component';
import { InstanciaFormComponent } from './instancia-form/instancia-form.component';
import { InstanciaGridComponent } from './instancia-grid/instancia-grid.component';
import { InstanciaListComponent } from './instancia-list/instancia-list.component';
import { InstanciaSelectComponent } from './instancia-select/instancia-select.component';

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';

import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { ModuloGridComponent } from './modulo-grid/modulo-grid.component';
import { ModuloListComponent } from './modulo-list/modulo-list.component';
import { ApiListComponent } from './api-list/api-list.component';
import { ApiGridComponent } from './api-grid/api-grid.component';
import { ModuloFormComponent } from './modulo-form/modulo-form.component';
import { ApiFormComponent } from './api-form/api-form.component';

@NgModule({
  declarations: [
    InstanciasComponent, 
    InstanciaComponent,
    InstanciaDetailsComponent, 
    InstanciaFormComponent, 
    InstanciaGridComponent, 
    InstanciaListComponent, 
    InstanciaSelectComponent, 
    ModuloGridComponent, 
    ModuloListComponent, 
    ApiListComponent, 
    ApiGridComponent, 
    ModuloFormComponent, ApiFormComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    InstanciasRoutingModule,
    CoreModule,
    SharedModule,
    AuthModule
  ],
  exports: [
    InstanciaGridComponent,
    InstanciaFormComponent,
    InstanciaSelectComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    ModuloFormComponent,
    ApiFormComponent
  ]
})
export class InstanciasModule { }
