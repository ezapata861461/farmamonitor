import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanciaGridComponent } from './instancia-grid.component';

describe('InstanciaGridComponent', () => {
  let component: InstanciaGridComponent;
  let fixture: ComponentFixture<InstanciaGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanciaGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanciaGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
