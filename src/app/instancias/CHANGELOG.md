# Changelog

## [0.0.9] - 2019-12-13

### Added
* Add method getApiConfig to api.service.

### Changed
* Load moduloOptions from environments file (get it from config).

### Fixed
* Make parameters for 'addParam' and 'addFormPart' methods optional.

## [0.0.8] - 2019-12-08

### Added
* Add 'params' field to api-form.
* Add 'params' and 'enabled' field to module-form.
* Add 'empresa' and 'sede' fields to instancia-form.

### Changed
* Load module options from app.config.ts

## Removed
* Remove old 'module fields' from instancia-form.

## [0.0.7] - 2019-12-07

### Added
* Add 'modulos' functionality to create/edit 'modulos' by instancia.
* Add 'apis' functionality to create/edit 'apis' by instancia.

### Removed
* Remove old instancia-modulos/apis dependencies.

## [0.0.6] - 2019-11-12

### Changed
* Modify form to keep only modules.
* Improve form look.
* Improve grid toolbar and pagination.
* Test and complete with current components.
* Move 'modulos' config to a different module.
* Fix comments.

### Removed
* Remove UsuariosModule to avoid circular dependency.

## [0.0.5] - 2019-10-19

### Added
* Add woocommerce API to config.

## [0.0.4] - 2019-10-18

### Fixed
* Fix bug with matdatepicker.
* Remove 'parts' conditionals.

## [0.0.3] - 2019-10-17

### Changed
* Rename abstract classes to avoid legacy problems.
* Rename confirm-modal to confirm-dialog to avoid legacy problems.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.
* Add more config. options for collections.

### Changed
* Improve interface code and docs.

## [0.0.1] - 2019-10-10

* First commit.