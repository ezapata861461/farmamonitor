import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';
import * as _ from "lodash";

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { InstanciaAuthoService } from "../shared/instancia-autho.service";
import { ModuloService } from "../shared/modulo.service";
import { ModuloExistsValidator } from '../shared/modulo-exists.validator';
import { moduloOptions, strategyOptions } from "../shared/modulo-options";

import { ErrorLogService } from '../../core/shared/error-log.service';
import { NotifyService } from '../../core/shared/notify.service';
import { LoadingService } from '../../core/shared/loading.service';
import { AuthService } from '../../auth/shared/auth.service';

@Component({
  selector: 'win-modulo-form',
  templateUrl: './modulo-form.component.html',
  styleUrls: ['./modulo-form.component.scss']
})
export class ModuloFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'modulos';

  /**
   * Parent instancia
   */
  @Input()
  set idInstancia(idInstancia: string|null) { this._idInstancia = idInstancia }
  get idInstancia(): string|null { return this._idInstancia }
  private _idInstancia: string|null = null;

  // Options
  moduloOpts = moduloOptions;
  strategyOpts = strategyOptions;

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  constructor(
    protected router: Router,
    public mainService: ModuloService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    protected afs: AngularFirestore,
    protected fb: FormBuilder,
    protected auth: AuthService,
    public autho: InstanciaAuthoService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = this.autho.hasRol(['webmaster']);
    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      count: [{ value: 0, disabled: !this.canCreateOrEdit }],
      enabled: [{ value: false, disabled: !this.canCreateOrEdit }],
      formParts: this.fb.array([]),
      nombre: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required
      ]],
      strategy: [{ value: 'separatedCollection', disabled: !this.canCreateOrEdit }],
      params: this.fb.array([])
    });
  }

  /**
   * Load controls
   */
  loadControls(): void { }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.count = data.count;
      this.enabled = data.enabled;

      this.formParts.clear();
      for (const key in data.formParts) {
        if (data.formParts.hasOwnProperty(key)) {
          const value = data.formParts[key];
          this.addFormPart(key, value);
        }
      }

      this.nombre = data.nombre;
      this.strategy = data.strategy;

      this.params.clear();
      for (const key in data.params) {
        if (data.params.hasOwnProperty(key)) {
          const value = data.params[key];
          this.addParam(key, value);
        }
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get count() {
    return this.form.get('count');
  }

  set count(count) {
    this.count.setValue(count);
  }

  get enabled() {
    return this.form.get('enabled');
  }

  set enabled(enabled) {
    this.enabled.setValue(enabled);
  }

  get formParts(): FormArray {
    return this.form.get('formParts') as FormArray;
  }

  addFormPart(key: string = '', value: boolean = false) {
    const element = this.fb.group({
      part: [key, [
        Validators.required
      ]],
      active: [value, [
        Validators.required
      ]]
    });
    this.formParts.push(element);
  }

  deleteFormPart(i) {
    this.formParts.removeAt(i);
    this.form.markAsTouched();
  }

  get nombre() {
    return this.form.get('nombre');
  }

  set nombre(nombre) {
    this.nombre.setAsyncValidators(ModuloExistsValidator.nombre(this.afs, this.idInstancia, this.form));
    this.nombre.setValue(nombre);
  }

  get strategy() {
    return this.form.get('strategy');
  }

  set strategy(strategy) {
    this.strategy.setValue(strategy);
  }

  get params(): FormArray {
    return this.form.get('params') as FormArray;
  }

  addParam(key: string = '', value: string = '') {
    const element = this.fb.group({
      key: [key, [
        Validators.required
      ]],
      value: [value, [
        Validators.required
      ]]
    });
    this.params.push(element);
  }

  deleteParam(i) {
    this.params.removeAt(i);
    this.form.markAsTouched();
  }
}
