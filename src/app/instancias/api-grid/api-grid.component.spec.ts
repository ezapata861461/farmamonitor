import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiGridComponent } from './api-grid.component';

describe('ApiGridComponent', () => {
  let component: ApiGridComponent;
  let fixture: ComponentFixture<ApiGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
