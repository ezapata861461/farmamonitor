import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

import { AbsGridComponent } from "../../core/abstract/abs-grid.component";
import { ConfirmDialogComponent } from "../../core/confirm-dialog/confirm-dialog.component";

import { InstanciaAuthoService } from "../shared/instancia-autho.service";
import { ApiService } from "../shared/api.service";
import { GenericApiId } from "../shared/api";
import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'win-api-grid',
  templateUrl: './api-grid.component.html',
  styleUrls: ['./api-grid.component.scss']
})
export class ApiGridComponent extends AbsGridComponent implements OnInit, OnDestroy {

  // Mat table definition
  dataSource: MatTableDataSource<GenericApiId>;
  selection = new SelectionModel<GenericApiId>(true, []);

  // Selected ítems emmiter
  @Output() 
  select = new EventEmitter<GenericApiId[] | GenericApiId>();

  /**
   * Input to setup nested collection path
   */
  @Input()
  set idInstancia(idInstancia: string) {
    if ('' !== idInstancia) {
      this._idInstancia = idInstancia;
      this.mainService.collectionName = 'apis';
      this.mainService.collectionPrefix = `instancias/${idInstancia}`;
      // initCol() runs here not in service
      this.mainService.initCol();
    }
  }
  get idInstancia(): string { return this._idInstancia }
  private _idInstancia: string = '';

  constructor(
    public mainService: ApiService,
    public autho: InstanciaAuthoService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService
  ) {
    super(mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    super.ngOnInit();

    // Mat table column definition
    this.columns = [ 'mobileView', 'nombre', 'enabled' ];

    // Setup main service
    this.mainService.pageSize = 50;
    if (null === this.mainService.sort) { this.mainService.sort = 'nombre' }
    if (null === this.mainService.sortDir) { this.mainService.sortDir = 'asc' }
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  /**
   * Set grid filter
   * @param filter 
   */
  setFilter(filter) { }

  /**
   * Delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmDialogComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar estas apis?'},
    });

    this.subscriptions.push(
      confirm.afterClosed().subscribe(result => {
        if (result) {
          this.onLoading();
          this.mainService.deleteCollection(this.selection.selected)
            .then(x => {
              this.selection.clear();
              this.onLoaded('Borrado!');
              this.deleted.emit(true);
            })
            .catch(error => {
              this.onError(error, 'No fue posible borrar. Por favor intente de nuevo.');
              this.onLoaded();
            });
        }
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
