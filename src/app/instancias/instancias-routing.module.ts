import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstanciasComponent } from "./instancias.component";
import { InstanciaListComponent } from "./instancia-list/instancia-list.component";
import { InstanciaComponent } from "./instancia/instancia.component";
import { InstanciaDetailsComponent } from "./instancia-details/instancia-details.component";
import { ModuloListComponent } from './modulo-list/modulo-list.component';
import { ApiListComponent } from './api-list/api-list.component';

import { RoleGuard } from './shared/role.guard';

const routes: Routes = [
  {
    path: '',
    component: InstanciasComponent,
    canActivate: [RoleGuard],
    children: [
      {
        path: '',
        component: InstanciaListComponent,
      },
      {
        path: ':id',
        component: InstanciaComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles',
          },
          {
            path: 'detalles',
            component: InstanciaDetailsComponent
          },
          {
            path: 'modulos',
            component: ModuloListComponent
          },
          {
            path: 'apis',
            component: ApiListComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstanciasRoutingModule { }
