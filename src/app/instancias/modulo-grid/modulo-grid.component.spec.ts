import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuloGridComponent } from './modulo-grid.component';

describe('ModuloGridComponent', () => {
  let component: ModuloGridComponent;
  let fixture: ComponentFixture<ModuloGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuloGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuloGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
