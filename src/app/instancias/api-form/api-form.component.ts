import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';
import * as _ from "lodash";

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { InstanciaAuthoService } from "../shared/instancia-autho.service";
import { ApiService } from "../shared/api.service";
import { ApiExistsValidator } from '../shared/api-exists.validator';
import { apiOptions } from "../shared/api-options";

import { ErrorLogService } from '../../core/shared/error-log.service';
import { NotifyService } from '../../core/shared/notify.service';
import { LoadingService } from '../../core/shared/loading.service';
import { AuthService } from '../../auth/shared/auth.service';

@Component({
  selector: 'win-api-form',
  templateUrl: './api-form.component.html',
  styleUrls: ['./api-form.component.scss']
})
export class ApiFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'apis';

  /**
   * Parent instancia
   */
  @Input()
  set idInstancia(idInstancia: string|null) { this._idInstancia = idInstancia }
  get idInstancia(): string|null { return this._idInstancia }
  private _idInstancia: string|null = null;

  // Options
  apiOpts = apiOptions;

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  constructor(
    protected router: Router,
    public mainService: ApiService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    protected afs: AngularFirestore,
    protected fb: FormBuilder,
    protected auth: AuthService,
    public autho: InstanciaAuthoService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = this.autho.hasRol(['webmaster']);
    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      apiKey: [{ value: '', disabled: !this.canCreateOrEdit }],
      apiSecret: [{ value: '', disabled: !this.canCreateOrEdit }],
      cliente: [{ value: null, disabled: !this.canCreateOrEdit }],
      enabled: [{ value: false, disabled: !this.canCreateOrEdit }],
      nombre: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required
      ]],
      queryStringAuth: [{ value: false, disabled: !this.canCreateOrEdit }],
      url: [{ value: '', disabled: !this.canCreateOrEdit }],
      version: [{ value: '', disabled: !this.canCreateOrEdit }],
      params: this.fb.array([])
    });
  }

  /**
   * Load controls
   */
  loadControls(): void {
    // Disable 'empresaId' field for freelancers 
    this.subscriptions.push(
      this.nombre.valueChanges
        .subscribe(nombre => {
          switch (nombre) {
            case 'hablame':
              this.enabled.enable();
              this.apiKey.enable();
              this.apiSecret.disable();
              this.cliente.enable();
              this.queryStringAuth.disable();
              this.url.disable();
              this.version.disable();
              break;

            case 'googlemaps':
              this.enabled.enable();
              this.apiKey.enable();
              this.apiSecret.disable();
              this.cliente.disable();
              this.queryStringAuth.disable();
              this.url.disable();
              this.version.disable();
              break;

            case 'sendgrid':
              this.enabled.enable();
              this.apiKey.enable();
              this.apiSecret.disable();
              this.cliente.disable();
              this.queryStringAuth.disable();
              this.url.disable();
              this.version.disable();
              break;

            case 'woocommerce':
              this.enabled.enable();
              this.apiKey.enable();
              this.apiSecret.enable();
              this.cliente.disable();
              this.queryStringAuth.enable();
              this.url.enable();
              this.version.enable();
              break;

            case 'xredes':
              this.enabled.enable();
              this.apiKey.disable();
              this.apiSecret.disable();
              this.cliente.disable();
              this.queryStringAuth.disable();
              this.url.enable();
              this.version.disable();
              break;
            
            case '':
            default:
              this.enabled.disable();
              this.apiKey.disable();
              this.apiSecret.disable();
              this.cliente.disable();
              this.queryStringAuth.disable();
              this.url.disable();
              this.version.disable();
              break;
          }
        })
    );
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.apiKey = data.apiKey;
      this.apiSecret = data.apiSecret;
      this.cliente = data.cliente;
      this.enabled = data.enabled;
      this.nombre = data.nombre;
      this.queryStringAuth = data.queryStringAuth;
      this.url = data.url;
      this.version = data.version;

      this.params.clear();
      for (const key in data.params) {
        if (data.params.hasOwnProperty(key)) {
          const value = data.params[key];
          this.addParam(key, value);
        }
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get apiKey() {
    return this.form.get('apiKey');
  }

  set apiKey(apiKey) {
    this.apiKey.setValue(apiKey);
  }

  get apiSecret() {
    return this.form.get('apiSecret');
  }

  set apiSecret(apiSecret) {
    this.apiSecret.setValue(apiSecret);
  }

  get cliente() {
    return this.form.get('cliente');
  }

  set cliente(cliente) {
    this.cliente.setValue(cliente);
  }

  get enabled() {
    return this.form.get('enabled');
  }

  set enabled(enabled) {
    this.enabled.setValue(enabled);
  }

  get nombre() {
    return this.form.get('nombre');
  }

  set nombre(nombre) {
    this.nombre.setAsyncValidators(ApiExistsValidator.nombre(this.afs, this.idInstancia, this.form));
    this.nombre.setValue(nombre);
  }

  get queryStringAuth() {
    return this.form.get('queryStringAuth');
  }

  set queryStringAuth(queryStringAuth) {
    this.queryStringAuth.setValue(queryStringAuth);
  }

  get url() {
    return this.form.get('url');
  }

  set url(url) {
    this.url.setValue(url);
  }

  get version() {
    return this.form.get('version');
  }

  set version(version) {
    this.version.setValue(version);
  }

  get params(): FormArray {
    return this.form.get('params') as FormArray;
  }

  addParam(key: string = '', value: string = '') {
    const element = this.fb.group({
      key: [key, [
        Validators.required
      ]],
      value: [value, [
        Validators.required
      ]]
    });
    this.params.push(element);
  }

  deleteParam(i) {
    this.params.removeAt(i);
    this.form.markAsTouched();
  }
}
