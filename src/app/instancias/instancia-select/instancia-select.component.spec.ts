import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanciaSelectComponent } from './instancia-select.component';

describe('InstanciaSelectComponent', () => {
  let component: InstanciaSelectComponent;
  let fixture: ComponentFixture<InstanciaSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanciaSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanciaSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
