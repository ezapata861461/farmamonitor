import { environment } from "../../../environments/environment";

/**
 * Load enabled modules config from environment file
 */
export const moduloOptions = environment.modules;

/**
 * strategy options for each module
 */
export const strategyOptions = [
  { value: 'none', viewValue: 'Ninguna', icon: '' },
  { value: 'field', viewValue: 'Un Campo', icon: '' },
  { value: 'arrayField', viewValue: 'Un Array', icon: '' },
  { value: 'separatedCollection', viewValue: 'Colección Separada', icon: '' }
];
