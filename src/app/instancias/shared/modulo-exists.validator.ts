import { map, take, debounceTime } from "rxjs/operators";
import { FormGroup, AbstractControl } from "@angular/forms";
import { AngularFirestore } from "@angular/fire/firestore";

import { ModuloId } from "../shared/modulo";

/**
 * Validates 'modulo' is unique by nombre
 */
export class ModuloExistsValidator {
  static nombre(afs: AngularFirestore, idInstancia: string, form: FormGroup) {
    return (control: AbstractControl) => {
      const nombre = control.value.toLowerCase();
      return afs.collection(`instancias/${idInstancia}/modulos`, ref => ref.where('nombre', '==', nombre))
        .snapshotChanges().pipe(
          debounceTime(500),
          take(1),
          map(items => {
            let data = items.map(a => {
              const data = a.payload.doc.data() as ModuloId;
              const id = a.payload.doc.id;
              return { id, ...data };
            });

            if (data.length && '' !== form.value.nombre && (0 === form.value.id || data[0].id !== form.value.id)) {
              return { moduloExists: data[0].nombre };
            }
            return null;
          })
        );
    }
  }
}