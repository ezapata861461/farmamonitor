export interface Modulo {
  /** Conteo de los documentos de la colección */
  count: number;
  /** Si el módulo está habilitado */
  enabled: boolean;
  /** Objeto compuesto por pares strin - boolean, que indican si una parte del formulario esta habilitada o no. */
  formParts: { [ s: string ]: boolean };
  /** Nombre del módulo */
  nombre: string;
  /** Estrategia usada para separar los datos por instancia */
  strategy: InstanciaStrategy;
  /** Parámetros adicionales (pares key: value) */
  params: {[ s: string ]: string };
}

export interface ModuloId extends Modulo {
  id: string;
}