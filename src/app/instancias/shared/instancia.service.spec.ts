import { TestBed } from '@angular/core/testing';

import { InstanciaService } from './instancia.service';

describe('InstanciaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstanciaService = TestBed.get(InstanciaService);
    expect(service).toBeTruthy();
  });
});
