import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { Modulo, ModuloId } from "./modulo";

@Injectable({
  providedIn: 'root'
})
export class ModuloService extends AbsFirestoreService implements OnDestroy {

  // Since this is a nested thing
  defaultInstanciaStrategy: InstanciaStrategy = 'none';
  // Current loaded collection
  col$: Observable<ModuloId[]>;
  // Current loaded document
  doc$: BehaviorSubject<ModuloId | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
  ) {
    super(afs, auth);

    // Init current collection and current object
    // Delegate initCol to modulo-grid!! // this.initCol();
    this.initDoc();
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as ModuloId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<ModuloId>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): ModuloId {
    return {
      id: '0',
      count: 0,
      enabled: false,
      formParts: {},
      nombre: '',
      strategy: 'separatedCollection',
      params: {}
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): Modulo {
    let data: Modulo;
    data = {
      count: (fd.count) ? +fd.count : 0,
      enabled: (fd.enabled) ? true : false,
      formParts: (fd.formParts) ? this.extractFormParts(fd.formParts) : {},
      nombre: (fd.nombre) ? fd.nombre : '',
      strategy: (fd.strategy && '' !== fd.strategy) ? fd.strategy : 'none',
      params: (fd.params) ? this.extractParams(fd.params) : {},
    }

    return data;
  }

  /**
   * Extracts formParts array into an Object
   * @param formParts 
   */
  extractFormParts(formParts: any[]) {
    let formPartsObject: {[ s: string ]: boolean } = {};
    formParts.forEach(formPart => {
      formPartsObject[formPart.part] = formPart.active;
    })
    return formPartsObject;
  }

  /**
   * Extracts params array into an Object
   * @param formParts 
   */
  extractParams(params: any[]) {
    let paramsObject: {[ s: string ]: string } = {};
    params.forEach(param => {
      paramsObject[param.key] = param.value;
    })
    return paramsObject;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
