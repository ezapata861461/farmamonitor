export const apiOptions = [
  { value: 'hablame', viewValue: 'Hablame', icon: '' },
  { value: 'googlemaps', viewValue: 'Google Maps', icon: '' },
  { value: 'sendgrid', viewValue: 'Sendgrid', icon: '' },
  { value: 'woocommerce', viewValue: 'WooCommerce', icon: '' },
  { value: 'xredes', viewValue: 'XRedes', icon: '' }
];