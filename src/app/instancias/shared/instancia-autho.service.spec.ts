import { TestBed } from '@angular/core/testing';

import { InstanciaAuthoService } from './instancia-autho.service';

describe('InstanciaAuthoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstanciaAuthoService = TestBed.get(InstanciaAuthoService);
    expect(service).toBeTruthy();
  });
});
