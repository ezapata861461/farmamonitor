import { Modulo } from './modulo';
import { GenericApi } from './api';

/**
 * Instancia representa una unidad que agrupa a una empresa (o a una sede de ella) y a sus usuarios.
 * Cada registro de la aplicación tiene un campo/array con la(s) instancia(s) a la(s) que pertenece.
 * En la aplicación, la sesión apunta a una sola instancia y los registros se muestran solo si contiene la instancia seleccionada.
 * Adicionalmente, la instancia contiene la configuración para la aplicación y sus colecciones (modulos y apis).
 */
export interface Instancia {
  // Nombre de la instancia
  nombre: string;
  // Razón Social de la empresa asociada
  empresa: string;
  // Id de la empresa asociada
  empresaId: string;
  // Nombre de la sede asociada
  sede: string;
  // Id de la sede asociada
  sedeId: string;
  /** Array con la configuración de las apis disponibles para la instancia */
  apis?: { [ s: string ]: GenericApi };
  /** Array con la configuración de los diferentes módulos que componen la instancia. */
  modulos?: { [ s: string ]: Modulo };
  /** Fecha de Creación. */
  fCreado: Fecha;
  /** Id del usuario creador. */
  creadorId: string;
  /** Fecha de de la última modificación */
  fModificado: Fecha;
  /** ID del último usuario que modificó. */
  modificadorId: string;
}

export interface InstanciaId extends Instancia {
  id: string;
}
