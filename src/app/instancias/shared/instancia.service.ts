import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { Instancia, InstanciaId } from "./instancia";

@Injectable({
  providedIn: 'root'
})
export class InstanciaService extends AbsFirestoreService implements OnDestroy {

  // Collection prefix
  collectionPrefix = '';
  // Collection name
  collectionName: string = 'instancias';
  // Current loaded collection
  col$: Observable<InstanciaId[]>;
  // Current loaded document
  doc$: BehaviorSubject<InstanciaId | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
  ) {
    super(afs, auth);
    this.instancia = false;

    // Init current collection and current object
    this.initCol();
    this.initDoc();
  }

  /**
   * Save (create/update) a document
   * @param data 
   */
  save(data: InstanciaId): Promise<any> {
    const collectionRef = this.getCollectionReference();
    const docData: any = this.extract(data);

    // If edition
    if ('0' !== data.id && undefined !== data.id) {
      return collectionRef.doc(data.id).set(docData);
    // Otherwise, creation
    } else {
      // Create instancia with custom id
      const newId = data.nombre.toLowerCase().replace(/[^a-z]/gi,'')
      return collectionRef.doc(newId).set(docData);
    }
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as InstanciaId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<InstanciaId>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): InstanciaId {
    return {
      id: '0',
      nombre: '',
      empresa: '',
      empresaId: '',
      sede: '',
      sedeId: '',
      fCreado: null,
      creadorId: '',
      fModificado: null,
      modificadorId: ''
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): Instancia {
    let data: Instancia;
    data = {
      nombre: (fd.nombre) ? _.trim(fd.nombre) : '',
      empresa: (fd.empresa) ? fd.empresa : '',
      empresaId: (fd.empresaId) ? fd.empresaId : '',
      sede: (fd.sede) ? fd.sede : '',
      sedeId: (fd.sedeId) ? fd.sedeId : '',
      fCreado: (fd.fCreado && '' !== fd.fCreado) ? fd.fCreado : this.timestamp,
      creadorId: (fd.creadorId && '' !== fd.creadorId) ? fd.creadorId : this.auth.getUid(),
      fModificado: this.timestamp,
      modificadorId: this.auth.getUid(),
    }
    return data;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
