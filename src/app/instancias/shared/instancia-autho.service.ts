import { Injectable } from '@angular/core';

import { AbsAuthorizationService } from '../../auth/shared/abs-authorization.service';
import { AuthService } from "../../auth/shared/auth.service";

@Injectable({
  providedIn: 'root'
})
export class InstanciaAuthoService extends AbsAuthorizationService {

  constructor(
    protected auth: AuthService,
  ) {
    super(auth);

    // Set allowed roles arrays
    this.readAllowedRoles =       [  ];
    this.createAllowedRoles =     [  ];
    this.editAllowedRoles =       [  ];
    this.deleteAllowedRoles =     [  ];
    this.softdeleteAllowedRoles = [  ];
    this.fileAllowedRoles =       [  ];
  }
}
