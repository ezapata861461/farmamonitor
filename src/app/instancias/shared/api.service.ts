import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { GenericApi, GenericApiId } from './api';

@Injectable({
  providedIn: 'root'
})
export class ApiService extends AbsFirestoreService implements OnDestroy {

  defaultInstanciaStrategy: InstanciaStrategy = 'none';
  // Current loaded collection
  col$: Observable<GenericApi[]>;
  // Current loaded document
  doc$: BehaviorSubject<GenericApi | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
  ) {
    super(afs, auth);

    // Init current collection and current object
    // Delegate initCol to modulo-grid!! // this.initCol();
    this.initDoc();
  }

  /**
   * Retrieves one api configuration
   * @param instancia 
   * @param api 
   */
  getApiConfig(instancia: string, api: string): Observable<any[]> {
    return this.afs.collection(`instancias/${instancia}/apis`, ref => ref.where('nombre', '==', api)).valueChanges();
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as GenericApiId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<GenericApi>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): GenericApiId {
    return {
      id: '0',
      enabled: false,
      apiKey: '',
      apiSecret: '',
      cliente: null,
      nombre: '',
      queryStringAuth: false,
      url: '',
      version: '',
      params: {}
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): GenericApi {
    let data: GenericApi;
    data = {
      nombre: (fd.nombre) ? fd.nombre :  '',
      enabled: (fd.enabled) ? true : false,
      params: (fd.params) ? this.extractParams(fd.params) : {},
    }

    // Load Only propertys that are setted
    if (undefined !== fd.apiKey) { data.apiKey = (fd.apiKey) ? fd.apiKey : '' }
    if (undefined !== fd.apiSecret) { data.apiSecret = (fd.apiSecret) ? fd.apiSecret : '' }
    if (undefined !== fd.cliente) { data.cliente = (fd.cliente) ? fd.cliente : '' }
    if (undefined !== fd.queryStringAuth) { data.queryStringAuth =  (fd.queryStringAuth) ? fd.queryStringAuth : '' }
    if (undefined !== fd.url) { data.url = (fd.url) ? fd.url : '' }
    if (undefined !== fd.version) { data.version = (fd.version) ? fd.version : '' }

    return data;
  }

  /**
   * Extracts params array into an Object
   * @param formParts 
   */
  extractParams(params: any[]) {
    let paramsObject: {[ s: string ]: string } = {};
    params.forEach(param => {
      paramsObject[param.key] = param.value;
    })
    return paramsObject;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
