/**
 * Objeto que representa la configuración básica de una API
 */
export interface Api {
  /** Nombre de la API: hablame, sendgrid, woocommerce, xredes, ... */
  nombre: string;
  /** Si la API está habilitada o no */
  enabled: boolean;
  /** Parámetros adicionales (pares key: value) */
  params: {[ s: string ]: string };
}

/**
 * Representa una api genérica en la que caben todas las demás
 */
export interface GenericApi extends Api {
  apiKey?: string;
  apiSecret?: string;
  cliente?: number;
  queryStringAuth?: boolean;
  url?: string;
  version?: string;
}

export interface GenericApiId extends GenericApi {
  id: string;
}

/**
 * Configuración de la API de hablame.co
 */
export interface HablameApi extends Api {
  /** Clave de la API */
  apiKey: string;
  /** Numero de cliente */
  cliente: number | null;
}

/**
 * Configuración de la API de Google Maps
 */
export interface GoogleMapsApi extends Api {
  /** Clave de la API */
  apiKey: string;
}

/**
 * Configuración de la API de WooCommerce
 */
export interface WoocommerceApi extends GenericApi {
  /** Equivalente to consumer key */
  apiKey: string,
  /** Equivalente to Consumer secret */
  apiSecret: string,
  /** Habilitar la autenticación vía querystring */
  queryStringAuth: boolean;
  /** URL del sitio donde está instalado WooCommerce */
  url: string;
  /** versión de la API */
  version: string;
}

/**
 * Configuración de la API DE Xredes
 */
export interface XredesApi extends Api {
  /** URL para hacer los queries. */
  url: string;
}