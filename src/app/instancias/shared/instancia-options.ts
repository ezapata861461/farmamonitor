export const strategyOptions = [
  { value: 'none', viewValue: 'Ninguno', icon: '' },
  { value: 'field', viewValue: 'Un Campo', icon: '' },
  { value: 'arrayField', viewValue: 'Un Array', icon: '' },
  { value: 'separatedCollection', viewValue: 'Colección Separada', icon: '' }
];
