/**
 * hablame SMS API config
 */
export interface HablameApiConfig {
  /** Clave de la api */
  api: string;
  /** Numero de cliente */
  cliente: number | null;
  /** Si la api está habilitada */
  enabled: boolean;
}

/**
 * WooCommerce API config
 */
export interface WoocommerceApiConfig {
  /** Consumer key */
  consumerKey: string,
  /** Consumer secret */
  consumerSecret: string,
  /** Si la api está habilitada */
  enabled: boolean;
  /** Habilitar la autenticación vía querystring */
  queryStringAuth: boolean;
  /** WooCommerce website URL */
  url: string;
  /** API version */
  version: string;
}

/**
 * Xredes (Contabilidad) API config
 */
export interface XredesApiConfig {
  /** URL para hacer los queries */
  url: string;
  /** Si la api está habilitada */
  enabled: true;
}