import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanciaFormComponent } from './instancia-form.component';

describe('InstanciaFormComponent', () => {
  let component: InstanciaFormComponent;
  let fixture: ComponentFixture<InstanciaFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanciaFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanciaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
