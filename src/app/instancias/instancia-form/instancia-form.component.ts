import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { InstanciaAuthoService } from "../shared/instancia-autho.service";
import { InstanciaService } from "../shared/instancia.service";

import { ErrorLogService } from '../../core/shared/error-log.service';
import { NotifyService } from '../../core/shared/notify.service';
import { LoadingService } from '../../core/shared/loading.service';
import { strategyOptions } from "../shared/instancia-options";
import { EmpresaService } from 'src/app/empresas/shared/empresa.service';
import { Observable } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';
import { SedeService } from 'src/app/sedes/shared/sede.service';

@Component({
  selector: 'win-instancia-form',
  templateUrl: './instancia-form.component.html',
  styleUrls: ['./instancia-form.component.scss']
})
export class InstanciaFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'empresas'

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  // Options
  strategyOpts = strategyOptions;

  // Form parts show/hide flags
  parts = {}

  // empresas observable for autocomplete
  empresas$: Observable<any>;
  sedes$: Observable<any>;

  constructor(
    protected router: Router,
    protected mainService: InstanciaService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    private fb: FormBuilder,
    public autho: InstanciaAuthoService,
    private dialog: MatDialog,
    protected empresaService: EmpresaService,
    protected sedeService: SedeService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = (this.autho.canCreate && '0' === this.mainService.id) || (this.autho.canEdit && '0' !== this.mainService.id);
    this.isSuperUser = this.autho.hasRol(['webmaster', 'admin']);
    this.isWebmaster = this.autho.hasRol(['webmaster']);

    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      empresa: [{ value: '', disabled: !this.canCreateOrEdit }],
      empresaAux: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required
      ]],
      empresaId: [{ value: '', disabled: !this.canCreateOrEdit }],
      nombre: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required,
        Validators.minLength(3),
      ]],
      sede: [{ value: '', disabled: !this.canCreateOrEdit }],
      sedeAux: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required
      ]],
      sedeId: [{ value: '', disabled: !this.canCreateOrEdit }],
      fCreado: [{ value: null, disabled: !this.isSuperUser }],
      creadorId: [{ value: '', disabled: !this.isSuperUser }],
      fModificado: [{ value: null, disabled: !this.isSuperUser }],
      modificadorId: [{ value: '', disabled: !this.isSuperUser }]
    });
  }

  /**
   * Load controls
   */
  loadControls(): void {
    this.empresas$ = this.empresaAux.valueChanges
      .pipe(
        debounceTime(300),
        switchMap((value: string) => this.empresaService.search(value, 'razonSocial'))
      );
    this.sedes$ = this.sedeAux.valueChanges
      .pipe(
        debounceTime(300),
        switchMap((value: string) => this.sedeService.search(value, 'nombre', 'empresaId', this.empresaId.value))
      );
  }

  /**
   * Display function for empresaAutocomplete
   * @param empresa 
   */
  displayEmpresa(empresa: any) {
    if (empresa) {
      if ('string' === (typeof empresa)) {
        return empresa;
      } else {
        return empresa.razonSocial
      }
    }
  }

  /**
   * Display function for sedeAutocomplete
   * @param sede 
   */
  displaySede(sede: any) {
    if (sede) {
      if ('string' === (typeof sede)) {
        return sede;
      } else {
        return sede.nombre
      }
    }
  }

  /**
   * Empresa selected (autocomplete) event
   * @param $event 
   */
  empresaSelected($event) {
    const empresa = $event.option.value; 
    this.empresa = empresa.razonSocial;
    this.empresaId = empresa.id;
  }

  /**
   * Sede selected (autocomplete) event
   * @param $event 
   */
  sedeSelected($event) {
    const sede = $event.option.value; 
    this.sede = sede.nombre;
    this.sedeId = sede.id;
  }

  /**
   * On Empresa select event
   * @param $event 
   * @bug Select is emitting unexpected on doubleClick while on readOnly state. Conditional force exit if event is empty.
   */
  onEmpresaSelect($event) {
    if (!$event.id) { return; }
    // this.empresa.setValue($event.razonSocial);
  }

  /**
   * On Sede select event
   * @param $event 
   * @bug Select is emitting unexpected on doubleClick while on readOnly state. Conditional force exit if event is empty.
   */
  onSedeSelect($event) {
    if (!$event.id) { return; }
    // this.sede.setValue($event.nombre);
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;

      this.empresaAux = (data.empresa) ? data.empresa : '';
      this.empresa = (data.empresa) ? data.empresa : '';
      this.empresaId = data.empresaId;

      this.nombre = data.nombre;

      this.sedeAux = (data.sede) ? data.sede : '';
      this.sede = (data.sede) ? data.sede : '';
      this.sedeId = data.sedeId;
      
      this.fCreado = data.fCreado;
      this.creadorId = data.creadorId;
      this.fModificado = data.fModificado;
      this.modificadorId = data.modificadorId;
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get empresa() {
    return this.form.get('empresa');
  }

  set empresa(empresa: any) {
    this.empresa.setValue(empresa);
  }

  get empresaAux() {
    return this.form.get('empresaAux');
  }

  set empresaAux(empresaAux: any) {
    this.empresaAux.setValue(empresaAux);
  }

  get empresaId() {
    return this.form.get('empresaId');
  }

  set empresaId(empresaId) {
    this.empresaId.setValue(empresaId);
  }

  get nombre() {
    return this.form.get('nombre');
  }

  set nombre(nombre) {
    this.nombre.setValue(nombre);
  }

  get sede() {
    return this.form.get('sede');
  }

  set sede(sede: any) {
    this.sede.setValue(sede);
  }

  get sedeAux() {
    return this.form.get('sedeAux');
  }

  set sedeAux(sedeAux: any) {
    this.sedeAux.setValue(sedeAux);
  }

  get sedeId() {
    return this.form.get('sedeId');
  }

  set sedeId(sedeId) {
    this.sedeId.setValue(sedeId);
  }
}
