import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { PersonaAuthoService } from "../shared/persona-autho.service";
import { AuthService } from 'src/app/auth/shared/auth.service';

@Component({
  selector: 'app-persona-details',
  templateUrl: './persona-details.component.html',
  styleUrls: ['./persona-details.component.scss']
})
export class PersonaDetailsComponent {

  // Route slug
  slug: string = 'personas';

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: PersonaAuthoService,
  ) { }

  /**
   * On form creation event
   * @param $event 
   */
  onCreated($event) {
    this.router.navigate([this.auth.instancia, this.slug, $event]);
  }

  /**
   * On form update event
   * @param $event 
   */
  onUpdated($event) {}

  /**
   * On form delete event
   * @param $event 
   */
  onDeleted() {
    this.router.navigate([this.auth.instancia, this.slug]);
  }
}
