import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import * as _ from "lodash";

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { PersonaAuthoService } from "../shared/persona-autho.service";
import { PersonaService } from "../shared/persona.service";
import { PersonaExistsValidator } from '../shared/persona-exists.validator';
import { cargoOptions, documentoOptions, relacionOptions, shoppingOptions, 
  subscriptionOptions, telefonoOptions } from "../shared/persona-options";

import { ErrorLogService } from '../../core/shared/error-log.service';
import { NotifyService } from '../../core/shared/notify.service';
import { LoadingService } from '../../core/shared/loading.service';
import { AuthService } from '../../auth/shared/auth.service';
import { EmpresaId } from 'src/app/empresas/shared/empresa';

@Component({
  selector: 'win-persona-form',
  templateUrl: './persona-form.component.html',
  styleUrls: ['./persona-form.component.scss']
})
export class PersonaFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'personas';

  /**
   * Parent empresa for empleado (asociated empresa and empresaId)
   */
  @Input()
  set parentEmpresa(parentEmpresa: EmpresaId|null) { this._parentEmpresa = parentEmpresa }
  get parentEmpresa(): EmpresaId|null { return this._parentEmpresa }
  private _parentEmpresa: EmpresaId|null = null;

  // Options
  cargoOpts = cargoOptions;
  documentoOpts = documentoOptions;
  relacionOpts = relacionOptions;
  shoppingOpts = shoppingOptions;
  subscriptionOpts = subscriptionOptions;
  telefonoOpts = telefonoOptions;

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  // Form parts show/hide flags
  parts = {
    deprecated: false,
    instancias: false,
    interacciones: false,
    registro: false,
    relacion: false,
    tags: false,
    usuario: false
  }

  // Separators for tags
  tagsSeparatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(
    protected router: Router,
    public mainService: PersonaService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    protected afs: AngularFirestore,
    protected fb: FormBuilder,
    protected auth: AuthService,
    public autho: PersonaAuthoService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = (this.autho.canCreate && '0' === this.mainService.id) || (this.autho.canEdit && '0' !== this.mainService.id);
    this.isSuperUser = this.autho.hasRol(['webmaster', 'admin']);
    this.isWebmaster = this.autho.hasRol(['webmaster']);

    // Get form config and hide/show form parts
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        const conf = config.modulos['personas'].formParts;
        this.parts = {
          deprecated: this.isWebmaster || conf.deprecated,
          instancias: this.isWebmaster || conf.instancias,
          interacciones: this.isWebmaster || conf.interacciones,
          registro: this.isWebmaster || conf.registro,
          relacion: this.isWebmaster || conf.relacion,
          tags: this.isWebmaster || conf.tags,
          usuario: this.isSuperUser
        }
      })
    )

    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      apellido: [{ value: '', disabled: !this.canCreateOrEdit }],
      cargo: [{ value: '', disabled: !this.canCreateOrEdit }],
      ciudad: [{ value: 'Medellín', disabled: !this.canCreateOrEdit }],
      direccion: [{ value: '', disabled: !this.canCreateOrEdit }],
      documento: [{ value: '', disabled: !this.canCreateOrEdit }],
      email: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required,
        Validators.email
      ]],
      emails: this.fb.array([]),
      empresa: [{ value: '', disabled: !this.canCreateOrEdit }],
      empresaId: [{ value: '', disabled: !this.canCreateOrEdit }],
      fNacimiento: [{ value: null, disabled: !this.canCreateOrEdit }],
      fUltimaCompra: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      fUltimaReserva: [{ value: null, disabled: !this.isSuperUser }],
      fUltimaVisita: [{ value: null, disabled: !this.isSuperUser }],
      movil: [{ value: '', disabled: !this.canCreateOrEdit }],
      nombre: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required,
        Validators.minLength(3),
      ]],
      observaciones: [{ value: '', disabled: !this.canCreateOrEdit }],
      relacion: [{ value: '', disabled: !this.canCreateOrEdit }],
      responsable: [{ value: '', disabled: !this.canCreateOrEdit }],
      sede: [{ value: '', disabled: !this.canCreateOrEdit }],
      sedeId: [{ value: '', disabled: !this.canCreateOrEdit }],
      shopping: [{ value: '', disabled: !this.isSuperUser }],
      subscription: [{ value: '', disabled: !this.isSuperUser }],
      synced: [{ value: null, disabled: !this.canCreateOrEdit }],
      tags: [{ value: [], disabled: !this.canCreateOrEdit }],
      telefonos: this.fb.array([]),
      tipoDocumento: [{ value: 'CC', disabled: !this.canCreateOrEdit }],
      uid: [{ value: '', disabled: !this.isSuperUser }],
      fCreado: [{ value: null, disabled: !this.isSuperUser }],
      creadorId: [{ value: '', disabled: !this.isSuperUser }],
      fModificado: [{ value: null, disabled: !this.isSuperUser }],
      modificadorId: [{ value: '', disabled: !this.isSuperUser }],
      instancia: [{ value: '', disabled: !this.isSuperUser }],
      instancias: this.fb.array([]),
      /** @deprecated */
      creado: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      creador: [{ value: '', disabled: !this.isSuperUser }],
      /** @deprecated */
      modificado: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      modificador: [{ value: '', disabled: !this.isSuperUser }],
    });
  }

  /**
   * Load controls
   */
  loadControls(): void {
    // Disable 'empresaId' field for freelancers 
    this.subscriptions.push(
      this.relacion.valueChanges
        .subscribe(relacion => {
          if ('freelance' === relacion || 'otro' === relacion || 'directo' === relacion) {
            this.empresa.setValue('');
            this.empresaId.setValue('');
            this.empresaId.disable();
            this.empresaId.clearValidators();
            this.sede.setValue('');
            this.sedeId.setValue('');
            this.sedeId.disable();
            this.sedeId.clearValidators();
          } else if ('agente'=== relacion) {
            this.empresaId.enable();
            this.empresaId.setValidators([Validators.required]);
          } else {
            this.empresaId.enable();
            this.empresaId.clearValidators();
          }
          this.empresaId.updateValueAndValidity();
          this.sedeId.updateValueAndValidity();
        })
    );

    this.subscriptions.push(
      this.empresaId.valueChanges
      .subscribe(empresaId => {
        if ('' !== empresaId) {
          this.sedeId.enable();
          this.sedeId.setValidators([Validators.required]);
        } else {
          this.sede.setValue('');
          this.sedeId.setValue('');
          this.sedeId.disable();
          this.sedeId.clearValidators();
        }
      })
    );
  }

  /**
   * On Empresa select event
   * @param $event 
   * @bug Select is emitting unexpected on doubleClick while on readOnly state. Conditional force exit if event is empty.
   */
  onEmpresaSelect($event) {
    if (!$event.id) { return; }
    this.empresa.setValue($event.razonSocial);
  }

  /**
   * On Sede select event
   * @param $event 
   * @bug Select is emitting unexpected on doubleClick while on readOnly state. Conditional force exit if event is empty.
   */
  onSedeSelect($event) {
    if (!$event.id) { return; }
    this.sede.setValue($event.nombre);
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.apellido = data.apellido;
      this.cargo = data.cargo;
      this.ciudad = data.ciudad;
      this.direccion = data.direccion;
      this.documento = data.documento;
      this.email = data.email;

      this.emails.clear();
      if (data.emails && data.emails.length > 0) {
        data.emails.forEach(element => {
          this.addEmail(element);
        });
      }

      // Load parent empresa on creation
      if ('0' === this.id.value) { 
        if (null !== this.parentEmpresa) {
          this.empresa = this.parentEmpresa.razonSocial;
          this.empresaId = this.parentEmpresa.id;
        }
      } else {
        this.empresa = data.empresa;
        this.empresaId = data.empresaId;
      }


      this.fNacimiento = data.fNacimiento;
      this.fUltimaCompra = data.fUltimaCompra;
      /** @deprecated */
      this.fUltimaReserva = data.fUltimaReserva;
      this.fUltimaVisita =  data.fUltimaVisita;
      this.movil = data.movil;
      this.nombre = data.nombre;
      this.observaciones = data.observaciones;
      this.relacion = data.relacion;
      this.responsable = data.responsable;
      this.sede = data.sede;
      this.sedeId = data.sedeId;
      this.shopping = data.shopping;
      this.subscription = data.subscription;
      this.synced = data.synced;
      this.tags = data.tags;

      this.telefonos.clear();
      if (data.telefonos && data.telefonos.length > 0) {
        data.telefonos.forEach(element => {
          this.addTelefono(element);
        });
      }
      
      this.tipoDocumento = data.tipoDocumento;
      this.uid = data.uid;

      this.fCreado = data.fCreado;
      this.creadorId = data.creadorId;
      this.fModificado = data.fModificado;
      this.modificadorId = data.modificadorId;

      /** @deprecated */
      this.creado = data.creado;
      /** @deprecated */
      this.creador = data.creador;
      /** @deprecated */
      this.modificado = data.modificado;
      /** @deprecated */
      this.modificador = data.modificador;
      
      this.instancia = data.instancia;

      this.instancias.clear();
      if (data.instancias && data.instancias.length > 0) {
        data.instancias.forEach(element => {
          this.addInstancia(element);
        });
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get apellido() {
    return this.form.get('apellido');
  }

  set apellido(apellido) {
    this.apellido.setValue(apellido);
  }

  get cargo() {
    return this.form.get('cargo');
  }

  set cargo(cargo) {
    this.cargo.setValue(cargo);
  }

  get ciudad() {
    return this.form.get('ciudad');
  }

  set ciudad(ciudad) {
    this.ciudad.setValue(ciudad);
  }

  get direccion() {
    return this.form.get('direccion');
  }

  set direccion(direccion) {
    this.direccion.setValue(direccion);
  }

  get documento() {
    return this.form.get('documento');
  }

  set documento(documento) {
    this.documento.setValue(documento);
  }

  get email() {
    return this.form.get('email');
  }

  set email(email) {
    this.email.setAsyncValidators(PersonaExistsValidator.email(this.auth, this.afs, this.form));
    this.email.setValue(email);
  }

  get emails(): FormArray {
    return this.form.get('emails') as FormArray;
  }

  addEmail(email = '') {
    const element = this.fb.group({
      email: [{ value: email, disabled: !this.canCreateOrEdit }, [
        Validators.email,
        Validators.required
      ]],
    });
    this.emails.push(element);
  }

  deleteEmail(i) {
    this.emails.removeAt(i);
    this.form.markAsTouched();
  }

  get empresa() {
    return this.form.get('empresa');
  }

  set empresa(empresa: any) {
    this.empresa.setValue(empresa);
  }

  get empresaId() {
    return this.form.get('empresaId');
  }

  set empresaId(empresaId: any) {
    this.empresaId.setValue(empresaId);
  }

  get fNacimiento() {
    return this.form.get('fNacimiento');
  }

  set fNacimiento(fNacimiento: any) {
    if (null != fNacimiento && '' != fNacimiento) {
      const d = new Date(fNacimiento.seconds * 1000);
      this.fNacimiento.setValue(d);
    } else {
      this.fNacimiento.setValue(null);
    }
  }

  get fUltimaCompra() {
    return this.form.get('fUltimaCompra');
  }

  set fUltimaCompra(fUltimaCompra: any) {
    if (null != fUltimaCompra && '' != fUltimaCompra) {
      const d = new Date(fUltimaCompra.seconds * 1000);
      this.fUltimaCompra.setValue(d);
    } else {
      this.fUltimaCompra.setValue(null);
    }
  }

  /** @deprecated */
  get fUltimaReserva() {
    return this.form.get('fUltimaReserva');
  }

  /** @deprecated */
  set fUltimaReserva(fUltimaReserva: any) {
    if (null != fUltimaReserva && '' != fUltimaReserva) {
      const d = new Date(fUltimaReserva.seconds * 1000);
      this.fUltimaReserva.setValue(d);
    } else {
      this.fUltimaReserva.setValue(null);
    }
  }
  
  get fUltimaVisita() {
    return this.form.get('fUltimaVisita');
  }

  set fUltimaVisita(fUltimaVisita: any) {
    if (null != fUltimaVisita && '' != fUltimaVisita) {
      const d = new Date(fUltimaVisita.seconds * 1000);
      this.fUltimaVisita.setValue(d);
    } else {
      this.fUltimaVisita.setValue(null);
    }
  }

  get movil() {
    return this.form.get('movil');
  }

  set movil(movil: any) {
    this.movil.setValue(this.formatPhone(movil));
  }

  get nombre() {
    return this.form.get('nombre');
  }

  set nombre(nombre) {
    this.nombre.setValue(nombre);
  }

  get observaciones() {
    return this.form.get('observaciones');
  }

  set observaciones(observaciones) {
    this.observaciones.setValue(observaciones);
  }

  get relacion() {
    return this.form.get('relacion');
  }

  set relacion(relacion) {
    this.relacion.setValue(relacion);
  }
  
  get responsable() {
    return this.form.get('responsable');
  }

  set responsable(responsable) {
    this.responsable.setValue(responsable);
  }

  get sede() {
    return this.form.get('sede');
  }

  set sede(sede) {
    this.sede.setValue(sede);
  }

  get sedeId() {
    return this.form.get('sedeId');
  }

  set sedeId(sedeId) {
    this.sedeId.setValue(sedeId);
  }

  get shopping() {
    return this.form.get('shopping');
  }

  set shopping(shopping) {
    this.shopping.setValue(shopping);
  }

  get subscription() {
    return this.form.get('subscription');
  }

  set subscription(subscription) {
    this.subscription.setValue(subscription);
  }

  get synced() {
    return this.form.get('synced');
  }

  set synced(synced) {
    this.synced.setValue(synced);
  }

  get tags() {
    return this.form.get('tags');
  }

  set tags(tags) {
    this.tags.setValue(tags);
  }

  deleteTag(i) {
    this.tags.value.splice(i, 1);
    this.form.markAsTouched();
  }

  addTag($event): void {
    if (($event.value || '').trim()) {
      this.tags.value.push(_.kebabCase($event.value.trim()));
    }
    if ($event.input) { $event.input.value = '' }
    this.tags.setValue(_.uniq(this.tags.value));
    this.form.markAsTouched();
  }

  get telefonos(): FormArray {
    return this.form.get('telefonos') as FormArray;
  }

  addTelefono(telefono = {tipo: '', numero: ''}) {
    const element = this.fb.group({
      tipo: [telefono.tipo, [
        Validators.required
      ]],
      numero: [this.formatPhone(telefono.numero), [
        Validators.required
      ]]
    });
    this.telefonos.push(element);
  }

  deleteTelefono(i) {
    this.telefonos.removeAt(i);
    this.form.markAsTouched();
  }

  /**
   * Format phone (With spaces for readability)
   */
  formatPhone(string: string): string {
    const onlyD = string.replace(/[^\d]/g,'');
    const l = onlyD.length;
    // Country code + Celphone
    if (l > 10) {
      return onlyD.slice(0, l-10) +' '+ onlyD.slice(l-10, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Cellphone or phone with country and city code
    if (l >= 9) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone with city code
    if (l >= 8) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone
    if (l > 4) {
      return onlyD.slice(0, l-4) +' '+ onlyD.slice(l-4, l);
    } else {
      return onlyD;
    }
  }

  get tipoDocumento() {
    return this.form.get('tipoDocumento');
  }

  set tipoDocumento(tipoDocumento) {
    this.tipoDocumento.setValue(tipoDocumento);
  }

  get uid() {
    return this.form.get('uid');
  }

  set uid(uid) {
    this.uid.setValue(uid);
  }

  get instancia() {
    return this.form.get('instancia');
  }

  set instancia(instancia) {
    this.instancia.setValue(instancia);
  }

  get instancias(): FormArray {
    return this.form.get('instancias') as FormArray;
  }

  addInstancia(instancia = '') {
    const element = this.fb.group({
      instancia: [instancia, [
        Validators.required
      ]]
    });
    this.instancias.push(element);
  }

  deleteInstancia(i) {
    this.instancias.removeAt(i);
    this.form.markAsTouched();
  }

  /** @deprecated */
  get creado() {
    return this.form.get('creado');
  }

  /** @deprecated */
  set creado(creado: any) {
    if (null != creado) {
      const d = new Date(creado.seconds * 1000);
      this.creado.setValue(new Date(creado.seconds * 1000));
    } else {
      this.creado.setValue(null);
    }
  }

  /** @deprecated */
  get creador() {
    return this.form.get('creador');
  }

  /** @deprecated */
  set creador(creador) {
    this.creador.setValue(creador);
  }

  /** @deprecated */
  get modificado() {
    return this.form.get('modificado');
  }

  /** @deprecated */
  set modificado(modificado: any) {
    if (null != modificado) {
      const d = new Date(modificado.seconds * 1000);
      this.modificado.setValue(d);
    } else {
      this.modificado.setValue(null);
    }
  }

  /** @deprecated */
  get modificador() {
    return this.form.get('modificador');
  }

  /** @deprecated */
  set modificador(modificador) {
    this.modificador.setValue(modificador);
  }
}
