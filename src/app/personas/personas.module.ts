import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule } from "@angular/forms";

import { PersonasRoutingModule } from './personas-routing.module';
import { PersonasComponent } from './personas.component';
import { PersonaComponent } from './persona/persona.component';
import { PersonaListComponent } from './persona-list/persona-list.component';
import { PersonaGridComponent } from './persona-grid/persona-grid.component';
import { PersonaDetailsComponent } from './persona-details/persona-details.component';
import { PersonaFormComponent } from './persona-form/persona-form.component';
import { PersonaSelectComponent } from './persona-select/persona-select.component';

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { EmpresasModule } from '../empresas/empresas.module';
import { SedesModule } from '../sedes/sedes.module';
import { InstanciasModule } from "../instancias/instancias.module";

import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { UsuarioGridComponent } from '../usuarios/usuario-grid/usuario-grid.component';
import { EmpresaGridComponent } from '../empresas/empresa-grid/empresa-grid.component';
import { SedeGridComponent } from '../sedes/sede-grid/sede-grid.component';
import { InstanciaGridComponent } from "../instancias/instancia-grid/instancia-grid.component";

@NgModule({
  declarations: [
    PersonasComponent, 
    PersonaComponent, 
    PersonaListComponent, 
    PersonaGridComponent, 
    PersonaDetailsComponent, 
    PersonaFormComponent, 
    PersonaSelectComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    PersonasRoutingModule,
    CoreModule,
    SharedModule,
    AuthModule,
    UsuariosModule,
    EmpresasModule,
    SedesModule,
    InstanciasModule
  ],
  exports: [
    PersonaGridComponent,
    PersonaFormComponent,
    PersonaSelectComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    UsuarioGridComponent,
    EmpresaGridComponent,
    SedeGridComponent,
    InstanciaGridComponent
  ]
})
export class PersonasModule { }
