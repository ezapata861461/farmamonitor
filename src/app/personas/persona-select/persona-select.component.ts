import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, Output, EventEmitter, Optional, Self, OnDestroy } from '@angular/core';
import { FormBuilder, NgControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldControl } from '@angular/material/form-field';

import { AbsSelectComponent } from '../../core/abstract/abs-select.component';
import { PersonaGridComponent } from '../persona-grid/persona-grid.component';
import { PersonaService } from '../shared/persona.service';
import { PersonaId } from '../shared/persona';

@Component({
  selector: 'win-persona-select',
  templateUrl: './persona-select.component.html',
  styleUrls: ['./persona-select.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: PersonaSelectComponent }],
  host: {
    '[class.label-floating]': 'shouldLabelFloat',
    '[id]': 'id',
    '[attr.aria-describedby]': 'describedBy',
  }
})
export class PersonaSelectComponent extends AbsSelectComponent implements OnDestroy {

  // "description" field name (to extract it from data)
  descrFieldName = 'nombreCompleto';
  // Current selected document
  document: PersonaId;

  // Emited events
  @Output() load = new EventEmitter<PersonaId>();
  @Output() select = new EventEmitter<PersonaId>();

  // Custom input type and id
  controlType = 'win-persona-select';
  id = `win-persona-select-${PersonaSelectComponent.nextId++}`;

  constructor(
    fb: FormBuilder,
    protected mainService: PersonaService,
    protected fm: FocusMonitor,
    protected elRef: ElementRef<HTMLElement>,
    public dialog: MatDialog,
    @Optional() @Self() public ngControl: NgControl,
  ) {
    super(fb, mainService, fm, elRef, ngControl);
  }

  /**
   * Open select popup and populate fields on select
   */
  openSelect() {
    const dialog = this.dialog.open(PersonaGridComponent, {data: {}, width: '80%', height: '80%'});
    dialog.componentInstance.mode = 'select';
    dialog.componentInstance.isPopup = true;
    dialog.disableClose = true;
    this.subscriptions.push(
      this.dialogCloseSub = dialog.componentInstance.close.subscribe(() => { dialog.close() })
    );

    this.subscriptions.push(
      this.dialogSub = dialog.componentInstance.select.subscribe(document => {
        // Keep selected document
        this.document = document;
  
        // Load data into form fields
        this.inputId = document.id;
        this.inputDescr = document[this.descrFieldName];
  
        // Emit selected document
        this.select.emit(document);
  
        // Notify change and run change detection
        this.onChange(document.id);
        this.stateChanges.next();
        
        // Close select dialog
        dialog.close();
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
