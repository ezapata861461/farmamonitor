import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaSelectComponent } from './persona-select.component';

describe('PersonaSelectComponent', () => {
  let component: PersonaSelectComponent;
  let fixture: ComponentFixture<PersonaSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
