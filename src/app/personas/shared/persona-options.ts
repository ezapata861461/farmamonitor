export const cargoOptions = [
  { value: 'administrativo', viewValue: 'Administrativo', icon: '' },
  { value: 'comercial', viewValue: 'Comercial Externo', icon: '' },
  { value: 'gerente', viewValue: 'Gerente', icon: '' },
  { value: 'internet', viewValue: 'Encargado Internet', icon: '' },
  { value: 'jefeOficina', viewValue: 'Jefe de Oficina', icon: '' },
  { value: 'operativo', viewValue: 'Asesor Comercial', icon: '' },
  { value: 'otro', viewValue: 'Otro', icon: '' }
];

export const documentoOptions = [
  { value: 'CC', viewValue: 'Cédula de Ciudadanía', icon: '' },
  { value: 'CE', viewValue: 'Cédula de Extranjería', icon: '' },
  { value: 'PP', viewValue: 'Pasaporte', icon: '' },
  { value: 'RC', viewValue: 'Registro Civil (NUIP)', icon: '' },
  { value: 'TI', viewValue: 'Tarjeta de Identidad' }
];

export const relacionOptions = [
  { value: 'agente', viewValue: 'Cliente Agente', icon: '' },
  { value: 'freelance', viewValue: 'Freelance', icon: '' },
  { value: 'directo', viewValue: 'Cliente Directo', icon: '' },
  { value: 'proveedor', viewValue: 'Proveedor', icon: '' },
  { value: 'otro', viewValue: 'Otro', icon: '' }
];

export const shoppingOptions = [
  { value: 'recent', viewValue: 'Reciente', icon: '' },
  { value: 'repeat', viewValue: 'Repetitivo', icon: '' },
  { value: 'lapsed', viewValue: 'Ausente', icon: '' },
  { value: 'never', viewValue: 'Nunca', icon: '' },
];

export const subscriptionOptions = [
  { value: 'subscribed', viewValue: 'Subscrito', icon: '' },
  { value: 'unsubscribed', viewValue: 'Des-suscrito', icon: '' },
  { value: 'nonsubscribed', viewValue: 'No suscrito', icon: '' },
  { value: 'unwanted', viewValue: 'No deseado', icon: '' },
  { value: 'cleaned', viewValue: 'Limpio', icon: '' }
];

export const telefonoOptions = [
  { value: 'movil', viewValue: 'Móvil', icon: 'smartphone' },
  { value: 'fijo', viewValue: 'Fijo', icon: 'phone' }
];