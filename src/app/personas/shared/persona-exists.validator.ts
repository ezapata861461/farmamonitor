import { map, take, debounceTime } from "rxjs/operators";
import { FormGroup, AbstractControl } from "@angular/forms";
import { AngularFirestore } from "@angular/fire/firestore";

import { AuthService } from 'src/app/auth/shared/auth.service';
import { PersonaId } from "../shared/persona";

/**
 * Validates 'persona' is unique by email
 */
export class PersonaExistsValidator {
  static email(auth: AuthService, afs: AngularFirestore, form: FormGroup) {
    return (control: AbstractControl) => {
      const email = control.value.toLowerCase();
      return afs.collection('personas', ref => ref.where('email', '==', email).where('instancias', 'array-contains', auth.instancia))
        .snapshotChanges().pipe(
          debounceTime(500),
          take(1),
          map(items => {
            let data = items.map(a => {
              const data = a.payload.doc.data() as PersonaId;
              const id = a.payload.doc.id;
              return { id, ...data };
            });

            if (data.length && '' !== form.value.email && (0 === form.value.id || data[0].id !== form.value.id)) {
              return { personaExists: data[0].nombreCompleto };
            }
            return null;
          })
        );
    }
  }
}