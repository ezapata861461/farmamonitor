import { TestBed, inject } from '@angular/core/testing';

import { PersonaAuthoService } from './persona-autho.service';

describe('PersonaAuthoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonaAuthoService]
    });
  });

  it('should be created', inject([PersonaAuthoService], (service: PersonaAuthoService) => {
    expect(service).toBeTruthy();
  }));
});
