import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { Persona, PersonaId } from "./persona";

@Injectable({
  providedIn: 'root'
})
export class PersonaService extends AbsFirestoreService implements OnDestroy {

  // Collection prefix
  collectionPrefix = '';
  // Collection name
  collectionName = 'personas';
  // Current loaded collection
  col$: Observable<PersonaId[]>;
  // Current loaded document
  doc$: BehaviorSubject<PersonaId | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
  ) {
    super(afs, auth);

    // Init current collection and current object
    this.initCol();
    this.initDoc();
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as PersonaId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<PersonaId>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): PersonaId {
    return {
      id: '0',
      apellido: '',
      cargo: '',
      ciudad: '',
      direccion: '',
      documento: '',
      email: '',
      emails: [],
      empresa: '',
      empresaId: '',
      fNacimiento: null,
      fUltimaCompra: null,
      fUltimaReserva: null,
      fUltimaVisita: null,
      movil: '',
      nombre: '',
      nombreCompleto: '',
      observaciones: '',
      relacion: '',
      responsable: '',
      sede: '',
      sedeId: '',
      shopping: 'never',
      subscription: 'nonsubscribed',
      synced: null,
      tags: [],
      telefonos: [],
      tipoDocumento: 'CC',
      uid: '',
      fCreado: null,
      creadorId: '',
      fModificado: null,
      modificadorId: '',
      instancia: this.auth.instancia,
      instancias: [ this.auth.instancia ],
      /** @deprecated */
      creado: null,
      /** @deprecated */
      creador: '',
      /** @deprecated */
      modificado: null,
      /** @deprecated */
      modificador: '' 
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): Persona {
    let data: Persona;
    data = {
      apellido: (fd.apellido) ? _.trim(fd.apellido) : '',
      cargo: (fd.cargo) ? fd.cargo :  '',
      ciudad: (fd.ciudad) ? _.trim(fd.ciudad) :  '',
      direccion: (fd.direccion) ? _.trim(fd.direccion) : '',
      documento: (fd.documento) ? fd.documento.replace(/[^a-z0-9]/gi,'') : '',
      email: (fd.email) ? fd.email.toLowerCase() : '',
      emails: (fd.emails && 0 <= fd.emails.length) ? fd.emails.map(el => { el = el.email.toLowerCase(); return el; }) : [],
      empresa: (fd.empresa) ? fd.empresa : '',
      empresaId: (fd.empresaId) ? fd.empresaId : '',
      fNacimiento: (fd.fNacimiento && '' !== fd.fNacimiento) ? fd.fNacimiento : null,
      fUltimaCompra: (fd.fUltimaCompra && '' !== fd.fUltimaCompra) ? fd.fUltimaCompra : null,
      fUltimaReserva: (fd.fUltimaReserva && '' !== fd.fUltimaReserva) ? fd.fUltimaReserva : null,
      fUltimaVisita: (fd.fUltimaVisita && '' !== fd.fUltimaVisita) ? fd.fUltimaVisita : null,
      movil: (fd.movil) ? fd.movil.replace(/[^\d]/g,'') : '',
      nombre: (fd.nombre) ? _.trim(fd.nombre) : '',
      nombreCompleto: ((fd.nombre) ? _.trim(fd.nombre) : '') +' '+ ((fd.apellido) ? _.trim(fd.apellido) : ''),
      observaciones: (fd.observaciones) ? _.trim(fd.observaciones) : '',
      relacion: (fd.relacion) ? fd.relacion : '',
      responsable: (fd.responsable && '' !== fd.responsable) ? fd.responsable : this.auth.getUid(), 
      sede: (fd.sede) ? fd.sede : '',
      sedeId: (fd.sedeId) ? fd.sedeId : '',
      shopping: (fd.shopping) ? fd.shopping : 'never',
      subscription: (fd.subscription) ? fd.subscription : 'nonsubscribed',
      synced: (fd.synced && '' !== fd.synced) ? fd.synced : null,
      tags: (fd.tags && 0 <= fd.tags.length) ? fd.tags.map(el => { el = el.toLowerCase().replace(/[^a-z\-]/gi,''); return el; }) : [],
      telefonos: (fd.telefonos && 0 <= fd.telefonos.length) ? fd.telefonos.map(el => { el.numero = el.numero.replace(/[^\d]/g,''); return el; }) : [],
      tipoDocumento: (fd.tipoDocumento && '' !== fd.tipoDocumento) ? fd.tipoDocumento : 'CC',
      uid: (fd.uid) ? fd.uid : '',
      fCreado: (fd.fCreado && '' !== fd.fCreado) ? fd.fCreado : this.timestamp,
      creadorId: (fd.creadorId && '' !== fd.creadorId) ? fd.creadorId : this.auth.getUid(),
      fModificado: this.timestamp,
      modificadorId: this.auth.getUid(),
      instancia: (fd.instancia) ? fd.instancia : this.auth.instancia,
      instancias: (fd.instancias && '' !== fd.instancias && 0 < fd.instancias.length) ? fd.instancias.map(el => { el = el.instancia; return el; }) : [ this.auth.instancia ],
      /** @deprecated */
      creado: (fd.creado && '' !== fd.creado) ? fd.creado : this.timestamp,
      /** @deprecated */
      creador: (fd.creador && '' !== fd.creador) ? fd.creador : this.auth.getUid(),
      /** @deprecated */
      modificado: this.timestamp,
      /** @deprecated */
      modificador: this.auth.getUid(),
    }

    return data;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
