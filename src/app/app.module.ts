import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule }        from "./app-material/app-material.module";
import { FlexLayoutModule }         from "@angular/flex-layout";
import { registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import localeEsExtra from "@angular/common/locales/extra/es";

import { AppComponent } from './app.component';

import { AngularFireModule }        from "@angular/fire";
import { AngularFirestoreModule }   from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule }    from "@angular/fire/auth";
import { AngularFireFunctionsModule } from "@angular/fire/functions";
import { environment }              from "../environments/environment";

import { CoreModule } from "./core/core.module";
import { AuthModule } from "./auth/auth.module";
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FullCalendarModule } from "@fullcalendar/angular";

export const firebaseConfig = environment.firebase;
registerLocaleData(localeEs, 'es', localeEsExtra);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireFunctionsModule,
    CoreModule,
    AuthModule,
    NgxMaterialTimepickerModule,
    FullCalendarModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
