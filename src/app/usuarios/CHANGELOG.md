# Changelog

## [0.0.9] - 2019-12-13

### Changed
* Remove 'wembaster' role from options and add it dinamically on user.form.html.
* Improve usuario.form behaviour to be more secure for webmaster and admin roles.

## [0.0.8] - 2019-12-08

### Changed
* Show 'instancias' in form.
* Update how formParts configuration is retrieved (from modulos array field).

## [0.0.7] - 2019-11-22

## Fixed
* Fix mainService in form (Has to be public).

## [0.0.6] - 2019-11-19

### Changed
* Modify authorized roles in usuario-autho service.

### Fixed
* Fix bug with rol disable/enable.

## [0.0.5] - 2019-11-12

### Added
* Import InstanciasModule.

### Changed
* Add instancia field to model. Required to work with instanciaStrategy.
* Improve grid mat-toolbar.
* Remove bottom paginator.
* Catch memory leaks (subrcriptions).
* Force user diable before role modification, to force session closing.

### Fixed
* Force select popup to be closed only with close button.

## [0.0.4] - 2019-10-18

### Fixed
* Fix bug with matdatepicker.
* Init parts default options.

## [0.0.3] - 2019-10-17

### Changed
* Rename abstract classes to avoid legacy problems.
* Rename confirm-modal to confirm-dialog to avoid legacy problems.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.

### Changed
* Improve interface code and docs.

### Removed
* Remove unused interface Usuario and UsuarioId.

## [0.0.1] - 2019-10-10

* First commit.