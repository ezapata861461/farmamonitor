export const roleOptions = [
  /** Webmaster role is added dynamically on usuario-form.component.html */
  { value: 'admin', viewValue: 'Admin', icon: '' },
  { value: 'manager', viewValue: 'Gerente', icon: '' },
  { value: 'operations', viewValue: 'Operaciones', icon: '' },
  { value: 'sales', viewValue: 'Ventas', icon: '' },
  { value: 'accounting', viewValue: 'Contabilidad', icon: '' },
  { value: 'client', viewValue: 'Cliente', icon: '' },
  { value: 'direct', viewValue: 'Cliente Directo', icon: '' },
  { value: 'guest', viewValue: 'Invitado', icon: '' }
];