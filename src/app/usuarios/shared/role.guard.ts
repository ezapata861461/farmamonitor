import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, map, take } from "rxjs/operators";

import { AuthService } from "../../auth/shared/auth.service";
import { UsuarioAuthoService } from "./usuario-autho.service";
import { NotifyService } from '../../core/shared/notify.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate, CanActivateChild {
  constructor(
    private auth: AuthService, 
    private autho: UsuarioAuthoService, 
    private notify: NotifyService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.auth.user.pipe(
        take(1),
        map(() => this.autho.canRead),
        tap(athorized => {
          if (!athorized) {
            // If browser reloaded, send to login where redirect will be handled
            if (!this.router.navigated) {
              this.router.navigate(['/login']);
            // Otherwise, just notify
            } else {
              this.notify.update('El usuario no tiene suficientes privilegios.', 'error');
            }
          }
        })
      );
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(next, state);
  }
}
