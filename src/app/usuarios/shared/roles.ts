/**
 * Representa los roles que tiene un usuario. Funcionan forma jerarquica; un usario 'admin' cuenta 
 * también con los roles de los usuarios de menor jerarquía, como el 'editor' o el 'guest'.
 */
export interface Roles {
  /** Top Super User */
  webmaster: boolean;
  /** Administrative privileges. Is a superuser too, but whitout app level access. */
  admin: boolean;
  /** 'instancia' level user. Can change all inside an 'instancia'. Is a 'gerente'. */
  manager: boolean;
  /** 'instancia' level user. Common operative user inside app. */
  operations: boolean;
  /** 'instancia' level user. Reserved user for some sales roles. */
  sales: boolean;
  /** 'instancia' level user. Reserved user for some accounting roles. */
  accounting: boolean;
  /** 'instancia' level user. External client that belongs to a company (for example: a travel agent). */
  client: boolean;
  /** 'instancia' level user. External client that doesn´t belong to a company. A person (for example un cliente directo). */
  direct: boolean;
  /** Default role for a just signed in user. It's added to 'default' instancia. */
  guest: boolean;
}
