import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { AngularFireFunctions } from "@angular/fire/functions";
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { User, UserId } from "./user";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends AbsFirestoreService implements OnDestroy {

  // Collection prefix
  collectionPrefix = '';
  // Collection name
  collectionName: string = 'users';
  // Current loaded collection
  col$: Observable<UserId[]>;
  // Current loaded document
  doc$: BehaviorSubject<UserId | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
    private afFns: AngularFireFunctions
  ) {
    super(afs, auth);

    // Init current collection and current object
    this.initCol();
    this.initDoc();
  }

  /**
   * Request server to change user password
   * @param password 
   */
  changePassword(uid: string, password: string) {
    const userChangePassword = this.afFns.httpsCallable('userChangePassword');
    return userChangePassword({
      uid: uid,
      password: password
    }).toPromise();
  }

  /**
   * Request server to grant role to user
   * @param uid 
   * @param email 
   * @param role 
   */
  grantRole(uid: string, email: string, role: string) {
    const userGrantRole = this.afFns.httpsCallable('userGrantRole');
    return userGrantRole({
      uid: uid,
      email: email,
      role: role
    }).toPromise();
  }

  /**
   * Toggle disabled state for user
   * @param uid 
   * @param disabled 
   */
  disabledToggle(uid: string, disabled: boolean) {
    const userDisabledToggle = this.afFns.httpsCallable('userDisabledToggle');
    return userDisabledToggle({
      uid: uid,
      disabled: disabled
    }).toPromise();
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as UserId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<UserId>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): UserId {
    return {
      id: '0',
      uid: '',
      disabled: false,
      displayName: '',
      email: null,
      photoURL: '',
      personaId: '',
      roles: [],
      lastLogin: null,
    
      fCreado: null,
      creadorId: '',
      fModificado: null,
      modificadorId: '',
      instancia: this.auth.instancia,
      instancias: [ this.auth.instancia ],
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): User {
    let data: User;
    data = {
      uid: (fd.uid) ? _.trim(fd.uid) :  '',
      disabled: (fd.disabled) ? true : false,
      displayName: (fd.displayName) ? _.trim(fd.displayName) : '',
      email: (fd.email) ? fd.email.toLowerCase() : '',
      photoURL: (fd.photoURL) ? _.trim(fd.photoURL) : '',
      personaId: (fd.personaId) ? fd.personaId : '',
      roles: (fd.roles && '' !== fd.roles && 0 < fd.roles.length) ? fd.roles.map(el => { el = el.role; return el; }) : [ 'guest' ],
      lastLogin: (fd.lastLogin && '' !== fd.lastLogin) ? fd.lastLogin : null,
      fCreado: (fd.fCreado && '' !== fd.fCreado) ? fd.fCreado : this.timestamp,
      creadorId: (fd.creadorId && '' !== fd.creadorId) ? fd.creadorId : this.auth.getUid(),
      fModificado: this.timestamp,
      modificadorId: this.auth.getUid(),
      instancia: (fd.instancia) ? fd.instancia : this.auth.instancia,
      instancias: (fd.instancias && '' !== fd.instancias && 0 < fd.instancias.length) ? fd.instancias.map(el => { el = el.instancia; return el; }) : [ this.auth.instancia ]
    }
    return data;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
