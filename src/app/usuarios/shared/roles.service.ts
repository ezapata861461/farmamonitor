import { Injectable } from '@angular/core';
import { Roles } from './roles';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor() { }

  /**
   * Extract roles from custom claims
   * If empty, returns a guest role
   * @param claims 
   */
  getRolesFromClaims(claims): Roles {
    return {
      webmaster: claims.webmaster ? true : false,
      admin: claims.admin ? true : false,
      manager: claims.manager ? true : false,
      operations: claims.operations ? true : false,
      sales: claims.sales ? true : false,
      accounting: claims.accounting ? true : false,
      client: claims.client ? true : false,
      direct: claims.direct ? true : false,
      guest: true
    }
  }
}
