import { Injectable } from '@angular/core';

import { AbsAuthorizationService } from '../../auth/shared/abs-authorization.service';
import { AuthService } from "../../auth/shared/auth.service";

@Injectable({
  providedIn: 'root'
})
export class UsuarioAuthoService  extends AbsAuthorizationService {

  constructor(
    protected auth: AuthService,
  ) {
    super(auth);

    // Set allowed roles arrays
    this.readAllowedRoles =       [ 'admin' ];
    this.createAllowedRoles =     [ 'admin' ];
    this.editAllowedRoles =       [ 'admin' ];
    this.deleteAllowedRoles =     [ 'admin' ];
    this.softdeleteAllowedRoles = [ 'admin' ];
    this.fileAllowedRoles =       [ 'admin' ];
  }
}