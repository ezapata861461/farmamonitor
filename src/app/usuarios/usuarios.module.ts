import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule }      from "@angular/forms";

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuarioGridComponent } from './usuario-grid/usuario-grid.component';
import { UsuarioSelectComponent } from './usuario-select/usuario-select.component';
import { UsuariosComponent } from './usuarios.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { UsuarioDetailsComponent } from './usuario-details/usuario-details.component';
import { UsuarioFormComponent } from './usuario-form/usuario-form.component';
import { UsuarioListComponent } from './usuario-list/usuario-list.component';

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { InstanciasModule } from '../instancias/instancias.module';

import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { InstanciaGridComponent } from '../instancias/instancia-grid/instancia-grid.component';


@NgModule({
  declarations: [
    UsuarioGridComponent,
    UsuarioSelectComponent,
    UsuariosComponent,
    UsuarioComponent,
    UsuarioDetailsComponent,
    UsuarioFormComponent,
    UsuarioListComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    UsuariosRoutingModule,
    CoreModule,
    SharedModule,
    AuthModule,
    InstanciasModule
  ],
  exports: [
    UsuarioGridComponent,
    UsuarioFormComponent,
    UsuarioSelectComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    InstanciaGridComponent
  ]
})
export class UsuariosModule { }
