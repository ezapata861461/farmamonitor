import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleGuard } from "./shared/role.guard";
import { UsuariosComponent } from './usuarios.component';
import { UsuarioListComponent } from './usuario-list/usuario-list.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { UsuarioDetailsComponent } from './usuario-details/usuario-details.component';

const routes: Routes = [
  {
    path: '',
    component: UsuariosComponent,
    canActivate: [RoleGuard],
    children: [
      {
        path: '',
        component: UsuarioListComponent,
      },
      {
        path: ':id',
        component: UsuarioComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles',
          },
          {
            path: 'detalles',
            component: UsuarioDetailsComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
