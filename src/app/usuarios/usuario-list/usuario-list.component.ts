import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth/shared/auth.service';
import { UsuarioAuthoService } from "../shared/usuario-autho.service";
import { LoadingService } from "../../core/shared/loading.service";

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.scss']
})
export class UsuarioListComponent {

  // Route slug
  slug: string = 'usuarios';

  // Sidebar menu
  sidebar = [
    { name: 'Escritorio', icon: 'arrow_back', link: '../admin', disabled: false },
    { name: 'Lista de Usuarios', icon: 'account_circle', link: './' , disabled: false }
  ]

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: UsuarioAuthoService,
    private loading: LoadingService
  ) { }

  /**
   * Grid create event handler
   * @param $event 
   */
  onCreate() {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, 0]);
  }

  /**
   * Grid row click event handler
   * @param $event 
   */
  onSelect($event) {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, $event.id]);
  }
}
