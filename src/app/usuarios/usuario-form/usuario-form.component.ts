import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { UsuarioAuthoService } from "../shared/usuario-autho.service";
import { UsuarioService } from "../shared/usuario.service";
import { UsuarioExistsValidator } from '../shared/usuario-exists.validator';
import { roleOptions } from "../shared/role-options";
import { disabledOptions } from "../shared/user-options";

import { ErrorLogService } from '../../core/shared/error-log.service';
import { NotifyService } from '../../core/shared/notify.service';
import { LoadingService } from '../../core/shared/loading.service';
import { AuthService } from '../../auth/shared/auth.service';

@Component({
  selector: 'win-usuario-form',
  templateUrl: './usuario-form.component.html',
  styleUrls: ['./usuario-form.component.scss']
})
export class UsuarioFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'users'

  // Options
  roleOpts = roleOptions;
  disabledOpts = disabledOptions;

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  canDelete: boolean = false;
  isAdmin: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  // Form parts show/hide flags
  parts = {
    disable: false,
    instancias: false,
    password: false,
    registro: false,
    roles: false
  }

  constructor(
    protected router: Router,
    public mainService: UsuarioService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    private afs: AngularFirestore,
    private fb: FormBuilder,
    private auth: AuthService,
    public autho: UsuarioAuthoService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = (this.autho.canCreate && '0' === this.mainService.id) || (this.autho.canEdit && '0' !== this.mainService.id);
    this.canDelete = this.autho.canDelete;
    this.isAdmin = this.autho.hasRol(['admin']) && !this.autho.hasRol(['webmaster']);
    this.isSuperUser = this.autho.hasRol(['webmaster', 'admin']);
    this.isWebmaster = this.autho.hasRol(['webmaster']);

    // Get form config and hide/show form parts
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        const conf = config.modulos['users'].formParts;
        this.parts = {
          disable: this.isWebmaster || conf.disable,
          instancias: this.isWebmaster, /** Solo un webmaster puede dar acceso a otras instancias. @todo: Check rules and validations. */
          password: this.isWebmaster || conf.password,
          registro: this.isWebmaster || conf.registro,
          roles: this.isSuperUser /** Solo un webmaster o un admin pueden modificar los roles. @todo: Check rules and validations. */
        }
      })
    );

    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      uid: [{ value: '', disabled: true }],
      disabled: [{ value: false, disabled: !this.isWebmaster }],
      displayName: [{ value: '', disabled: !this.canCreateOrEdit }, // Owner can edit
        Validators.required
      ],
      email: [{ value: '', disabled: !this.isWebmaster }, [
        Validators.required,
        Validators.email
      ]],
      photoURL: [{ value: '', disabled: !this.isWebmaster }],
      personaId: [{ value: '', disabled: !this.isWebmaster }],
      role: [{ value: '', disabled: true }],
      roles: this.fb.array([]),
      lastLogin: [{ value: null, disabled: !this.isWebmaster }],
      password: [{ value: '', disabled: !this.isWebmaster }],
      fCreado: [{ value: null, disabled: !this.isWebmaster }],
      creadorId: [{ value: '', disabled: !this.isWebmaster }],
      fModificado: [{ value: null, disabled: !this.isWebmaster }],
      modificadorId: [{ value: '', disabled: !this.isWebmaster }],
      instancia: [{ value: '', disabled: !this.isWebmaster }],
      instancias: this.fb.array([])
    });
  }

  /**
   * Load controls
   */
  loadControls(): void {
    this.roles.valueChanges.subscribe(() => {
      if (this.roles.value[0]) {
        if ('webmaster' === this.roles.value[0].role && this.isAdmin) {
          this.canCreateOrEdit = false;
          this.canDelete = false;
          this.displayName.disable();
          this.email.disable();
          this.password.disable();
          this.role.disable();
          this.fCreado.disable();
          this.creadorId.disable();
          this.fModificado.disable();
          this.modificadorId.disable();
        } else if ('webmaster' !== this.roles.value[0].role && this.isAdmin) {
          this.canCreateOrEdit = true;
          this.canDelete = true;
          this.displayName.enable();
          this.email.enable();          
          this.password.enable();
          if (this.disabled.value) {  // Watch disabled switch
            this.role.enable();
          }
          this.fCreado.enable();
          this.creadorId.enable();
          this.fModificado.enable();
          this.modificadorId.enable();
        }
      }
    })
  }

  /**
   * Change password
   * @param password 
   */
  changePassword() {
    this.onLoading();
    this.mainService.changePassword(this.uid.value, this.password.value)
      .then(result => {
        /**
         * @todo: catch error. Check for { error: '...' }. It comes as result!
         */
        this.notify.update('Contraseña actualizada exitosamente.', 'success');
        this.onLoaded();
      })
      .catch(error => { 
        this.notify.update('Error al cambiar la contraseña. Por favor intente de nuevo.', 'error');
        this.onLoaded();
      });
  }

  /**
   * Grant role to user
   */
  grantRole() {
    this.onLoading();
    this.mainService.grantRole(this.uid.value, this.email.value, this.role.value)
      .then(result => {
        /**
         * @todo: catch error. Check for { error: '...' }. It comes as result!
         */
        this.notify.update('Rol asignado exitosamente.', 'success');
        this.onLoaded();
      })
      .catch(error => { 
        this.notify.update('Error al asignar el rol. Por favor intente de nuevo.', 'error');
        this.onLoaded();
      });
  }

  disabledToggle($event) {
    this.onLoading();
    this.mainService.disabledToggle(this.uid.value, !$event.checked)
      .then(result => {
        /**
         * @todo: catch error. Check for { error: '...' }. It comes as result!
         */
        // Enable/disable fields
        if ($event.checked) {
          this.role.disable();
        } else {
          this.role.enable();
        }
        // Notify success
        this.notify.update('Estado actualizado con exito.', 'success');
        this.onLoaded();
      })
      .catch(error => { 
        this.notify.update('Error al actualizar el estado. Por favor intente de nuevo.', 'error');
        this.onLoaded();
      });
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.uid = data.uid;
      this.disabled = data.disabled;
      this.displayName = data.displayName;
      this.email = data.email;
      this.photoURL = data.photoURL;
      this.personaId = data.personaId;

      this.roles.clear();
      if (data.roles && data.roles.length > 0) {
        for (let i = 0; i < data.roles.length; i++) {
          const element = data.roles[i];
          if (0 === i) {
            this.role = element;
          }
          this.addRole(element);
        }
      }

      this.lastLogin = data.lastLogin;
      this.fCreado = data.fCreado;
      this.creadorId = data.creadorId;
      this.fModificado = data.fModificado;
      this.modificadorId = data.modificadorId;

      this.instancia = data.instancia;
      
      this.instancias.clear();
      if (data.instancias && data.instancias.length > 0) {
        data.instancias.forEach(element => {
          this.addInstancia(element);
        });
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get uid() {
    return this.form.get('uid');
  }

  set uid(uid) {
    this.uid.setValue(uid);
  }

  get disabled() {
    return this.form.get('disabled');
  }

  set disabled(disabled) {
    this.disabled.setValue(disabled);
    // Set rol enabled/disabled
    if (this.isSuperUser && disabled) {
      this.role.enable();
    } else {
      this.role.disable();
    }
    
  }

  get displayName() {
    return this.form.get('displayName');
  }

  set displayName(displayName) {
    this.displayName.setValue(displayName);
  }

  get email() {
    return this.form.get('email');
  }

  set email(email) {
    this.email.setAsyncValidators(UsuarioExistsValidator.email(this.auth, this.afs, this.form));
    this.email.setValue(email);
  }

  get password() {
    return this.form.get('password');
  }

  set password(password) {
    this.password.setValue(password);
  }

  get photoURL() {
    return this.form.get('photoURL');
  }

  set photoURL(photoURL) {
    this.photoURL.setValue(photoURL);
  }

  get personaId() {
    return this.form.get('personaId');
  }

  set personaId(personaId) {
    this.personaId.setValue(personaId);
  }

  get role() {
    return this.form.get('role');
  }

  set role(role) {
    this.role.setValue(role);
  }

  get roles(): FormArray {
    return this.form.get('roles') as FormArray;
  }

  addRole(role = '') {
    const element = this.fb.group({
      role: [role, [
        Validators.required
      ]]
    });
    this.roles.push(element);
  }

  deleteRole(i) {
    this.roles.removeAt(i);
    this.form.markAsTouched();
  }

  get lastLogin() {
    return this.form.get('lastLogin');
  }

  set lastLogin(lastLogin: any) {
    if (null != lastLogin && '' != lastLogin) {
      const d = new Date(lastLogin.seconds * 1000);
      this.lastLogin.setValue(d);
    } else {
      this.lastLogin.setValue(null);
    }
  }

  get instancia() {
    return this.form.get('instancia');
  }

  set instancia(instancia) {
    this.instancia.setValue(instancia);
  }

  get instancias(): FormArray {
    return this.form.get('instancias') as FormArray;
  }

  addInstancia(instancia = '') {
    const element = this.fb.group({
      instancia: [instancia, [
        Validators.required
      ]]
    });
    this.instancias.push(element);
  }

  deleteInstancia(i) {
    this.instancias.removeAt(i);
    this.form.markAsTouched();
  }
}
