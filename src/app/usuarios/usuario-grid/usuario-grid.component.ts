import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';

import { AbsGridComponent } from "../../core/abstract/abs-grid.component";
import { ConfirmDialogComponent } from "../../core/confirm-dialog/confirm-dialog.component";

import { UsuarioAuthoService } from "../shared/usuario-autho.service";
import { UsuarioService } from "../shared/usuario.service";
import { UserId } from "../shared/user";
import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'win-usuario-grid',
  templateUrl: './usuario-grid.component.html',
  styleUrls: ['./usuario-grid.component.scss']
})
export class UsuarioGridComponent extends AbsGridComponent implements OnInit, OnDestroy {

  // Route slug
  slug: string = '/empresas';

  // Mat table definition
  dataSource: MatTableDataSource<UserId>;
  selection = new SelectionModel<UserId>(true, []);

  // Selected ítems emmiter
  @Output() 
  select = new EventEmitter<UserId[] | UserId>();

  // Save subscriptions to unsubscribe
  confirmSub: Subscription;

  constructor(
    public mainService: UsuarioService,
    public autho: UsuarioAuthoService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService
  ) {
    super(mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    super.ngOnInit();

    // Mat table column definition
    this.columns = [ 'mobileView', 'displayName', 'email' ];

    // Setup main service
    this.mainService.pageSize = 50;
    if (null === this.mainService.sort) { this.mainService.sort = 'displayName' }
    if (null === this.mainService.sortDir) { this.mainService.sortDir = 'asc' }
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  /**
   * Set grid filter
   * @param filter 
   */
  setFilter(filter) {}

  /**
   * Delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmDialogComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar estos usuarios?'},
    });

    this.confirmSub = confirm.afterClosed().subscribe(result => {
      if (result) {
        this.onLoading();
        this.mainService.deleteCollection(this.selection.selected)
          .then(x => {
            this.selection.clear();
            this.onLoaded('Borrado!');
            this.deleted.emit(true);
          })
          .catch(error => {
            this.onError(error, 'No fue posible borrar. Por favor intente de nuevo.');
            this.onLoaded();
          });
      }
    });
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    if (undefined !== this.confirmSub) {
      this.confirmSub.unsubscribe();
    }
    super.ngOnDestroy();
  }
}