import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioSelectComponent } from './usuario-select.component';

describe('UsuarioSelectComponent', () => {
  let component: UsuarioSelectComponent;
  let fixture: ComponentFixture<UsuarioSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuarioSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
