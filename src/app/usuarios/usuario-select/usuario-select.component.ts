import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, Output, EventEmitter, Optional, Self, OnDestroy } from '@angular/core';
import { FormBuilder, NgControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldControl } from '@angular/material/form-field';

import { AbsSelectComponent } from '../../core/abstract/abs-select.component';
import { UsuarioGridComponent } from '../usuario-grid/usuario-grid.component';
import { UsuarioService } from '../shared/usuario.service';
import { UserId } from '../shared/user';

@Component({
  selector: 'win-usuario-select',
  templateUrl: './usuario-select.component.html',
  styleUrls: ['./usuario-select.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: UsuarioSelectComponent }],
  host: {
    '[class.label-floating]': 'shouldLabelFloat',
    '[id]': 'id',
    '[attr.aria-describedby]': 'describedBy',
  }
})
export class UsuarioSelectComponent extends AbsSelectComponent implements OnDestroy {

  // "description" field name (to extract it from data)
  descrFieldName = 'displayName';
  // Current selected document
  document: UserId;

  // Emited events
  @Output() load = new EventEmitter<UserId>();
  @Output() select = new EventEmitter<UserId>();

  // Custom input type and id
  controlType = 'win-usuario-select';
  id = `win-usuario-select-${UsuarioSelectComponent.nextId++}`;

  constructor(
    fb: FormBuilder,
    protected mainService: UsuarioService,
    protected fm: FocusMonitor,
    protected elRef: ElementRef<HTMLElement>,
    public dialog: MatDialog,
    @Optional() @Self() public ngControl: NgControl,
  ) {
    super(fb, mainService, fm, elRef, ngControl);
  }

  /**
   * Open select popup and populate fields on select
   */
  openSelect() {
    const dialog = this.dialog.open(UsuarioGridComponent, {data: {}, width: '80%', height: '80%'});
    dialog.componentInstance.mode = 'select';
    dialog.componentInstance.isPopup = true;
    dialog.disableClose = true;
    this.subscriptions.push(
      this.dialogCloseSub = dialog.componentInstance.close.subscribe(() => { dialog.close() })
    );

    this.subscriptions.push(
      this.dialogSub = dialog.componentInstance.select.subscribe(document => {
        // Keep selected document
        this.document = document;
  
        // Load data into form fields
        this.inputId = document.id;
        this.inputDescr = document[this.descrFieldName];
  
        // Emit selected document
        this.select.emit(document);
  
        // Notify change and run change detection
        this.onChange(document.id);
        this.stateChanges.next();
        
        // Close select dialog
        dialog.close();
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
