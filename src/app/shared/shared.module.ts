import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhoneDirective } from './directive/phone.directive';

@NgModule({
  declarations: [
    PhoneDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PhoneDirective
  ]
})
export class SharedModule { }
