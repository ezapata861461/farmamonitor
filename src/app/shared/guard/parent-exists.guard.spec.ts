import { TestBed, async, inject } from '@angular/core/testing';

import { ParentExistsGuard } from './parent-exists.guard';

describe('ParentExistsGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParentExistsGuard]
    });
  });

  it('should ...', inject([ParentExistsGuard], (guard: ParentExistsGuard) => {
    expect(guard).toBeTruthy();
  }));
});
