import { Injectable }     from '@angular/core';
import { CanActivate, 
  ActivatedRouteSnapshot, 
  RouterStateSnapshot }   from '@angular/router';
import { Observable }     from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParentExistsGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    /**
     * Check if parent record id is not 0 (exists) before navigating to
     * other routes that depend of parent record (asociations)
     */
    if ('0' != next.parent.params.id) {
      return true;
    } else {
      return false;
    }
  }
}