# Changelog

## [0.0.6] - 2019-12-08

### Added
* Add StatusInventario type.

## [0.0.5] - 2019-12-03

### Added
* Add tipo-productos type.

## [0.0.4] - 2019-11-18

### Added
* Add custom types for the whole app.
* Add 'instancia' property to ownerAwareInterface.

### Removed
* Remove useless modules.

## [0.0.3] - 2019-10-17

### Added
* Add legacy files to keep backwards compatibility.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.

### Changed
* Improve interface code and docs.

## [0.0.1] - 2019-10-10

* First commit.