/**
 * @deprecated: This comes from outside. Integrate into app better.
 */
export interface Termino {
  descripcion: string;
}

/**
 * descripcio
 * String. Descripción del ´termino.
 */