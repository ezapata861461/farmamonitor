/**
 * @deprecated: This comes from outside. Integrate into app better.
 */
export interface Recomendacion {
  descripcion: string,
  condicion: string,
}

/**
 * descripcion
 * String. Descripción de la recomendación.
 * 
 * condicion
 * String. Condición para mostrar o no la recomdación.
 * '': Significa que la condición se muestra, que no está condicionada.
 * 'if_martes': La condición se muestra solo si es martes.
 * 'if_viernes': La condición se muestra solo si es viernes. 
 */