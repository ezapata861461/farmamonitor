/**
 * @deprecated: This comes from outside. Integrate into app better.
 */
export interface Tarifa {
  acomodacion: string;
  cupo: number;
  tarifa: number;
}

export interface TarifaId extends Tarifa { id: string }