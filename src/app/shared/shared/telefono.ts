/**
 * @deprecated: This comes from outside. Integrate into app better.
 * 
 * numero:
 * Número de teléfono
 * 
 * tipo:
 * Tipo de teléfono: Móvil ó Fijo
 */
export interface Telefono {
  numero: string;
  tipo: string;
}