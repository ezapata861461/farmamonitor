export interface Email {
  email: string;
}

/**
 * Objeto que representa un email.
 * email: string
 * Email.
 */