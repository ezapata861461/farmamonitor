type Tipo = "" | "movil" | "fijo";

/**
 * Objeto que representa un teléfono
 */
export interface Telefono {
  /** Número de teléfono */
  numero: string;
  /** Tipo de teléfono */
  tipo: Tipo;
}
