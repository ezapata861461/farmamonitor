import { Directive, ElementRef, OnDestroy } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Directive({
  selector: '[winPhone]'
})
export class PhoneDirective implements OnDestroy {

  keyupSub: Subscription;

  constructor(public el: ElementRef) {
    // Reject anything but numbers
    this.el.nativeElement.onkeypress = (event) => {
      if (event.which < 48 || event.which > 57) {
        event.preventDefault();
      }
    };

    // Add spaces for readability
    this.keyupSub = fromEvent(this.el.nativeElement, 'keyup').pipe(debounceTime(300)).subscribe(event => {
      this.el.nativeElement.value = this.formatPhone(this.el.nativeElement.value);
    });
  }

  /**
   * Format phone (With spaces for readability)
   */
  formatPhone(string: string): string {
    const onlyD = string.replace(/[^\d]/g,'');
    const l = onlyD.length;
    // Country code + Celphone
    if (l > 10) {
      return onlyD.slice(0, l-10) +' '+ onlyD.slice(l-10, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Cellphone or phone with country and city code
    if (l >= 9) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone with city code
    if (l >= 8) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone
    if (l > 4) {
      return onlyD.slice(0, l-4) +' '+ onlyD.slice(l-4, l);
    } else {
      return onlyD;
    }
  }

  ngOnDestroy() {
    if (this.keyupSub) { this.keyupSub.unsubscribe() }
  }
}
