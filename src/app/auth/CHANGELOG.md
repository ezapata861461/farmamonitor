# Changelog

## [0.0.7] - 2019-12-13

### Changed
* Pull 'redirectToMain' routes from environments file.

## [0.0.6] - 2019-12-08

### Changed
* Improve how instancia config is retrieved ( instancia + modulos nested collection).

## [0.0.5] - 2019-11-26

### Fixed
* Fix redirect after login.

## [0.0.4] - 2019-11-21

### Changed
* Adjust redirects after login.

## [0.0.3] - 2019-11-19

### Added
* Add legacy abstract authorization service, with changes to avoid errors.

### Changed
* Rename abstract authorization service to avoid collision with legacy.

### Fixed
* Fix bug on role check in abstract-authorization service.

## [0.0.2] - 2019-11-18

### Added
* Force sign-out when user disabled.

## [0.0.1] - 2019-10-10

* First commit.