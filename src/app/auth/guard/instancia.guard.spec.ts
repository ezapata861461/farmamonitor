import { TestBed, async, inject } from '@angular/core/testing';

import { InstanciaGuard } from './instancia.guard';

describe('InstanciaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstanciaGuard]
    });
  });

  it('should ...', inject([InstanciaGuard], (guard: InstanciaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
