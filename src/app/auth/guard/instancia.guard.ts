import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { tap, map, take } from "rxjs/operators";

import { AuthService } from "../shared/auth.service";

@Injectable({
  providedIn: 'root'
})
export class InstanciaGuard implements CanActivate, CanActivateChild {
  constructor(private auth: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.user.pipe(
      take(1),
      map(user => {
        // If user has same 'instancia' as route
        if (user.instancias.includes(next.params.instancia)) {
          return true;
        } else {
          return false;
        }
      }),
      tap((allowed) => {
        // If user not allowed
        if (!allowed) {
          this.auth.user.subscribe(user => {
            // If user has no 'instancia'
            if (user.instancias.length < 1) {
              this.auth.signOut();

            // If user is user has only one instancia
            } else if (user.instancias.length === 1) {
              // Set instancia in authService
              this.auth.instancia = user.instancias[0];
              // Redirect to same route with appropiated 'instancia'
              const newRoute = next.url.map(item => item.path)
              newRoute[0] = user.instancias[0];
              this.router.navigate(newRoute);

            // If multiple instancias, send user to instancia-switch
            } else if (user.instancias.length >= 1) {
              this.router.navigate(['instancia-switch']);
            }
          });

        // If user allowed
        } else {
          // Check if authService.instancia is set
          if (this.auth.instancia !== next.params.instancia) {
            this.auth.instancia = next.params.instancia;
          }
        }
      })
    );
  }
  
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
}
