import { Component, OnInit } from '@angular/core';

import { Subscription, BehaviorSubject } from 'rxjs';

import { LoadingService } from "../../core/shared/loading.service";
import { AuthService } from '../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-instancia-switch',
  templateUrl: './instancia-switch.component.html',
  styleUrls: ['./instancia-switch.component.scss']
})
export class InstanciaSwitchComponent implements OnInit {

  private _instancias = new BehaviorSubject<string[] | null>(null);
  instancias = this._instancias.asObservable();

  // Subscriptions
  userSub: Subscription;

  constructor(
    private loading: LoadingService,
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.onLoading();
    this.userSub = this.auth.user.subscribe(user => {
      // If user has no 'instancias'
      if (user.instancias.length === 0) {
        this.router.navigate(['/login'])
      // If user has one 'instancias
      } else if (user.instancias.length === 1) {
        this.auth.instancia = user.instancias[0];
        this.auth.redirectToMain();
      } else {
        this._instancias.next(user.instancias);
        this.onLoaded();
      }
    })
  }

  /**
   * Ask authService to navigate to instancia
   * @param instancia 
   */
  navigateToInstancia(instancia) {
    this.auth.instancia = instancia;
    this.auth.redirectToMain();
  }

  onLoading() {
    this.loading.loading();
  }

  onLoaded() {
    this.loading.loaded();
  }

  ngOnDestroy() {
    if (this.userSub) { this.userSub.unsubscribe() }
  }
}
