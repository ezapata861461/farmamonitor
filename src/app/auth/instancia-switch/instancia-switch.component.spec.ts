import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanciaSwitchComponent } from './instancia-switch.component';

describe('InstanciaSwitchComponent', () => {
  let component: InstanciaSwitchComponent;
  let fixture: ComponentFixture<InstanciaSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanciaSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanciaSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
