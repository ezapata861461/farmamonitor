import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { AppMaterialModule } from "../app-material/app-material.module";
import { FlexLayoutModule } from '@angular/flex-layout';

import { ReactiveFormsModule } from "@angular/forms";

import { LoginComponent } from './login/login.component';
import { InstanciaSwitchComponent } from './instancia-switch/instancia-switch.component';

@NgModule({
  declarations: [
    LoginComponent,
    InstanciaSwitchComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AppMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
  ]
})
export class AuthModule { }
