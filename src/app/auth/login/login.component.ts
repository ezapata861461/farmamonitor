import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';

import { LoadingService } from "../../core/shared/loading.service";
import { AuthService } from "../shared/auth.service";
import { NotifyService } from 'src/app/core/shared/notify.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;

  // Subscriptions
  userSub: Subscription;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    public loading: LoadingService,
    private auth: AuthService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.onLoading();
    this.form = this.buildForm();
    this.signedIn();
  }

  /**
   * Build login form
   */
  buildForm() {
    return this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required
      ]]
    });
  }

  /**
   * Sign with email and password
   * @param email 
   * @param password 
   */
  async signWithEmailPassword() {
    this.onLoading();
    try {
      await this.auth.emailPasswordLogin(this.email.value, this.password.value);
    } catch (error) {
      this.password.setValue('');
      this.form.updateValueAndValidity();
      this.onLoaded();
    }
  }

  /**
   * Sign with google
   */
  async signWithGoogle() {
    this.onLoading();
    try {
      await this.auth.googleLogin();
    } catch {
      this.onLoaded();
    }
  }

  /**
   * Watch user and redirect to right page.
   */
  async signedIn() {
    this.userSub = this.auth.user.subscribe(user => {
      if (user) {
        // If user has one 'instancia', sign him out.
        if (user.instancias.length === 0) {
          this.auth.signOut();
          this.notify.update('Usuario sin privilegios. No cuenta con una instancia asignada.', 'error');
          this.onLoaded();

        // I user has an 'instancia', set it in authService and redirect to main
        } else if (user.instancias.length === 1) {
          this.auth.instancia = user.instancias[0];
          this.notify.update('Bienvenido!', 'success');
          this.auth.redirectToMain();
        
        // If user has more than one instance, send him to instancia-switch
        } else {
          this.notify.update('Bienvenido!', 'success');
          this.router.navigate(['instancia-switch']);
        }
      } else {
        this.onLoaded();
      }
    });
  }

  onLoading() {
    this.loading.loading();
  }

  onLoaded() {
    this.loading.loaded();
  }

  ngOnDestroy() {
    if(undefined !== this.userSub) { this.userSub.unsubscribe() }
  }

  /**
   * Getters & setters
   */

  get email() {
    return this.form.get('email');
  }

  set email(email) {
    this.email.setValue(email);
  }

  get password() {
    return this.form.get('password');
  }

  set password(password) {
    this.password.setValue(password);
  }
}
