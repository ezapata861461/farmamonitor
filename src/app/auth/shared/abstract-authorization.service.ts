import { Injectable }   from '@angular/core';
import * as _           from "lodash";
import { AuthService }  from "../../auth/shared/auth.service";
import { User } from 'src/app/usuarios/shared/user';
import { Roles } from 'src/app/usuarios/shared/roles';

@Injectable({
  providedIn: 'root'
})
export class AbstractAuthorizationService {

  /**
   * Available roles
   * 'webmaster' (Always allowed all).
   * 'operativo', 'comercial', 'contable', 'gerencial'
   */

  // Array of allowed read roles
  private _readAllowedRoles: Array<string> = ['webmaster'];
  get readAllowedRoles() { return this._readAllowedRoles; }
  set readAllowedRoles(readAllowedRoles) {
    this._readAllowedRoles = [];
    this._readAllowedRoles = readAllowedRoles;
    this._readAllowedRoles.push('webmaster');
  }

  // Array of allowed create roles
  private _createAllowedRoles: Array<string> = ['webmaster'];
  get createAllowedRoles() { return this._createAllowedRoles; }
  set createAllowedRoles(createAllowedRoles) {
    this._createAllowedRoles = [];
    this._createAllowedRoles = createAllowedRoles;
    this._createAllowedRoles.push('webmaster');
  }

  // Array of allowed edit roles
  private _editAllowedRoles: Array<string> = ['webmaster'];
  get editAllowedRoles() { return this._editAllowedRoles; }
  set editAllowedRoles(editAllowedRoles) {
    this._editAllowedRoles = [];
    this._editAllowedRoles = editAllowedRoles;
    this._editAllowedRoles.push('webmaster');
  }

  // Array of allowed delete roles
  private _deleteAllowedRoles: Array<string> = ['webmaster'];
  get deleteAllowedRoles() { return this._deleteAllowedRoles; }
  set deleteAllowedRoles(deleteAllowedRoles) {
    this._deleteAllowedRoles = [];
    this._deleteAllowedRoles = deleteAllowedRoles;
    this._deleteAllowedRoles.push('webmaster');
  }

  // Array of allowed softdelete roles
  private _softdeleteAllowedRoles: Array<string> = ['webmaster'];
  get softdeleteAllowedRoles() { return this._softdeleteAllowedRoles; }
  set softdeleteAllowedRoles(softdeleteAllowedRoles) {
    this._softdeleteAllowedRoles = [];
    this._softdeleteAllowedRoles = softdeleteAllowedRoles;
    this._softdeleteAllowedRoles.push('webmaster');
  }

  // Array of allowed file roles
  private _fileAllowedRoles: Array<string> = ['webmaster'];
  get fileAllowedRoles() { return this._fileAllowedRoles; }
  set fileAllowedRoles(fileAllowedRoles) {
    this._fileAllowedRoles = [];
    this._fileAllowedRoles = fileAllowedRoles;
    this._fileAllowedRoles.push('webmaster');
  }

  /**
   * !!!Added new, for compatibility!!!
   * User and roles from auth service
   */
  user: User|null = null;
  roles: Roles|null = null;

  constructor(
    protected auth: AuthService
  ) {
    /**
     * !!!Added new, for compatibility!!!
     */
    // Load user from service
    this.auth.user.subscribe(user => {
      this.user = user;
      if (user) {
        this.auth.getRoles().then(roles => {
          this.roles = roles;
        })
      }
    });
  }

  /**
   * Can current user read?
   */
  get canRead(): boolean {
    if (this.hasRol('webmaster')) { return true }
    return this.matchingRole(this.readAllowedRoles);
  }

  /**
   * Can current user create?
   */
  get canCreate(): boolean {
    if (this.hasRol('webmaster')) { return true }
    return this.matchingRole(this.createAllowedRoles);
  }

  /**
   * Can current user create?
   */
  get canEdit(): boolean {
    if (this.hasRol('webmaster')) { return true }
    return this.matchingRole(this.editAllowedRoles);
  }

  /**
   * Can current user create?
   */
  get canDelete(): boolean {
    if (this.hasRol('webmaster')) { return true }
    return this.matchingRole(this.deleteAllowedRoles);
  }

  /**
   * Can current user softdelete?
   */
  get canSoftdelete(): boolean {
    if (this.hasRol('webmaster')) { return true }
    return this.matchingRole(this.softdeleteAllowedRoles);
  }

  /**
   * Can current user file?
   */
  get canFile(): boolean {
    if (this.hasRol('webmaster')) { return true }
    return this.matchingRole(this.fileAllowedRoles);
  }

  /**
   * Current user can read if is owner
   * @param owner userId
   * 
   * @deprecated: Ownership should be verified using isOwner method.
   */
  ownerCanRead(owner: string) {
    if (this.hasRol('webmaster')) { return true }
    return (this.canRead && this.isOwner(owner));
  }

  /**
   * Current user can create if is owner
   * @param owner userId
   * 
   * @deprecated: Ownership should be verified using isOwner method.
   * @todo: Delete. Search where is used.
   */
  ownerCanCreate(owner: string) {
    if (this.hasRol('webmaster')) { return true }
    return (this.canCreate && this.isOwner(owner));
  }

  /**
   * Current user can edit if is owner
   * @param owner userId
   * 
   * @deprecated: Ownership should be verified using isOwner method.
   * @todo: Delete. Search where is used.
   */
  ownerCanEdit(owner: string) {
    if (this.hasRol('webmaster')) { return true }
    return (this.canEdit && this.isOwner(owner));
  }

  /**
   * Current user can delete if is owner
   * @param owner userId
   * 
   * @deprecated: Ownership should be verified using isOwner method.
   * @todo: Delete. Search where is used.
   */
  ownerCanDelete(owner: string) {
    if (this.hasRol('webmaster')) { return true }
    return (this.canDelete && this.isOwner(owner));
  }

  /**
   * Current user can softdelete if is owner
   * @param owner userId
   * 
   * @deprecated: Ownership should be verified using isOwner method.
   * @todo: Delete. Search where is used.
   */
  ownerCanSoftdelete(owner: string) {
    if (this.hasRol('webmaster')) { return true }
    return (this.canSoftdelete && this.isOwner(owner));
  }

  /**
   * Current user can delete if is owner
   * @param owner userId
   * 
   * @deprecated: Ownership should be verified using isOwner method.
   * @todo: Delete. Search where is used.
   */
  ownerCanFile(owner: string) {
    if (this.hasRol('webmaster')) { return true }
    return (this.canFile && this.isOwner(owner));
  }

  /**
   * Check if owner (must be an email) is equal to current user email
   * @param owner 
   */
  isOwner(owner): boolean {
    if (this.hasRol('webmaster')) { return true }
    return (null != this.auth.currentUser && null != this.auth.currentUser.value) ? (owner == this.auth.currentUser.value.email) : false;
  }

  /**
   * !!! REPLACED FOR COMPATIBILITY !!!
   * 
   * Check if user has rol
   * @param rol 
   */
  /**
  hasRol(rol: string): boolean {
    if (null != this.auth.currentUser.value) {
      return (rol == this.auth.currentUser.value.rol);
    } else {
      return false;
    }    
  }
  */
  /**
   * Check if user has any of indicated roles
   * @param rol 
   */
  hasRol(role: string): boolean {
    const roles = [ role ];
    for (let role of roles) {
      if (null !== this.roles && this.roles[role]) {
        return true;
      }
    }
    return false;
  }

  /**
   * !!! REPLACED FOR COMPATIBILITY !!!
   * 
   * Check if role exists in allowedRoles
   * @param allowedRoles 
   */
  /**
  protected matchingRole(allowedRoles): boolean {
    if (null != this.auth.currentUser.value) {
      const rol = this.auth.currentUser.value.rol;
      return allowedRoles.includes(rol);
    } else {
      return false;
    }
  }
  */
  /**
   * Check if role exists in allowedRoles
   * @param allowedRoles 
   */
  protected matchingRole(allowedRoles): boolean {
    if (null === this.roles) { return false }

    for (const role in this.roles) {
      // If user has role and role is allowed
      if (true === this.roles[role] && true === allowedRoles.includes(role)) {
        return true;
      }
    }
    // If no matching role
    return false;
  }
}