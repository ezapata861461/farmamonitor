import { TestBed } from '@angular/core/testing';

import { AbsAuthorizationService } from './abs-authorization.service';

describe('AbsAuthorizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbsAuthorizationService = TestBed.get(AbsAuthorizationService);
    expect(service).toBeTruthy();
  });
});
