import { TestBed } from '@angular/core/testing';

import { AbstractAuthorizationService } from './abstract-authorization.service';

describe('AbstractAuthorizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbstractAuthorizationService = TestBed.get(AbstractAuthorizationService);
    expect(service).toBeTruthy();
  });
});
