import { Injectable } from '@angular/core';

import { AuthService } from "./auth.service";
import { User } from 'src/app/usuarios/shared/user';
import { Roles } from 'src/app/usuarios/shared/roles';

@Injectable({
  providedIn: 'root'
})
export class AbsAuthorizationService {

  // Array of allowed read roles
  get readAllowedRoles() { return this._readAllowedRoles; }
  set readAllowedRoles(readAllowedRoles) {
    this._readAllowedRoles = [];
    this._readAllowedRoles = readAllowedRoles;
    this._readAllowedRoles.push('webmaster');
  }
  private _readAllowedRoles: Array<string> = ['webmaster'];

  // Array of allowed create roles
  get createAllowedRoles() { return this._createAllowedRoles; }
  set createAllowedRoles(createAllowedRoles) {
    this._createAllowedRoles = [];
    this._createAllowedRoles = createAllowedRoles;
    this._createAllowedRoles.push('webmaster');
  }
  private _createAllowedRoles: Array<string> = ['webmaster'];

  // Array of allowed edit roles
  get editAllowedRoles() { return this._editAllowedRoles; }
  set editAllowedRoles(editAllowedRoles) {
    this._editAllowedRoles = [];
    this._editAllowedRoles = editAllowedRoles;
    this._editAllowedRoles.push('webmaster');
  }
  private _editAllowedRoles: Array<string> = ['webmaster'];

  // Array of allowed delete roles
  get deleteAllowedRoles() { return this._deleteAllowedRoles; }
  set deleteAllowedRoles(deleteAllowedRoles) {
    this._deleteAllowedRoles = [];
    this._deleteAllowedRoles = deleteAllowedRoles;
    this._deleteAllowedRoles.push('webmaster');
  }
  private _deleteAllowedRoles: Array<string> = ['webmaster'];

  // Array of allowed softdelete roles
  get softdeleteAllowedRoles() { return this._softdeleteAllowedRoles; }
  set softdeleteAllowedRoles(softdeleteAllowedRoles) {
    this._softdeleteAllowedRoles = [];
    this._softdeleteAllowedRoles = softdeleteAllowedRoles;
    this._softdeleteAllowedRoles.push('webmaster');
  }
  private _softdeleteAllowedRoles: Array<string> = ['webmaster'];

  // Array of allowed file roles
  get fileAllowedRoles() { return this._fileAllowedRoles; }
  set fileAllowedRoles(fileAllowedRoles) {
    this._fileAllowedRoles = [];
    this._fileAllowedRoles = fileAllowedRoles;
    this._fileAllowedRoles.push('webmaster');
  }
  private _fileAllowedRoles: Array<string> = ['webmaster'];

  /**
   * User and roles from auth service
   */
  user: User|null = null;
  roles: Roles|null = null;

  constructor(
    protected auth: AuthService
  ) {
    // Load user from service
    this.auth.user.subscribe(user => {
      this.user = user;
      if (user) {
        this.auth.getRoles().then(roles => {
          this.roles = roles;
        })
      }
    });
  }

  /**
   * Can current user read?
   */
  get canRead(): boolean {
    if (this.hasRol(['webmaster'])) { return true }
    return this.matchingRole(this.readAllowedRoles);
  }

  /**
   * Can current user create?
   */
  get canCreate(): boolean {
    if (this.hasRol(['webmaster'])) { return true }
    return this.matchingRole(this.createAllowedRoles);
  }

  /**
   * Can current user create?
   */
  get canEdit(): boolean {
    if (this.hasRol(['webmaster'])) { return true }
    return this.matchingRole(this.editAllowedRoles);
  }

  /**
   * Can current user create?
   */
  get canDelete(): boolean {
    if (this.hasRol(['webmaster'])) { return true }
    return this.matchingRole(this.deleteAllowedRoles);
  }

  /**
   * Can current user softdelete?
   */
  get canSoftdelete(): boolean {
    if (this.hasRol(['webmaster'])) { return true }
    return this.matchingRole(this.softdeleteAllowedRoles);
  }

  /**
   * Can current user file?
   */
  get canFile(): boolean {
    if (this.hasRol(['webmaster'])) { return true }
    return this.matchingRole(this.fileAllowedRoles);
  }

  /**
   * Check if owner is equal to current user (check by uid)
   * @param owner 
   */
  isOwner(owner: string): boolean {
    if (this.hasRol(['webmaster'])) { return true }
    return (null !== this.user) ? (owner === this.user.uid) : false;
  }

  /**
   * Check if user has any of indicated roles
   * @param rol 
   */
  hasRol(roles: Array<string>): boolean {
    for (let role of roles) {
      if (null !== this.roles && this.roles[role]) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if role exists in allowedRoles
   * @param allowedRoles 
   */
  protected matchingRole(allowedRoles): boolean {
    if (null === this.roles) { return false }

    for (const role in this.roles) {
      // If user has role and role is allowed
      if (true === this.roles[role] && true === allowedRoles.includes(role)) {
        return true;
      }
    }
    // If no matching role
    return false;
  }
}