import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFireFunctions } from "@angular/fire/functions";
import { auth } from 'firebase/app';

import { Observable, of, BehaviorSubject, combineLatest } from 'rxjs';
import { switchMap, map } from "rxjs/operators";

import { ErrorLogService } from "../../core/shared/error-log.service";
import { NotifyService } from "../../core/shared/notify.service";
import { User, UserId } from '../../usuarios/shared/user';
import { Roles } from 'src/app/usuarios/shared/roles';
import { RolesService } from 'src/app/usuarios/shared/roles.service';
import { Instancia } from 'src/app/instancias/shared/instancia';
import { Modulo } from 'src/app/instancias/shared/modulo';

import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * Current User
   * @deprecated it's used by legacy abstract-autho service.
   */
  currentUser: BehaviorSubject<UserId|null> = new BehaviorSubject(null);

  user: Observable<User|null>;
  config: Observable<Instancia|null>;

  set instancia(instancia: string) { this._instancia$.next(instancia) }
  get instancia(): string { return this._instancia$.value }
  private _instancia$: BehaviorSubject<string|null> = new BehaviorSubject(null);

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private afFns: AngularFireFunctions,
    private router: Router,
    private rolesService: RolesService,
    private errorLog: ErrorLogService,
    private notify: NotifyService
  ) {

    // Load user
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );

    // Kill session if user is disabled
    this.user.subscribe(user => {
      if (null === user || true === user.disabled) {
        this.signOut();
      
      /** @deprecated: currentUser is deprecated! */
      } else {
        this.currentUser.next(user as UserId);
      }
    })

    /**
     * @test: Use this lines to test user identity and custom claims
       this.user.subscribe(user => {
        console.log(user);
        this.afAuth.auth.currentUser.getIdTokenResult()
          .then(idTokenResult => {
            console.log(idTokenResult.claims);
          })
          .catch(error => {
            console.log(error);
          })
        });
     */

    // Load config
    this.config = combineLatest(this._instancia$).pipe(
      switchMap(([instancia]) => {
        if (instancia) {
          // Get config directly from firestore! Can´t use service, since they depend on this class!          
          let modulos: Observable<{ [s: string]: Modulo}> = this.afs.collection(`instancias/${instancia}/modulos`)
          .valueChanges().pipe(
            map((items: Modulo[]) => {
              const mods: { [ s: string ]: Modulo } = {};
              items.forEach(item => {
                mods[item.nombre] = item;
              })
              return mods;
            })
          )

          // Combine 'instancia' + 'modulos'
          return combineLatest(
            this.afs.collection('instancias').doc<Instancia>(this.instancia).valueChanges(),
            modulos
          ).pipe(
            switchMap(([instancia, modulos]) => {
              instancia['modulos'] = modulos;
              return of(instancia);
            })
          )
        } else {
          return of(null);
        }
      })
    );

    /**
     * @test: Use this lines to test config
      this.config.subscribe(config => {
        console.log(config);
      });
     */
  }

  /**
   * Returns user uid
   */
  getUid(): string {
    return this.afAuth.auth.currentUser.uid;
  }

  /**
   * Returns roles from idTokenResult claims
   */
  getRoles(): Promise<Roles> {
    return this.afAuth.auth.currentUser.getIdTokenResult().then(idTokenResult => {
      return this.rolesService.getRolesFromClaims(idTokenResult.claims);
    });
  }

  /**
   * Redirects from login to the an appropiated route depending on role
   */
  redirectToMain() {
    // Guet default redirects from environment
    const defaultRedirects = environment.defaultRedirects;
    this.getRoles().then(roles => {
      if (roles.webmaster) {
        this.router.navigate([this.instancia, defaultRedirects.webmaster]);
      } else if(roles.admin) {
        this.router.navigate([this.instancia, defaultRedirects.admin]);
      } else if(roles.manager) {
        this.router.navigate([this.instancia, defaultRedirects.manager]);
      } else if(roles.operations) {
        this.router.navigate([this.instancia, defaultRedirects.operations]);
      } else if(roles.sales) {
        this.router.navigate([this.instancia, defaultRedirects.sales]);
      } else if(roles.accounting) {
        this.router.navigate([this.instancia, defaultRedirects.accounting]);
      } else if(roles.client) {
        this.router.navigate([this.instancia, defaultRedirects.client]);
      } else if(roles.direct) {
        this.router.navigate([this.instancia, defaultRedirects.direct]);
      } else if(roles.guest) {
        this.router.navigate([this.instancia, defaultRedirects.guest]);
      }
    });
  }

  /**
   * Login with email and password
   * @param credentials 
   */
  emailPasswordLogin(email: string, password: string) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(credential => {
        return this.updateUserData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  /**
   * google account login
   */
  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  /**
   * @todo: Add more login methods here
   */

  /**
   * Create sign In popup
   * @param provider 
   */
  private oAuthLogin(provider: any) {
    return this.afAuth.auth
      .signInWithPopup(provider)
      .then(credential => {
        return this.updateUserData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  /**
   * Update/create users
   * @param user 
   */
  async updateUserData(user): Promise<void> {
    const data: User = {
      uid: user.uid,
      email: user.email || null,
      displayName: user.displayName || '',
      photoURL: user.photoURL || ''
    };

    // Request server to update user
    const updateUserOnLogin = this.afFns.httpsCallable('userOnLogin');
    const updated = await updateUserOnLogin(data).toPromise();

    // If new user, refresh token to get custom claims
    if (undefined !== updated.result) {
      await this.afAuth.auth.currentUser.getIdTokenResult(true);
    }
    return;
  }

  /**
   * Sign out and redirect to login.
   * 
   * @todo: Capturar los errores que ocurren al hacer logout.
   */
  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

  /**
   * Notify, log and throw errors.
   * @param error 
   */
  handleError(error: Error) {
    this.errorLog.log(error, false);
    this.notify.update('Usuario no autorizado.', 'error');
    throw error;
  }
}
