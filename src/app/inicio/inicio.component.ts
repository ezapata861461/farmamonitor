import { Component, OnInit } from '@angular/core';

import { LoadingService } from "../core/shared/loading.service";

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  constructor(
    private loading: LoadingService
  ) { }

  ngOnInit() {
    this.loading.loaded();
  }
}
