# Changelog

## [0.0.3] - 2019-11-26

### Changed
* Clean and add basic message.

## [0.0.2] - 2019-11-19

### Changed
* Add some dummy content to index.
* Set allowed roles (All!).

## [0.0.1] - 2019-10-10

* First commit.