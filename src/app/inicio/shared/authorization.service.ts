import { Injectable }                   from '@angular/core';
import * as _                           from 'lodash';

import { AbsAuthorizationService } from '../../auth/shared/abs-authorization.service';
import { AuthService }                  from "../../auth/shared/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService extends AbsAuthorizationService {

  constructor(
    protected auth: AuthService,
  ) {
    super(auth);

    // Set allowed roles arrays
    this.readAllowedRoles =       [ 'webmaster', 'admin', 'manager', 'operations', 'sales', 'accounting', 'client', 'direct', 'guest' ];
    this.createAllowedRoles =     [  ];
    this.editAllowedRoles =       [  ];
    this.deleteAllowedRoles =     [  ];
    this.softdeleteAllowedRoles = [  ];
    this.fileAllowedRoles =       [  ];
  }
}