import { TestBed } from '@angular/core/testing';

import { SedeAuthoService } from './sede-autho.service';

describe('AuthorizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SedeAuthoService = TestBed.get(SedeAuthoService);
    expect(service).toBeTruthy();
  });
});
