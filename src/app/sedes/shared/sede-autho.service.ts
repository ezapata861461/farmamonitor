import { Injectable } from '@angular/core';

import { AbsAuthorizationService } from '../../auth/shared/abs-authorization.service';
import { AuthService } from "../../auth/shared/auth.service";

@Injectable({
  providedIn: 'root'
})
export class SedeAuthoService extends AbsAuthorizationService {

  constructor(
    protected auth: AuthService,
  ) {
    super(auth);

    // Set allowed roles arrays
    this.readAllowedRoles =       [ 'admin', 'operations', 'sales', 'accounting' ];
    this.createAllowedRoles =     [ 'admin', 'operations', 'sales', 'accounting' ];
    this.editAllowedRoles =       [ 'admin', 'operations', 'sales', 'accounting' ];
    this.deleteAllowedRoles =     [ 'admin', 'operations', 'sales', 'accounting' ];
    this.softdeleteAllowedRoles = [  ];
    this.fileAllowedRoles =       [  ];
  }
}
