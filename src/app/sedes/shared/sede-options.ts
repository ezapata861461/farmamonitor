export const tipoOptions = [
  { value: 'cerrada', viewValue: 'Cerrada', icon: '' },
  { value: 'fisica', viewValue: 'Física', icon: '' },
  { value: 'virtual', viewValue: 'Vistual', icon: '' }
];

export const telefonoOptions = [
  { value: 'movil', viewValue: 'Móvil', icon: 'smartphone' },
  { value: 'fijo', viewValue: 'Fijo', icon: 'phone' },
];