import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject }  from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import * as firebase from 'firebase/app';
import 'firebase/firestore'
import { AbsFirestoreService } from '../../core/abstract/abs-firestore.service';
import * as  _  from "lodash";

import { AuthService } from "../../auth/shared/auth.service";
import { Sede, SedeId } from "./sede";

@Injectable({
  providedIn: 'root'
})
export class SedeService  extends AbsFirestoreService implements OnDestroy {

  // Collection prefix
  collectionPrefix = '';
  // Collection name
  collectionName = 'sedes';
  // Current loaded collection
  col$: Observable<SedeId[]>;
  // Current loaded document
  doc$: BehaviorSubject<SedeId | null> = new BehaviorSubject(this.createEmpty());

  /**
   * Constructor
   * @param afs 
   * @param auth 
   */
  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService,
  ) {
    super(afs, auth);

    // Init current collection and current object
    this.initCol();
    this.initDoc();
  }

  /**
   * *********************************
   * ABSTRACT FUNCTIONS IMPLEMENTATION
   * *********************************
   */

  /**
   * Map collection items into objects array
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as SedeId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<SedeId>(this.collectionName + '/' + id);
  }

  /**
   * Creates an empty object
   */
  createEmpty(): SedeId {
    return {
      id: '0',
      alt: null,
      ciudad: 'Medellín',
      coords: null,
      direccion: '',
      email: '',
      emails: [],
      empresa: '',
      empresaId: '',
      fUltimaCompra: null,
      fUltimaReserva: null,
      fUltimaVisita: null,
      nombre: '',
      observaciones: '',
      principal: false,
      punto: null,
      telefonos: [],
      tipo: 'fisica',
      tipos: [],
      zoom: 10,
      fCreado: null,
      creadorId: '',
      fModificado: null,
      modificadorId: '',
      instancia: this.auth.instancia,
      instancias: [],
      /** @deprecated */
      creado: null,
      /** @deprecated */
      creador: '',
      /** @deprecated */
      modificado: null,
      /** @deprecated */
      modificador: '' 
    }
  }

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  extract(fd): Sede {
    let data: Sede;
    data = {
      alt: (fd.alt) ? +fd.alt : null,
      ciudad: (fd.ciudad) ? _.trim(fd.ciudad) :  '',
      coords: (fd.coords) ? new firebase.firestore.GeoPoint(+fd.coords.latitude, +fd.coords.longitude) : null,
      direccion: (fd.direccion) ? _.trim(fd.direccion) : '',
      email: (undefined == fd.email) ? '' : fd.email.toLowerCase(),
      emails: (fd.emails && 0 <= fd.emails.length) ? fd.emails.map(el => { el = el.email.toLowerCase(); return el; }) : [],
      empresa: (fd.empresa) ? fd.empresa : '',
      empresaId: (fd.empresaId) ? fd.empresaId : '',
      fUltimaCompra: (fd.fUltimaCompra && '' !== fd.fUltimaCompra) ? fd.fUltimaCompra : null,
      fUltimaReserva: (fd.fUltimaReserva && '' !== fd.fUltimaReserva) ? fd.fUltimaReserva : null,
      fUltimaVisita: (fd.fUltimaVisita && '' !== fd.fUltimaVisita) ? fd.fUltimaVisita : null,
      nombre: (fd.nombre) ? _.trim(fd.nombre) : '',
      observaciones: (fd.observaciones) ? _.trim(fd.observaciones) : '',
      principal: (fd.principal) ? true : false,
      punto: (fd.punto) ? new firebase.firestore.GeoPoint(+fd.punto.latitude, +fd.punto.longitude) : null,
      telefonos: (fd.telefonos && 0 <= fd.telefonos.length) ? fd.telefonos.map(el => { el.numero = el.numero.replace(/[^\d]/g,''); return el; }) : [],
      tipo: (fd.tipo && '' !== fd.tipo) ? fd.tipo : 'fisica',
      tipos: (fd.tipos && 0 <= fd.tipos.length) ? fd.tipos : [],
      zoom: (fd.zoom) ? +fd.zoom : null,
      fCreado: (fd.fCreado && '' !== fd.fCreado) ? fd.fCreado : this.timestamp,
      creadorId: (fd.creadorId && '' !== fd.creadorId) ? fd.creadorId : this.auth.getUid(),
      fModificado: this.timestamp,
      modificadorId: this.auth.getUid(),
      instancia: (fd.instancia) ? fd.instancia : this.auth.instancia,
      instancias: (fd.instancias && '' !== fd.instancias && 0 < fd.instancias.length) ? fd.instancias.map(el => { el = el.instancia; return el; }) : [ this.auth.instancia ],
      /** @deprecated */
      creado: (fd.creado && '' !== fd.creado) ? fd.creado : this.timestamp,
      /** @deprecated */
      creador: (fd.creador && '' !== fd.creador) ? fd.creador : this.auth.getUid(),
      /** @deprecated */
      modificado: this.timestamp,
      /** @deprecated */
      modificador: this.auth.getUid(),
    }
    return data;
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
