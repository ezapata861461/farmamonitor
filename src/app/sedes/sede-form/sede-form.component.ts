import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { debounceTime, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { SedeAuthoService } from "../shared/sede-autho.service";
import { SedeService } from "../shared/sede.service";
import { tipoOptions, telefonoOptions } from "../shared/sede-options";

import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';
import { AuthService } from 'src/app/auth/shared/auth.service';
import { EmpresaService } from 'src/app/empresas/shared/empresa.service';
import { EmpresaId } from 'src/app/empresas/shared/empresa';

@Component({
  selector: 'win-sede-form',
  templateUrl: './sede-form.component.html',
  styleUrls: ['./sede-form.component.scss']
})
export class SedeFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'sedes'

  /**
   * Parent empresa for empleado (asociated empresa and empresaId)
   */
  @Input()
  set parentEmpresa(parentEmpresa: EmpresaId|null) { this._parentEmpresa = parentEmpresa }
  get parentEmpresa(): EmpresaId|null { return this._parentEmpresa }
  private _parentEmpresa: EmpresaId|null = null;

  // Init map coords to medellín
  mapType = 'roadmap';
  mapLat: number = 6.249947;
  mapLng: number = -75.567830;
  mapZoom: number = 10;

  // Options
  tipoOpts = tipoOptions;
  telefonoOpts = telefonoOptions;

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  // Form parts show/hide flags
  parts = {
    deprecated: false,
    instancias: false,
    interacciones: false,
    registro: false
  }

  // empresas observable for autocomplete
  empresas$: Observable<any>;

  constructor(
    protected router: Router,
    public mainService: SedeService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    private fb: FormBuilder,
    private auth: AuthService,
    public autho: SedeAuthoService,
    protected empresaService: EmpresaService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = (this.autho.canCreate && '0' === this.mainService.id) || (this.autho.canEdit && '0' !== this.mainService.id);
    this.isSuperUser = this.autho.hasRol(['webmaster', 'admin']);
    this.isWebmaster = this.autho.hasRol(['webmaster']);

    /**
     * @todo: If creation (id === '0'), load map location from googlemaps config.
     *  This should be done after id setter
     */

    // Get form config and hide/show form parts
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        const conf = config.modulos['sedes'].formParts;
        this.parts = {
          deprecated: this.isWebmaster || conf.deprecated,
          instancias: this.isWebmaster || conf.instancias,
          interacciones: this.isWebmaster || conf.interacciones,
          registro: this.isWebmaster || conf.registro
        }
      })
    );

    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      alt: [{ value: null, disabled: !this.canCreateOrEdit }],
      ciudad: [{ value: 'Medellín', disabled: !this.canCreateOrEdit }],
      coords: this.fb.group({
        latitude: [{ value: null, disabled: !this.canCreateOrEdit }],
        longitude: [{ value: null, disabled: !this.canCreateOrEdit }],
      }),
      direccion: [{ value: '', disabled: !this.canCreateOrEdit }],
      /** @deprecated */
      email: [{ value: '', disabled: !this.canCreateOrEdit }],
      emails: this.fb.array([]),
      empresaAux: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required
      ]],
      empresa: [{ value: '', disabled: !this.canCreateOrEdit }],
      empresaId: [{ value: '', disabled: !this.canCreateOrEdit }],
      fUltimaCompra: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      fUltimaReserva: [{ value: null, disabled: !this.isSuperUser }],
      fUltimaVisita: [{ value: null, disabled: !this.isSuperUser }],
      nombre: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required,
        Validators.minLength(3),
      ]],
      observaciones: [{ value: '', disabled: !this.canCreateOrEdit }],
      principal: [{ value: false, disabled: !this.canCreateOrEdit }],
      punto: this.fb.group({
        latitude: [{ value: null, disabled: !this.canCreateOrEdit }],
        longitude: [{ value: null, disabled: !this.canCreateOrEdit }],
      }),
      telefonos: this.fb.array([]),
      tipo: [{ value: '', disabled: !this.canCreateOrEdit }],
      tipos: this.fb.array([]),
      zoom: [{ value: 10, disabled: !this.canCreateOrEdit }],
      fCreado: [{ value: null, disabled: !this.isSuperUser }],
      creadorId: [{ value: '', disabled: !this.isSuperUser }],
      fModificado: [{ value: null, disabled: !this.isSuperUser }],
      modificadorId: [{ value: '', disabled: !this.isSuperUser }],
      instancia: [{ value: '', disabled: !this.isSuperUser }],
      instancias: this.fb.array([]),
      /** @deprecated */
      creado: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      creador: [{ value: '', disabled: !this.isSuperUser }],
      /** @deprecated */
      modificado: [{ value: null, disabled: !this.isSuperUser }],
      /** @deprecated */
      modificador: [{ value: '', disabled: !this.isSuperUser }],
    });
  }

  /**
   * Load controls
   */
  loadControls(): void {
    this.empresas$ = this.empresaAux.valueChanges
      .pipe(
        debounceTime(300),
        switchMap((value: string) => this.empresaService.search(value, 'razonSocial'))
      );
  }

  /**
   * Display function for empresaAutocomplete
   * @param empresa 
   */
  displayEmpresa(empresa: any) {
    if (empresa) {
      if ('string' === (typeof empresa)) {
        return empresa;
      } else {
        return empresa.razonSocial
      }
    }
  }

  /**
   * Empresa selected (autocomplete) event
   * @param $event 
   */
  empresaSelected($event) {
    const empresa = $event.option.value; 
    this.empresa = empresa.razonSocial;
    this.empresaId = empresa.id;
  }

  /**
   * Sets tipo to keep it for later
   * @param $event 
   * @param i 
   */
  onSedeTipoSelect($event, i) {
    this.tipos.controls[i].get('tipo').setValue($event.tipo);
  }

  /**
   * On map click, set marker
   * @param $event 
   */
  onMapClick($event) {
    if (this.canCreateOrEdit) {
      this.coords_latitude = $event.coords.lat;
      this.coords_longitude = $event.coords.lng;
      this.punto_latitude = $event.coords.lat;
      this.punto_longitude = $event.coords.lng;
      this.form.markAsTouched();
    }
  }

  /**
   * On zoom change, update zoom
   * @param $event 
   */
  onZoomChange($event) {
    if (this.canCreateOrEdit) {
      this.zoom = $event;
      this.form.markAsTouched();
    }
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.alt = data.alt;
      this.ciudad = data.ciudad;
      this.coords = data.coords;
      this.direccion = data.direccion;
      this.email = data.email;

      this.emails.clear();
      if (data.emails && data.emails.length > 0) {
        data.emails.forEach(element => {
          this.addEmail(element);
        });
      }

      // Load parent empresa on creation
      if ('0' === this.id.value) { 
        if (null !== this.parentEmpresa) {
          this.empresaAux = this.parentEmpresa.razonSocial;
          this.empresa = this.parentEmpresa.razonSocial;
          this.empresaId = this.parentEmpresa.id;
        }
      } else {
        this.empresaAux = data.empresa;
        this.empresa = data.empresa;
        this.empresaId = data.empresaId;
      }

      this.fUltimaCompra = data.fUltimaCompra;
      /** @deprecated */
      this.fUltimaReserva = data.fUltimaReserva;
      this.fUltimaVisita =  data.fUltimaVisita;
      this.nombre = data.nombre;
      this.observaciones = data.observaciones;
      this.principal = data.principal;
      this.punto = data.punto;

      this.telefonos.clear();
      if (data.telefonos && data.telefonos.length > 0) {
        data.telefonos.forEach(element => {
          this.addTelefono(element);
        });
      }
      
      this.tipo = data.tipo;

      this.tipos.clear();
      if (data.tipos && data.tipos.length > 0) {
        data.tipos.forEach(element => {
          this.addTipo(element);
        });
      }

      this.zoom = data.zoom;

      this.fCreado = data.fCreado;
      this.creadorId = data.creadorId;
      this.fModificado = data.fModificado;
      this.modificadorId = data.modificadorId;

      /** @deprecated */
      this.creado = data.creado;
      /** @deprecated */
      this.creador = data.creador;
      /** @deprecated */
      this.modificado = data.modificado;
      /** @deprecated */
      this.modificador = data.modificador;

      this.instancia = data.instancia;
      
      this.instancias.clear();
      if (data.instancias && data.instancias.length > 0) {
        data.instancias.forEach(element => {
          this.addInstancia(element);
        });
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */

  get alt() {
    return this.form.get('alt');
  }

  set alt(alt) {
    this.alt.setValue(alt);
  }

  get ciudad() {
    return this.form.get('ciudad');
  }

  set ciudad(ciudad) {
    this.ciudad.setValue(ciudad);
  }

  get coords() {
    return this.form.get('coords');
  }

  set coords(coords: any) {
    if (coords) {
      this.coords_latitude = coords.latitude;
      this.coords_longitude = coords.longitude;
    } else {
      this.coords_latitude = null;
      this.coords_longitude = null;
    }
  }

  get coords_latitude() {
    return this.coords.get('latitude');
  }

  set coords_latitude(latitude) {
    this.coords_latitude.setValue(latitude);
  }

  get coords_longitude() {
    return this.coords.get('longitude');
  }

  set coords_longitude(longitude) {
    this.coords_longitude.setValue(longitude);
  }

  get direccion() {
    return this.form.get('direccion');
  }

  set direccion(direccion) {
    this.direccion.setValue(direccion);
  }

  get email() {
    return this.form.get('email');
  }

  set email(email) {
    this.email.setValue(email);
  }

  get emails(): FormArray {
    return this.form.get('emails') as FormArray;
  }

  addEmail(email = '') {
    const element = this.fb.group({
      email: [{ value: email, disabled: !this.canCreateOrEdit }, [
        Validators.email,
        Validators.required
      ]],
    });
    this.emails.push(element);
  }

  deleteEmail(i) {
    this.emails.removeAt(i);
    this.form.markAsTouched();
  }

  get empresaAux() {
    return this.form.get('empresaAux');
  }

  set empresaAux(empresaAux: any) {
    this.empresaAux.setValue(empresaAux);
  }

  get empresa() {
    return this.form.get('empresa');
  }

  set empresa(empresa: any) {
    this.empresa.setValue(empresa);
  }

  get empresaId() {
    return this.form.get('empresaId');
  }

  set empresaId(empresaId: any) {
    this.empresaId.setValue(empresaId);
  }

  get fUltimaCompra() {
    return this.form.get('fUltimaCompra');
  }

  set fUltimaCompra(fUltimaCompra: any) {
    if (null != fUltimaCompra && '' != fUltimaCompra) {
      const d = new Date(fUltimaCompra.seconds * 1000);
      this.fUltimaCompra.setValue(d);
    } else {
      this.fUltimaCompra.setValue(null);
    }
  }

  /** @deprecated */
  get fUltimaReserva() {
    return this.form.get('fUltimaReserva');
  }

  /** @deprecated */
  set fUltimaReserva(fUltimaReserva: any) {
    if (null != fUltimaReserva && '' != fUltimaReserva) {
      const d = new Date(fUltimaReserva.seconds * 1000);
      this.fUltimaReserva.setValue(d);
    } else {
      this.fUltimaReserva.setValue(null);
    }
  }
  
  get fUltimaVisita() {
    return this.form.get('fUltimaVisita');
  }

  set fUltimaVisita(fUltimaVisita: any) {
    if (null != fUltimaVisita && '' != fUltimaVisita) {
      const d = new Date(fUltimaVisita.seconds * 1000);
      this.fUltimaVisita.setValue(d);
    } else {
      this.fUltimaVisita.setValue(null);
    }
  }

  get nombre() {
    return this.form.get('nombre');
  }

  set nombre(nombre) {
    this.nombre.setValue(nombre);
  }

  get observaciones() {
    return this.form.get('observaciones');
  }

  set observaciones(observaciones) {
    this.observaciones.setValue(observaciones);
  }

  get principal() {
    return this.form.get('principal');
  }

  set principal(principal) {
    this.principal.setValue(principal);
  }
  
  get punto() {
    return this.form.get('punto');
  }

  set punto(punto: any) {
    if (punto) {
      this.punto_latitude = punto.latitude;
      this.punto_longitude = punto.longitude;
      this.mapLat = punto.latitude;
      this.mapLng = punto.longitude;
    } else {
      this.punto_latitude = null;
      this.punto_longitude = null;
    }
  }

  get punto_latitude() {
    return this.punto.get('latitude');
  }

  set punto_latitude(latitude) {
    this.punto_latitude.setValue(latitude);
  }

  get punto_longitude() {
    return this.punto.get('longitude');
  }

  set punto_longitude(longitude) {
    this.punto_longitude.setValue(longitude);
  }

  get telefonos(): FormArray {
    return this.form.get('telefonos') as FormArray;
  }

  addTelefono(telefono = {tipo: '', numero: ''}) {
    const element = this.fb.group({
      tipo: [telefono.tipo, [
        Validators.required
      ]],
      numero: [this.formatPhone(telefono.numero), [
        Validators.required
      ]]
    });
    this.telefonos.push(element);
  }

  deleteTelefono(i) {
    this.telefonos.removeAt(i);
    this.form.markAsTouched();
  }

  /**
   * Format phone (With spaces for readability)
   */
  formatPhone(string: string): string {
    const onlyD = string.replace(/[^\d]/g,'');
    const l = onlyD.length;
    // Country code + Celphone
    if (l > 10) {
      return onlyD.slice(0, l-10) +' '+ onlyD.slice(l-10, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Cellphone or phone with country and city code
    if (l >= 9) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone with city code
    if (l >= 8) {
      return onlyD.slice(0, l-7) +' '+ onlyD.slice(l-7, l-4) +' '+ onlyD.slice(l-4, l);
    }
    // Phone
    if (l > 4) {
      return onlyD.slice(0, l-4) +' '+ onlyD.slice(l-4, l);
    } else {
      return onlyD;
    }
  }

  get tipo() {
    return this.form.get('tipo');
  }

  set tipo(tipo) {
    this.tipo.setValue(tipo);
  }

  get tipos(): FormArray {
    return this.form.get('tipos') as FormArray;
  }

  addTipo(tipo = {id: '', tipo: ''}) {
    const element = this.fb.group({
      id: [tipo.id, [
        //Validators.required
      ]],
      tipo: [tipo.tipo, [
        //Validators.required
      ]]
    });
    this.tipos.push(element);
  }

  deleteTipo(i) {
    this.tipos.removeAt(i);
    this.form.markAsTouched();
  }

  get instancia() {
    return this.form.get('instancia');
  }

  set instancia(instancia) {
    this.instancia.setValue(instancia);
  }

  get instancias(): FormArray {
    return this.form.get('instancias') as FormArray;
  }

  addInstancia(instancia = '') {
    const element = this.fb.group({
      instancia: [instancia, [
        Validators.required
      ]]
    });
    this.instancias.push(element);
  }

  deleteInstancia(i) {
    this.instancias.removeAt(i);
    this.form.markAsTouched();
  }

  get zoom() {
    return this.form.get('zoom');
  }

  set zoom(zoom: any) {
    this.zoom.setValue(zoom);
    this.mapZoom = zoom;
  }

  /** @deprecated */
  get creado() {
    return this.form.get('creado');
  }

  /** @deprecated */
  set creado(creado: any) {
    if (null != creado) {
      const d = new Date(creado.seconds * 1000);
      this.creado.setValue(new Date(creado.seconds * 1000));
    } else {
      this.creado.setValue(null);
    }
  }

  /** @deprecated */
  get creador() {
    return this.form.get('creador');
  }

  /** @deprecated */
  set creador(creador) {
    this.creador.setValue(creador);
  }

  /** @deprecated */
  get modificado() {
    return this.form.get('modificado');
  }

  /** @deprecated */
  set modificado(modificado: any) {
    if (null != modificado) {
      const d = new Date(modificado.seconds * 1000);
      this.modificado.setValue(d);
    } else {
      this.modificado.setValue(null);
    }
  }

  /** @deprecated */
  get modificador() {
    return this.form.get('modificador');
  }

  /** @deprecated */
  set modificador(modificador) {
    this.modificador.setValue(modificador);
  }
}
