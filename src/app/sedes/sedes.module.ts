import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule } from "@angular/forms";

import { AgmCoreModule } from "@agm/core";

import { SedesRoutingModule } from './sedes-routing.module';
import { SedesComponent } from './sedes.component';
import { SedeComponent } from './sede/sede.component';
import { SedeDetailsComponent } from './sede-details/sede-details.component';
import { SedeFormComponent } from './sede-form/sede-form.component';
import { SedeGridComponent } from './sede-grid/sede-grid.component';
import { SedeListComponent } from './sede-list/sede-list.component';
import { SedeSelectComponent } from './sede-select/sede-select.component';

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { SedeTiposModule } from '../sede-tipos/sede-tipos.module';
import { InstanciasModule } from '../instancias/instancias.module';
import { environment } from "../../environments/environment";

import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { UsuarioGridComponent } from '../usuarios/usuario-grid/usuario-grid.component';
import { SedeTipoGridComponent } from '../sede-tipos/sede-tipo-grid/sede-tipo-grid.component';
import { InstanciaGridComponent } from '../instancias/instancia-grid/instancia-grid.component';

@NgModule({
  declarations: [
    SedesComponent, 
    SedeComponent, 
    SedeDetailsComponent, 
    SedeFormComponent, 
    SedeGridComponent, 
    SedeListComponent, 
    SedeSelectComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({apiKey: environment.gmaps.apiKey}),
    SedesRoutingModule,
    CoreModule,
    SharedModule,
    AuthModule,
    UsuariosModule,
    SedeTiposModule,
    InstanciasModule
  ],
  exports: [
    SedeGridComponent,
    SedeFormComponent,
    SedeSelectComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    UsuarioGridComponent,
    SedeTipoGridComponent,
    InstanciaGridComponent
  ]
})
export class SedesModule { }
