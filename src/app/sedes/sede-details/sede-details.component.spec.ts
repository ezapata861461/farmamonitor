import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeDetailsComponent } from './sede-details.component';

describe('SedeDetailsComponent', () => {
  let component: SedeDetailsComponent;
  let fixture: ComponentFixture<SedeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
