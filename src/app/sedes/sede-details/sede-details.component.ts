import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { SedeAuthoService } from "../shared/sede-autho.service";
import { AuthService } from 'src/app/auth/shared/auth.service';

@Component({
  selector: 'app-sede-details',
  templateUrl: './sede-details.component.html',
  styleUrls: ['./sede-details.component.scss']
})
export class SedeDetailsComponent {

  // Route slug
  slug: string = 'sedes';

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: SedeAuthoService,
  ) { }

  /**
   * On form creation event
   * @param $event 
   */
  onCreated($event) {
    this.router.navigate([this.auth.instancia, this.slug, $event]);
  }

  /**
   * On form update event
   * @param $event 
   */
  onUpdated($event) {}

  /**
   * On form delete event
   * @param $event 
   */
  onDeleted() {
    this.router.navigate([this.auth.instancia, this.slug]);
  }
}
