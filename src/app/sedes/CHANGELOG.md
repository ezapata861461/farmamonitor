# Changelog

## [0.0.10] - 2019-12-13

### Changed
* Add some todos.

### Fixed
* Fix how form config is retrieved.

## [0.0.9] - 2019-12-08

### Changed
* Update how formParts configuration is retrieved (from modulos array field).

## [0.0.8] - 2019-11-25

## Changed
* Move google maps API key to environments.

## [0.0.7] - 2019-11-22

## Fixed
* Fix mainService in form (Has to be public).

## [0.0.6] - 2019-11-19

### Changed
* Add allowed roles.
* Adjust form parts visibility.

### Removed
* Remove Punto module dependency.

## [0.0.5] - 2019-11-06

## Added
Add instancia module.

### Changed
* Move types in interface to its own files.
* Add instancia field to model. Required to work with instanciaStrategy.
* Improve grid mat-toolbar.
* Remove bottom paginator.
* Catch memory leaks (subrcriptions).

### Fixed
* Force select popup to be closed only with close button.
* Fix error on map pointer set.
* Fix bug on map point selection.

### Removed
* Remove EmpresasModule import to avoid circular dependency.
* Replace empresa-select components with autocompletes.

## [0.0.4] - 2019-10-18

### Fixed
* Fix bug with matdatepicker.
* Init parts default options.
* Add missing options for 'telefonos'.

## [0.0.3] - 2019-10-17

### Changed
* Rename abstract classes to avoid legacy problems.
* Rename confirm-modal to confirm-dialog to avoid legacy problems.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.

### Changed
* Improve interface code and docs.
* Simplify how maps works.

## [0.0.1] - 2019-10-10

* First commit.