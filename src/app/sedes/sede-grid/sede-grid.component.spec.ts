import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeGridComponent } from './sede-grid.component';

describe('SedeGridComponent', () => {
  let component: SedeGridComponent;
  let fixture: ComponentFixture<SedeGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
