import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SedesComponent } from "./sedes.component";
import { SedeListComponent } from "./sede-list/sede-list.component";
import { SedeComponent } from "./sede/sede.component";
import { SedeDetailsComponent } from "./sede-details/sede-details.component";

import { RoleGuard } from './shared/role.guard';
/**se copia desde aca */
const routes: Routes = [
  {
    path: '',
    component: SedesComponent,
    canActivate: [RoleGuard],
    children: [
      {
        path: '',
        component: SedeListComponent,
      },
      {
        path: ':id',
        component: SedeComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles',
          },
          {
            path: 'detalles',
            component: SedeDetailsComponent
          }
        ]
      }
    ]
  }
];
/**se copia hasta aca */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SedesRoutingModule { }
