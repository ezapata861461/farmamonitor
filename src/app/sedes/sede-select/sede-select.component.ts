import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, Input, Output, EventEmitter, Optional, Self, OnDestroy } from '@angular/core';
import { FormBuilder, NgControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldControl } from '@angular/material/form-field';

import { AbsSelectComponent } from '../../core/abstract/abs-select.component';
import { SedeGridComponent } from '../sede-grid/sede-grid.component';
import { SedeService } from '../shared/sede.service';
import { SedeId } from '../shared/sede';

@Component({
  selector: 'win-sede-select',
  templateUrl: './sede-select.component.html',
  styleUrls: ['./sede-select.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: SedeSelectComponent }],
  host: {
    '[class.label-floating]': 'shouldLabelFloat',
    '[id]': 'id',
    '[attr.aria-describedby]': 'describedBy',
  }
})
export class SedeSelectComponent extends AbsSelectComponent implements OnDestroy {

  // "description" field name (to extract it from data)
  descrFieldName = 'nombre';
  // Current selected document
  document: SedeId;

  // EmpresaId to filter sedes by
  @Input()
  set empresaIdFilter(empresaIdFilter: string) { this._empresaIdFilter = empresaIdFilter }
  get empresaIdFilter(): string { return this._empresaIdFilter }
  protected _empresaIdFilter = '';

  // Emited events
  @Output() load = new EventEmitter<SedeId>();
  @Output() select = new EventEmitter<SedeId>();

  // Custom input type and id
  controlType = 'win-sede-select';
  id = `win-sede-select-${SedeSelectComponent.nextId++}`;

  constructor(
    fb: FormBuilder,
    protected mainService: SedeService,
    protected fm: FocusMonitor,
    protected elRef: ElementRef<HTMLElement>,
    public dialog: MatDialog,
    @Optional() @Self() public ngControl: NgControl,
  ) {
    super(fb, mainService, fm, elRef, ngControl);
  }

  /**
   * Open select popup and populate fields on select
   */
  openSelect() {
    const dialog = this.dialog.open(SedeGridComponent, {data: {}, width: '80%', height: '80%'});
    dialog.componentInstance.mode = 'select';

    // Filter grid by empresaId
    if ('' !== this.empresaIdFilter) {
      dialog.componentInstance.setFilter(this.empresaIdFilter);
    }
    
    dialog.componentInstance.isPopup = true;
    dialog.disableClose = true;
    this.subscriptions.push(
      this.dialogCloseSub = dialog.componentInstance.close.subscribe(() => { dialog.close() })
    );

    this.subscriptions.push(
      this.dialogSub = dialog.componentInstance.select.subscribe(document => {
        // Keep selected document
        this.document = document;
  
        // Load data into form fields
        this.inputId = document.id;
        this.inputDescr = document[this.descrFieldName];
  
        // Emit selected document
        this.select.emit(document);
  
        // Notify change and run change detection
        this.onChange(document.id);
        this.stateChanges.next();
        
        // Close select dialog
        dialog.close();
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
