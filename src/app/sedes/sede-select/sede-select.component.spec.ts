import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeSelectComponent } from './sede-select.component';

describe('SedeSelectComponent', () => {
  let component: SedeSelectComponent;
  let fixture: ComponentFixture<SedeSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
