# Changelog

## [0.0.2] - 2019-11-12

### Added
* Add Autocomplete module.

## [0.0.1] - 2019-09-10

* First commit.