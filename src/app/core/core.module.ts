import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";

import { AppMaterialModule } from "../app-material/app-material.module";

import { NavComponent } from "./nav/nav.component";
import { NotFoundComponent } from './not-found/not-found.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { LoadingComponent } from './loading/loading.component';
import { CargandoDialogComponent } from './cargando-dialog/cargando-dialog.component';
import { ConfirmDialogComponent } from "./confirm-dialog/confirm-dialog.component";
/** @deprecated Replaced by ConfirmDialogComponent */
import { ConfirmModalComponent } from "./confirm-modal/confirm-modal.component";


@NgModule({
  declarations: [
    NavComponent,
    NotFoundComponent,
    NotificationsComponent,
    LoadingComponent,
    CargandoDialogComponent,
    ConfirmDialogComponent,
    /** @deprecated Replaced by ConfirmDialogComponent */
    ConfirmModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AppMaterialModule
  ],
  exports: [
    NavComponent,
    NotFoundComponent,
    NotificationsComponent,
    LoadingComponent,
    ConfirmDialogComponent,
    /** @deprecated Replaced by ConfirmDialogComponent */
    ConfirmModalComponent
  ],
  entryComponents: [
    CargandoDialogComponent
  ]
})
export class CoreModule { }
