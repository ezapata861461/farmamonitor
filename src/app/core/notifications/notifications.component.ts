import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

import { NotifyService } from "../shared/notify.service";

@Component({
  selector: 'win-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit, OnDestroy {

  // Subscriptions
  private notifySub: Subscription;
  private snackSub: Subscription;

  constructor(
    private notify: NotifyService,
    private _snack: MatSnackBar
  ) {}

  ngOnInit() {
    this.notifySub = this.notify.msg.subscribe(msg => {
      if (msg) {
        const duration = ('success' == msg.style) ? 3000 : 5000;
        let snack = this._snack.open(msg.content, 'Ok', {
          duration: duration,
          panelClass: msg.style
        });
        this.snackSub = snack.onAction().subscribe(() => this.notify.clear());
      }
    });
  }

  ngOnDestroy() {
    this.notifySub.unsubscribe();
    if (undefined !== this.snackSub) { this.snackSub.unsubscribe(); }
  }
}
