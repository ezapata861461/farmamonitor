import { AngularFirestore } from '@angular/fire/firestore';
import { combineLatest, defer } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

/**
 * Operator that joins with a collection of documents, each with a field holding an asociated id
 * (Like one to many inner join)
 * Example: Join all asociated hebras to each hilo.
 * 
 * @check Source idea https://github.com/AngularFirebase/133-firestore-joins-custom-rx-operators
 * 
 * @param afs 
 * @param field 
 * @param collection 
 * @param limit 
 */
export const innerJoin = (
  afs: AngularFirestore,
  /** Where to find foreign key (hebra.hiloId). */
  field,
  /** Where to put joined documents (hilo.hebras). */
  collection,
  /** Limit number of joined nudos. Default 100. */
  limit = 100
) => {
  return source =>
    defer(() => {
      // Operator state
      let collectionData;

      // Track total num of joined doc reads
      let totalJoins = 0;

      return source.pipe(
        switchMap(data => {
          // Clear mapping on each emitted val ;

          // Save the parent data state
          collectionData = data as any[];

          const reads$ = [];
          for (const doc of collectionData) {
            // Push doc read to Array

            const q = ref => ref.where(field, '==', doc.id).limit(limit);
            reads$.push(afs.collection(collection, q).snapshotChanges());
          }
          
          return combineLatest(reads$);
        }),
        map(joins => {
          return collectionData.map((v, i) => {
            totalJoins += joins[i].length;
            return { ...v, [collection]: joins[i] || null };
          });
        }),
        tap(final => {
          /**
           * Uncomment to show join count
          console.log(
            `Queried ${(final as any).length}, Joined ${totalJoins} docs`
          );
           */
          totalJoins = 0;
        })
      );
    });
};