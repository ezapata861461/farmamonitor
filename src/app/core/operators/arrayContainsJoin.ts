import { AngularFirestore } from '@angular/fire/firestore';
import { combineLatest, defer } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

/**
 * Operator that joins with a collections of documents, each with an array of asociated ids.
 * Example: joins all asociated nudos to each doc. 
 * 
 * @check Source idea https://github.com/AngularFirebase/133-firestore-joins-custom-rx-operators
 * 
 * @param afs 
 * @param field 
 * @param collection 
 * @param limit 
 */
export const arrayContainsJoin = (
  afs: AngularFirestore,
  /** Where to find array of foreign keys (Example: nudo.collectionData). */
  field: string,
  /** Where to put joined documents (Example:  doc.nudos). */
  collection: string,
  /** Limit number of joined documents. Default 100. */
  limit = 100
) => {
  return source =>
    defer(() => {
      // Operator state
      let collectionData;

      // Track total num of joined doc reads
      let totalJoins = 0;

      return source.pipe(
        switchMap(data => {
          // Clear mapping on each emitted val ;

          // Save the parent data state
          collectionData = data as any[];

          const reads$ = [];
          for (const doc of collectionData) {
              // Push doc read to Array
              const q = ref => ref.where(field, 'array-contains', doc.id).limit(limit);
              reads$.push(afs.collection(collection, q).valueChanges());
          }
          
          return combineLatest(reads$);
        }),
        map(joins => {
          return collectionData.map((v, i) => {
            totalJoins += joins[i].length;
            return { ...v, [collection]: joins[i] || null };
          });
        }),
        tap(final => {
          /**
           * Uncomment to show join count
          console.log(
            `Queried ${(final as any).length}, Joined ${totalJoins} docs`
          );
           */
          totalJoins = 0;
        })
      );
    });
};