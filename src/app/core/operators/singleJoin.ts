import { AngularFirestore } from '@angular/fire/firestore';
import { combineLatest, defer, of } from "rxjs";
import { switchMap, map } from "rxjs/operators";

/**
 * Joins an object asociated by id
 * 
 * @check Source idea https://github.com/AngularFirebase/133-firestore-joins-custom-rx-operators
 * 
 * @param afs 
 * @param field 
 * @param collection 
 * @param putInField 
 */
export const singleJoin = (
  afs: AngularFirestore,
  field,      // Field to find key to query
  collection, // Collection to query related field
  putInField, // Field to put result
) => {
  return source =>
    defer(() => {
      // Operator state
      let collectionData;

      return source.pipe(
        switchMap(data => {
          // Clear mapping on each emitted val ;
          // Save the parent data state
          collectionData = data as any[];
          const reads$ = [];
          for (const doc of collectionData) {
            const reserva = afs.doc(collection + '/' + doc[field]).valueChanges();
            reads$.push(reserva);
          }

          // If reads is empty, add null observable to
          // make combineLatest work as intended
          if (0 == reads$.length) {
            reads$.push(of(null));
          }

          return combineLatest(reads$);
        }),
        map(reads => {
          return collectionData.map((v, i) => {
            return { ...v, [putInField]: reads[i] || null }
          });
        })
      );
    });
};