# Changelog

## [0.0.12] - 2019-12-18

### Bug
* Fix bug with abs-firestore mergeDateAndTime method.

## [0.0.11] - 2019-12-08

### Changed
* Update how instanciaStrategy, itemCount is retrieved (from modulos array field).
* Add filter capability to search method.
* Update how nav items are shown/hidden.

## [0.0.10] - 2019-12-07

### Changed
* Make abs-firestore.service properties protected instead of private, to allow access to children.
* Fix error in CHANGELOG versions.

## [0.0.9] - 2019-12-02

### Added
* Add mergeDateAndTime method to abs-firestore service.
* Add 'citas' and 'productos' modules to menu.

## [0.0.8] - 2019-12-01

## Changed
* Adjust duration time for notifications.
* Add 'huso' module to menu.

## [0.0.7] - 2019-11-26

### Changed
* Improve not-found content with menu and message.
* Improve nav order. Conditionally show menus on user privileges.

## [0.0.6] - 2019-11-21

### Changed
* Fix links.

## [0.0.5] - 2019-11-18

### Changed
* Set default instanciaStrategy to 'none'.
* Improve nav to show only enabled components and only allow roles with canRead privilege.
* Improve how searchString is cleaned, specially for popup grid.

## [0.0.4] - 2019-11-12

### Added
* Add new operators for joins.
* Allow pagination enable/disable on grid.
* Improve grid code.
* Add instancia strategy and clena code for abs-firestore service.

### Changed
* Improve unsubscribe handling in abs-select component.
* When close, clean searchstring for grid on popup view.

## [0.0.3] - 2019-10-17

### Added
* Add legacy abstract classes.
* Add legacy confirm-modal component.

### Changed
* Rename abstract classes to avoid legacy problems.
* Rename confirm-modal to confirm-dialog to avoid legacy problems.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.

## [0.0.1] - 2019-09 10

* First commit.