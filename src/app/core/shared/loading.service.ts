import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private _loadintState = new BehaviorSubject<boolean | null>(null);

  loadingState = this._loadintState.asObservable();

  loading() {
    if (!this._loadintState.value) { // Avoid duplicated triggers
      this._loadintState.next(true);
    }
  }

  loaded() {
    if (this._loadintState.value) { // Avoid duplicated triggers
      this._loadintState.next(false);
    }
  }
}
