import { Injectable, isDevMode } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ErrorLogService {

  constructor(
    private afs: AngularFirestore
  ) {}

  /**
   * Log error in database and in console (optional).
   * @param error 
   * @param showInConsole 
   */
  log(error: Error, showInConsole = true) {
    // Show errors on console only during development or if required
    if (isDevMode || showInConsole) {
      console.error(error);
    }

    // Try to save error in firebase
    this.afs.collection('errors').add(error)
      .then(() => { })
      .catch(error => {});
  }
}