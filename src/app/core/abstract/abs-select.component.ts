import { Input, Output, DoCheck, ElementRef, EventEmitter, OnDestroy } from "@angular/core";
import { FormGroup, NgControl, FormBuilder, ControlValueAccessor } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FocusMonitor } from '@angular/cdk/a11y';
import { Subject, Subscription } from 'rxjs';

/**
 * If doubts about this, check original example
 * https://github.com/angular/material2/blob/master/src/material-examples/form-field-custom-control/form-field-custom-control-example.ts
 */

/**
 * How to implement example:
 * 
  <win-destino-select [formControl]="destinoId" 
    [showClear]="true"                    // If want to show clear button
    (select)="onDestinoSelect($event)"    // Destino selection event handler. Use it to load other controls / Update database.
    (load)="onDestinoLoad($event)"        // Destino loaded (when value already exists). Use it to load other controls.
    (clear)="onClear()"                   // Destino cleared event handler. IDEM...
    [busy]="busy"                         // Use this to make control busy while updating data on database
    [readOnly]="true">                    // Use it to turn it into readOnly. Default false;
  </win-destino-select>
  */

export abstract class AbsSelectComponent implements ControlValueAccessor, MatFormFieldControl<string>, DoCheck, OnDestroy {
  static nextId = 0;
  parts: FormGroup;
  stateChanges = new Subject<void>();
  focused = false;
  errorState = false;
  describedBy = '';
  onChange = (_: any) => {};
  onTouched = () => {};

  // "description" field name (to extract it from data)
  abstract descrFieldName: string;
  // Current selected document
  abstract document: any;

  // Set as read only
  @Input()
  get readOnly(): boolean { return this._readOnly; }
  set readOnly(readOnly: boolean) { this._readOnly = readOnly; }
  protected _readOnly: boolean = false;

  // Show clear icon
  @Input()
  get showClear(): boolean { return this._showClear; }
  set showClear(showClear: boolean) { this._showClear = showClear; }
  protected _showClear: boolean = false;

  /**
   * Flag to make input field busy/not-busy (loading = true) while parent form
   * does something (updatews database for example).
   */
  @Input()
  get busy(): boolean { return this._busy; }
  set busy(busy: boolean) { 
    this._busy = busy;
    this.loading = busy; 
  }
  protected _busy: boolean = false;

  // Emited events
  @Output() abstract load = new EventEmitter<any>();
  @Output() abstract select = new EventEmitter<any>();
  @Output() clear = new EventEmitter<boolean>();

  // Custom input type and id
  abstract controlType;
  abstract id;

  // Component state
  loading: boolean = true;

  // Subscriptions
  dialogSub: Subscription;
  dialogCloseSub: Subscription;

  get empty() {
    const id = this.inputId.value;
    return !id;
  }

  get shouldLabelFloat() { return this.focused || !this.empty; }

  @Input()
  get placeholder(): string { return this._placeholder; }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }
  private _placeholder: string;

  @Input()
  get required(): boolean { return this._required; }
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }
  private _required = false;

  @Input()
  get disabled(): boolean { return this._disabled; }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this._disabled ? this.parts.disable() : this.parts.enable();
    this.stateChanges.next();
  }
  private _disabled = false;

  @Input()
  get value(): string | null {
    const id = this.inputId.value;
    if (id.length > 0) {
      return id;
    }
    return null;
  }

  set value(id: string | null) {
    // If no id, clean form values
    if (!id) {
      this.inputId.setValue('');
      this.inputDescr.setValue('');
      this.onLoaded();
      return;
    }

    // Load description to input field
    this.subscriptions.push(
      this.mainService.getDoc(id).subscribe(document => {
        this.inputId.setValue(document.id);
        this.inputDescr.setValue(document[this.descrFieldName]);
        this.onLoaded(document);
      })
    );

    this.stateChanges.next();
  }

  subscriptions: Subscription[] = [];

  constructor(
    fb: FormBuilder,
    protected mainService: any,
    protected fm: FocusMonitor,
    protected elRef: ElementRef<HTMLElement>,
    public ngControl: NgControl,
  ) {

    // Init form
    this.parts = fb.group({
      inputId: '',
      inputDescr: '',
    });

    // Element Reference Monitor
    this.subscriptions.push(
      fm.monitor(elRef, true).subscribe(origin => {
        if (this.focused && !origin) {
          this.onTouched();
        }
        this.focused = !!origin;
        this.stateChanges.next();
      })
    );

    // Init value accessor
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  /**
   * Control error check
   * @todo: Resolver el problema que se genera cuando el control es requerido,
   * pero la validación se dispara al inicializar el formulario.
   * Ya intenté con todo, incluido los estados del control y del parent (touched, pristine, etc...).
   */
  ngDoCheck(): void {
    if (this.ngControl) {
      this.errorState = this.ngControl.invalid;
      this.stateChanges.next();
    }
  }

  /**
   * Destroy component
   */
  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.elRef);
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  /**
   * Click event handler
   * @param event
   */
  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() != 'input') {
      this.elRef.nativeElement.querySelector('input')!.focus();
    }
  }

  /**
   * Custom input required methods
   * @param ids 
   */
  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  writeValue(val: string | null): void {
    this.value = val;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  _handleInput(): void {
    this.onChange(this.parts.value);
  }

  /**
   * Clear values and run change detection
   */
  onClear() {
    this.inputId.setValue('');
    this.inputDescr.setValue('');
    this.onChange('');
    this.stateChanges.next();
    this.clear.emit(true);
  }

  /**
   * On loading event
   */
  onLoading(): void {
    this.loading = true;
  }

  /**
   * On loaded handler
   */
  onLoaded(document = null): void {
    if (document) {
      this.load.emit(document);
    }
    this.loading = false;
  }

  /**
   * Form getters and Setters
   */
  get inputId() {
    return this.parts.get('inputId');
  }

  set inputId(inputId) {
    this.inputId.setValue(inputId);
    this.stateChanges.next();
  }

  get inputDescr() {
    return this.parts.get('inputDescr');
  }

  set inputDescr(inputDescr) {
    this.inputDescr.setValue(inputDescr);
  }
}