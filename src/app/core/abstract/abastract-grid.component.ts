import { OnInit, OnDestroy, Input, Output, 
  EventEmitter, ViewChild }                 from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTableDataSource } from "@angular/material/table";
import { SelectionModel }                   from '@angular/cdk/collections';
import { FormGroup, FormControl }           from "@angular/forms";
import { Subscription }                     from 'rxjs';
import { debounceTime, tap }                from 'rxjs/operators';

import { ConfirmModalComponent }            from "../confirm-modal/confirm-modal.component";

export abstract class AbstractGridComponent implements OnInit, OnDestroy {

  // Route slug
  abstract slug: string;

  // Mat table definition
  abstract dataSource: MatTableDataSource<any>;
  abstract selection: SelectionModel<any>;

  // Mat table columns definition
  private _columns: string[];
  set columns(columns) { this._columns = columns }
  get columns() {
    // If mode is edit or multiselect, add 'select' column
    if ('edit' == this.mode || 'multiselect' == this.mode) {
      if (this._columns.indexOf('select') < 0) {
        this._columns.unshift('select');
      }
    }
    return this._columns;
  }

  // Loading state
  loading = true;

  // Mode for grid to work
  // 'readOnly', 'edit', 'select' (individual select) or 'multiselect'.
  protected _mode = 'readOnly';
  @Input()
  set mode(mode: string) { this._mode = mode }
  get mode(): string { return this._mode }

  /**
   * User permissions
   */
  @Input()
  set canCreate(canCreate: boolean)  { this._canCreate = canCreate }
  get canCreate(): boolean { return this._canCreate }
  protected _canCreate = false;

  @Input()
  set canDelete(canDelete: boolean) { this._canDelete = canDelete }
  get canDelete(): boolean { return this._canDelete }
  protected _canDelete = false;

  @Input()
  set canSoftdelete(canSoftdelete: boolean) { this._canSoftdelete = canSoftdelete }
  get canSoftdelete(): boolean { return this._canSoftdelete }
  protected _canSoftdelete =  false;

  @Input()
  set canFile(canFile: boolean) { this._canFile = canFile }
  get canFile(): boolean { return this._canFile }
  protected _canFile = false;

  // Back button for wizards
  @Input()
  set showBackButton(showBackButton: boolean) { this._showBackButton = showBackButton }
  get showBackButton() { return this._showBackButton }
  protected _showBackButton = false;

  @Input()
  set backButtonText(backButtonText: string) { this._backButtonText = backButtonText }
  get backButtonText() { return this._backButtonText }
  protected _backButtonText = 'Anterior';
  // Select button text
  @Input()
  set selectButtonText(selectButtonText: string) { this._selectButtonText = selectButtonText }
  get selectButtonText(): string { return this._selectButtonText }
  protected _selectButtonText = 'Seleccionar';

  // Allow select empty (on multiselect)
  @Input()
  set allowSelectEmpty(allowSelectEmpty: boolean) { this._allowSelectEmpty = allowSelectEmpty }
  get allowSelectEmpty(): boolean { return this._allowSelectEmpty }
  protected _allowSelectEmpty = false;

  // Allow search permission
  @Input()
  set allowSearch(allowSearch: boolean) { this._allowSearch = allowSearch }
  get allowSearch(): boolean { return this._allowSearch }
  protected _allowSearch = true;

  // Input to filter data. Triggers setFilter abstract function.
  @Input()
  set filter(filter: string) { 
    this._filter = filter;
    this.setFilter(filter);
  }
  get filter(): string { return this._filter }
  protected _filter = '';

  /**
   * Abstract function to set grid filter. Triggered by filter input.
   * @param filter 
   */
  abstract setFilter(filter: string);

  // Emited events
  @Output() abstract select: EventEmitter<any[] | any>; // Selected ítems emmitter. Child must define type.
  @Output() back = new EventEmitter<boolean>();         // Back button event emitter
  @Output() create = new EventEmitter<boolean>();
  @Output() deleted = new EventEmitter<boolean>();

  // Search form 
  searchIcon: string = 'search';
  searchForm = new FormGroup({
    searchString: new FormControl('')
  })
  get searchString() { return this.searchForm.get('searchString') }
  set searchString(searchString) { this.searchString.setValue(searchString) }

  // Paginator definition
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  // Subscriptions
  searchSub: Subscription;
  colSub: Subscription;
  pagSub: Subscription;
  confirmSub: Subscription;

  constructor(
    protected mainService: any,
    protected confirm: MatDialog,
    protected snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    // Enable / Disable search
    if (!this.allowSearch) {
      this.searchString.disable();
    } else {
      // Watch search form
      this.searchSub = this.searchString.valueChanges.pipe(
        debounceTime(300)
      )
      .subscribe(searchStr => {
        this.searchIcon = ('' === searchStr) ?  'search' : 'clear';
        this.paginator.firstPage();
        this.mainService.searchString = searchStr;
      });
    }

    // Restore last search if any (from service)
    if ('' !== this.mainService.searchString) {
      this.searchString.setValue(this.mainService.searchString);
    }

    // Watch paginator changes
    this.pagSub = this.paginator.page
      .pipe(tap(() => this.onPageChange()))
      .subscribe();
  }

  ngAfterViewInit() {
    // Restore last opened page (from service)
    this.paginator.pageIndex = this.mainService.pageIndex;

    // Load data into datasource
    this.colSub = this.mainService.col$.subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.onLoaded();
      },
      error => {
        // console.log(error); /** @todo: Remove after checking that all indexes were generated */
        this.onError(error, error.message);
        this.onLoaded();
      }
    );
  }

  /**
   * Clear search field
   */
  clearSearch() {
    this.searchString.setValue('');
    this.paginator.firstPage();
  }

  /**
   * Whether the number of selected elements matches the total number of rows.
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /**
   * Selects all rows if they are not all selected; otherwise clear selection.
   */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /**
   * Checkbox toggle handler
   * @param $event 
   * @param row 
   */
  checkboxToggle($event, row) {
    // Update row
    $event ? this.selection.toggle(row) : null;
  }

  /**
   * Create button click handler
   */
  onCreate() {
    this.create.emit(true);
  }

  /**
   * Grid delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmModalComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar los registr seleccionados?'},
    });

    this.confirmSub = confirm.afterClosed().subscribe(result => {
      if (result) {
        this.onLoading();
        this.mainService.deleteCollection(this.selection.selected)
          .then(x => {
            this.selection.clear();
            this.onLoaded('Borrado!');
            this.deleted.emit(true);
          })
          .catch(error => {
            this.onError(error);
            this.onLoaded();
          });
      }
    });
  }

  /**
   * Row click event handler
   * @param row 
   */
  onRowClick(row) {
    // On edit mode
    if ('edit' === this.mode || 'select' === this.mode) {
      this.select.emit(row);
    }
  }

  /**
   * Back button click event
   */
  onBack() {
    this.back.emit(true);
  }

  /**
   * Select button click event
   */
  onSelect() {
    this.select.emit(this.selection.selected);
  }

  /**
   * Handles page index change
   */
  onPageChange() {
    this.mainService.pageIndex = this.paginator.pageIndex;
    this.mainService.pageSize = this.paginator.pageSize;
  }

  /**
   * Data loading event
   */
  onLoading(): void {
    this.loading = true;
  }

  /**
   * On request loaded handler
   */
  onLoaded(msg = ''): void {
    if ('' != msg) {
      this.notify(msg);
    }
    this.loading = false;
  }

  /**
   * Set row class depending on mode
   * @param row 
   */
  setRowClickClass(row) {
    // If edit mode
    if ('edit' == this.mode) {
      return 'win-clickable-row';
    // If select mode
    } else if ('select' == this.mode) {
      return 'win-clickable-row';
    }
  }

  /**
   * Shows snackbar with message
   * @param msg 
   */
  notify(msg) {
    this.snackBar.open(msg, 'OK', { duration: 3000 });
  }
  
  /**
   * Error handler
   * @todo: Code error handler
   */
  onError(error, msg = '') {
    if ('' != msg) {
      this.notify(msg);
    }
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    if (undefined !== this.searchSub) {
      this.searchSub.unsubscribe();
    }
    if (undefined !== this.colSub) {
      this.colSub.unsubscribe();
    }
    if (undefined !== this.pagSub) {
      this.pagSub.unsubscribe();
    }
    if (undefined !== this.confirmSub) {
      this.confirmSub.unsubscribe();
    }
  }
}