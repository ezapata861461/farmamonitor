import { OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormControl } from "@angular/forms";
import { Subscription } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

import { ConfirmDialogComponent } from "../confirm-dialog/confirm-dialog.component";
import { ErrorLogService } from '../shared/error-log.service';
import { NotifyService } from '../shared/notify.service';
import { LoadingService } from '../shared/loading.service';

export abstract class AbsGridComponent implements OnInit, OnDestroy {

  // Mat table definition
  abstract dataSource: MatTableDataSource<any>;
  abstract selection: SelectionModel<any>;

  // Mat table columns definition
  private _columns: string[];
  set columns(columns) { this._columns = columns }
  get columns() {
    // If mode is edit or multiselect, add 'select' column
    if ('edit' == this.mode || 'multiselect' == this.mode) {
      if (this._columns.indexOf('select') < 0) {
        this._columns.unshift('select');
      }
    }
    return this._columns;
  }

  // Mode for grid to work
  // 'readOnly', 'edit', 'select' (individual select) or 'multiselect'.
  protected _mode = 'readOnly';
  @Input()
  set mode(mode: string) { this._mode = mode }
  get mode(): string { return this._mode }

  // If grid is in a popup or not
  @Input()
  set isPopup(isPopup: boolean) { this._isPopup = isPopup }
  get isPopup(): boolean { return this._isPopup }
  protected _isPopup =  false;

  // User permissions
  @Input()
  set canCreate(canCreate: boolean)  { this._canCreate = canCreate }
  get canCreate(): boolean { return this._canCreate }
  protected _canCreate = false;

  @Input()
  set canDelete(canDelete: boolean) { this._canDelete = canDelete }
  get canDelete(): boolean { return this._canDelete }
  protected _canDelete = false;

  @Input()
  set canSoftdelete(canSoftdelete: boolean) { this._canSoftdelete = canSoftdelete }
  get canSoftdelete(): boolean { return this._canSoftdelete }
  protected _canSoftdelete =  false;

  @Input()
  set canFile(canFile: boolean) { this._canFile = canFile }
  get canFile(): boolean { return this._canFile }
  protected _canFile = false;

  /** Show hide back button (for wizards) */
  @Input()
  set showBackButton(showBackButton: boolean) { this._showBackButton = showBackButton }
  get showBackButton() { return this._showBackButton }
  protected _showBackButton = false;

  /** Back button text */
  @Input()
  set backButtonText(backButtonText: string) { this._backButtonText = backButtonText }
  get backButtonText() { return this._backButtonText }
  protected _backButtonText = 'Anterior';

  /** Select button text (Enabled on select or multiselect mode) */
  @Input()
  set selectButtonText(selectButtonText: string) { this._selectButtonText = selectButtonText }
  get selectButtonText(): string { return this._selectButtonText }
  protected _selectButtonText = 'Seleccionar';

  /** Allow select empty (on multiselect) */
  @Input()
  set allowSelectEmpty(allowSelectEmpty: boolean) { this._allowSelectEmpty = allowSelectEmpty }
  get allowSelectEmpty(): boolean { return this._allowSelectEmpty }
  protected _allowSelectEmpty = false;

  /** Allow search permission */
  @Input()
  set allowSearch(allowSearch: boolean) { this._allowSearch = allowSearch }
  get allowSearch(): boolean { return this._allowSearch }
  protected _allowSearch = true;

  /** Enable/disable pagination */
  @Input()
  set paginate(paginate: boolean) { 
    this._paginate = paginate
    this.paginator.disabled = !this.paginate;
    this.mainService.paginate = this.paginate;
  }
  get paginate(): boolean { return this._paginate }
  protected _paginate: boolean = true;

  searching: boolean = false;

  // Input to filter data. Triggers setFilter abstract function.
  @Input()
  set filter(filter: string) { 
    this._filter = filter;
    this.setFilter(filter);
  }
  get filter(): string { return this._filter }
  protected _filter = '';

  /**
   * Abstract function to set grid filter. Triggered by filter input.
   * @param filter 
   */
  abstract setFilter(filter: string);

  // Emited events
  @Output() abstract select: EventEmitter<any[] | any>; // Selected ítems emmitter. Child must define type.
  @Output() back = new EventEmitter<boolean>();         // Back button event emitter
  @Output() create = new EventEmitter<boolean>();
  @Output() deleted = new EventEmitter<boolean>();
  @Output() close = new EventEmitter<boolean>();

  // Search form 
  searchIcon: string = 'search';
  searchForm = new FormGroup({
    searchString: new FormControl('')
  })
  get searchString() { return this.searchForm.get('searchString') }
  set searchString(searchString) { this.searchString.setValue(searchString) }

  // Paginator definition
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  subscriptions: Subscription[] = [];

  constructor(
    protected mainService: any,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService
  ) {}

  ngOnInit() {
    this.onLoading();

    // Enable/disable pagination
    this.paginator.disabled = !this.paginate;
    this.mainService.paginate = this.paginate;

    // Enable / Disable search
    if (!this.allowSearch) {
      this.searchString.disable();
    } else {
      // Watch search form
      this.subscriptions.push(
        this.searchString.valueChanges.pipe(
          debounceTime(300)
        )
        .subscribe(searchStr => {
          if (this.paginate) { this.paginator.firstPage() }
          this.mainService.searchString = searchStr;
        })
      );
    }

    // Restore last search if any (from service)
    if ('' !== this.mainService.searchString) {
      this.searchString.setValue(this.mainService.searchString);
    }

    // Watch paginator changes
    this.subscriptions.push(
      this.paginator.page
        .pipe(tap(() => this.onPageChange()))
        .subscribe()
    );
  }

  ngAfterViewInit() {
    // Restore last opened page (from service)
    if (this.paginate) { this.paginator.pageIndex = this.mainService.pageIndex }

    // Load data into datasource
    this.subscriptions.push(
      this.mainService.col$.subscribe(
        data => {
          this.dataSource = new MatTableDataSource(data);
          this.onLoaded();
        },
        error => {
          this.onError(error, 'Error al cargar los datos.');
          this.onLoaded();
        }
      )
    );
  }

  /**
   * Clear search field
   */
  clearSearch() {
    if (this.searching) {
      if (this.paginate) { this.paginator.firstPage() }
      this.searching = false;
    } else {
      this.searching = true;
    }
    // Clean search string to handle popup grid close/open
    if ('' !== this.searchString.value) {
      this.searchString.setValue('');
    }
  }

  /**
   * Whether the number of selected elements matches the total number of rows.
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /**
   * Selects all rows if they are not all selected; otherwise clear selection.
   */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /**
   * Checkbox toggle handler
   * @param $event 
   * @param row 
   */
  checkboxToggle($event, row) {
    // Update row
    $event ? this.selection.toggle(row) : null;
  }

  /**
   * Create button click handler
   */
  onCreate() {
    this.create.emit(true);
  }

  /**
   * Grid delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmDialogComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar los registros seleccionados?'},
    });

    this.subscriptions.push(
      confirm.afterClosed().subscribe(result => {
        if (result) {
          this.onLoading();
          this.mainService.deleteCollection(this.selection.selected)
            .then(x => {
              this.selection.clear();
              this.onLoaded('Borrado!');
              this.deleted.emit(true);
            })
            .catch(error => {
              this.onError(error, 'Error al intentar borrar. Por favor intente de nuevo.');
              this.onLoaded();
            });
        }
      })
    );
  }

  /**
   * Row click event handler
   * @param row 
   */
  onRowClick(row) {
    // On edit mode
    if ('edit' === this.mode || 'select' === this.mode) {
      this.select.emit(row);
    }
  }

  /**
   * Back button click event
   */
  onBack() {
    this.back.emit(true);
  }

  /**
   * Select button click event
   */
  onSelect() {
    this.select.emit(this.selection.selected);
  }

  /**
   * Handles page index change
   */
  onPageChange() {
    if (this.paginate) {
      this.mainService.pageIndex = this.paginator.pageIndex;
      this.mainService.pageSize = this.paginator.pageSize;
    }
  }

  /**
   * Close button click (only on popup mode)
   */
  onClose() {
    this.clearSearch();
    this.mainService.searchString = '';
    this.close.emit(true);
  }

  /**
   * Data loading event
   */
  onLoading(): void {
    this.loading.loading();
  }

  /**
   * On request loaded handler
   */
  onLoaded(msg = ''): void {
    if ('' != msg) {
      this.notify.update(msg, 'info');
    }
    this.loading.loaded();
  }

  /**
   * Set row class depending on mode
   * @param row 
   */
  setRowClickClass(row) {
    // If edit mode
    if ('edit' == this.mode) {
      return 'win-clickable-row';
    // If select mode
    } else if ('select' == this.mode) {
      return 'win-clickable-row';
    }
  }
  
  /**
   * Error handler
   */
  onError(error, msg = '', showInConsole = false) {
    this.errorLog.log(error, showInConsole);
    if ('' != msg) {
      this.notify.update(msg, 'error');
    }
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}