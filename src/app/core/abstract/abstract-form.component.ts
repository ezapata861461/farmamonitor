import { OnInit, OnDestroy, Input, 
  Output, EventEmitter }                from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormGroup }                    from '@angular/forms';
import { Subscription }                 from 'rxjs';
import { Router }                       from '@angular/router';

import { ConfirmModalComponent }        from "../../core/confirm-modal/confirm-modal.component";

export abstract class AbstractFormComponent implements OnInit, OnDestroy {

  // Base route
  abstract slug: string;

  // Main form
  form: FormGroup;

  // Loading state
  loading = true;

  // If form is in a popup or not
  @Input()
  set isPopup(isPopup: boolean) { this._isPopup = isPopup }
  get isPopup(): boolean { return this._isPopup }
  protected _isPopup =  false;

  @Input()
  set canCreate(canCreate: boolean) {
    this._canCreate = canCreate;
    this.enableForm(); // deal with asyn inputs
  }
  get canCreate(): boolean { return this._canCreate }
  protected _canCreate = false;
  
  @Input()
  set canEdit(canEdit: boolean) { 
    this._canEdit = canEdit;
    this.enableForm(); // deal with asyn inputs
  }
  get canEdit(): boolean { return this._canEdit }
  protected _canEdit = false;
  
  @Input()
  set canDelete(canDelete: boolean) {
    this._canDelete = canDelete;
    this.enableForm();
  }
  get canDelete(): boolean { return this._canDelete }
  protected _canDelete = false;
  
  @Input()
  set canSoftDelete(canSoftDelete: boolean) {
    this._canSoftDelete = canSoftDelete;
    this.enableForm(); // deal with asyn inputs
  }
  get canSoftDelete(): boolean { return this._canSoftDelete }
  protected _canSoftDelete = false;
  
  @Input()
  set canFile(canFile: boolean) {
    this._canFile = canFile;
    this.enableForm(); // deal with asyn inputs
  }
  get canFile(): boolean { return this._canFile }
  protected _canFile = false;

  @Output() created = new EventEmitter<string>();
  @Output() updated = new EventEmitter<boolean>();
  @Output() deleted = new EventEmitter<boolean>();
  @Output() close = new EventEmitter<boolean>();

  // Subscriptions
  docSub: Subscription
  confirmSub: Subscription;

  constructor(
    protected router: Router,
    protected mainService: any,
    protected confirm: MatDialog,
    protected snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.form = this.buildForm();
    this.loadControls();
    this.docSub = this.mainService.doc$.subscribe(
      doc => {
        this.populateForm(doc);
      },
      error => {
        this.onError(error, error.message);
        this.onLoaded();
      }
    )
  }

  /**
   * Builds reactive form for component
   */
  abstract buildForm(): FormGroup;

  /**
   * Load other controls, like autocomplete, lists and others
   */
  abstract loadControls(): void;

  /**
   * Populate form from doc
   * @param doc 
   */
  abstract populateForm(data: any): void;

  /**
   * Save button click handler.
   * 
   * @param raw: Allow to include or not disabled fields
   */
  onSave(raw: boolean = false) {
    this.onLoading();

    // Check if save rawForm or not
    let promise;
    if (raw) {
      promise = this.mainService.save(this.form.getRawValue());
    } else {
      promise = this.mainService.save(this.form.value);
    }
    
    // Run save
    promise.then(doc => {
      // If creation
      if (undefined !== doc) {
        this.mainService.id = doc.id;
        this.created.emit(doc.id);
        this.onLoaded('Creado!');
      // If edition
      } else {
        this.updated.emit(true);
        this.onLoaded('Guardado!');
      }
      this.form.markAsUntouched();
      this.form.markAsPristine();
    })
    .catch(error => {
      this.onError(error, error.message);
      this.onLoaded();
    });
  }

  /**
   * Delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmModalComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar este registro?'},
    });

    this.confirmSub = confirm.afterClosed().subscribe(result => {
      if (result) {
        this.onLoading();
        this.mainService.delete(this.id.value)
          .then(() => { this.deleted.emit() })
          .catch(error => { this.onError(error); });
      }
    });
  }

  /**
   * Close button click (only on popup mode)
   */
  onClose() {
    if (!this.form.dirty) {
      this.close.emit(true);
    } else {
      const confirm = this.confirm.open(ConfirmModalComponent, {
        width: '80%',
        data: {content: 'Hay cambios sin guardar ¿Seguro que quiere cerrar?'}
      });
  
      this.confirmSub = confirm.afterClosed().subscribe(result => {
        if (result) {
          this.onLoading();
          this.close.emit(true);
        }
      });
    }
  }

  /**
   * Navigate to grid
   */
  navigateToGrid() {
    this.router.navigate([this.slug]);
  }

  /**
   * Data loading event
   */
  onLoading(): void {
    this.loading = true;
  }

  /**
   * On request loaded handler
   */
  onLoaded(msg = ''): void {
    if ('' != msg) {
      this.notify(msg);
    }
    this.enableForm();
    this.loading = false;
  }

  /**
   * Enable form fields after loading process
   * This allows to overwrite this function to enable/disable fields
   * 
   * @todo: Se deshabilita el código. Cada formulario particular debería de
   *  hacer su implementación de enable form. Por lo pronto queda comentado.
   * 
   *  enableForm corre despuésd del estado 'loading', para poner el formulario
   *  activo nuevamente. Aquí es donde se habilitan/deshabilitan campos.
   *  Y también corre cuando ocurre algún setter de permisos, para refrescar los
   *  permisos de los campos durante el ciclo de carga.
   */
  enableForm() {
    /*if (undefined !== this.form) {
      this.form.enable();
    }*/
  }

  /**
   * Disable form edition during loading
   * 
   * @todo: Validar cuando se usa esto. El lío es que si se deshabilita el formulario durante
   *  el guardado, los valores no se guardan, debido a que todos funcionan con form.value y
   *  para poder utilizar esto deben funcionar con form.rawValue.
   */
  disableForm() {
    this.form.disable();
  }

  /**
   * Shows snackbar with message
   * @param msg 
   */
  notify(msg) {
    this.snackBar.open(msg, 'OK', {
      duration: 3000,
    });
  }
  
  /**
   * Error handler
   * @todo: Code error handler
   */
  onError(error, msg = '') {
    if ('' != msg) {
      this.notify(msg);
    }
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    if (this.docSub) { this.docSub.unsubscribe(); }
    if (this.confirmSub) { this.confirmSub.unsubscribe(); }
  }

  /**
   * Form getters & setters
   */

  get id() {
    return this.form.get('id');
  }

  set id(id) {
    this.id.setValue(id);
  }

  get creado() {
    return this.form.get('creado');
  }

  set creado(creado: any) {
    if (null != creado) {
      const d = new Date(creado.seconds * 1000);
      this.creado.setValue(new Date(creado.seconds * 1000));
    } else {
      this.creado.setValue(null);
    }
  }

  get creador() {
    return this.form.get('creador');
  }

  set creador(creador) {
    this.creador.setValue(creador);
  }

  get modificado() {
    return this.form.get('modificado');
  }

  set modificado(modificado: any) {
    if (null != modificado) {
      const d = new Date(modificado.seconds * 1000);
      this.modificado.setValue(d);
    } else {
      this.modificado.setValue(null);
    }
  }

  get modificador() {
    return this.form.get('modificador');
  }

  set modificador(modificador) {
    this.modificador.setValue(modificador);
  }
}