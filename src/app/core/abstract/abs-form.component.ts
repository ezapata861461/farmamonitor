import { OnInit, OnDestroy, Input, 
  Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { ConfirmDialogComponent } from "../confirm-dialog/confirm-dialog.component";
import { ErrorLogService } from '../shared/error-log.service';
import { NotifyService } from '../shared/notify.service';
import { LoadingService } from '../shared/loading.service';

export abstract class AbsFormComponent implements OnInit, OnDestroy {

  // Base route
  abstract slug: string;

  // Main form
  form: FormGroup;

  // If form is in a popup or not
  @Input()
  set isPopup(isPopup: boolean) { this._isPopup = isPopup }
  get isPopup(): boolean { return this._isPopup }
  protected _isPopup =  false;

  @Input()
  set canCreate(canCreate: boolean) {
    this._canCreate = canCreate;
    this.enableForm(); // deal with asyn inputs
  }
  get canCreate(): boolean { return this._canCreate }
  protected _canCreate = false;
  
  @Input()
  set canEdit(canEdit: boolean) { 
    this._canEdit = canEdit;
    this.enableForm(); // deal with asyn inputs
  }
  get canEdit(): boolean { return this._canEdit }
  protected _canEdit = false;
  
  @Input()
  set canDelete(canDelete: boolean) {
    this._canDelete = canDelete;
    this.enableForm();
  }
  get canDelete(): boolean { return this._canDelete }
  protected _canDelete = false;
  
  @Input()
  set canSoftDelete(canSoftDelete: boolean) {
    this._canSoftDelete = canSoftDelete;
    this.enableForm(); // deal with asyn inputs
  }
  get canSoftDelete(): boolean { return this._canSoftDelete }
  protected _canSoftDelete = false;
  
  @Input()
  set canFile(canFile: boolean) {
    this._canFile = canFile;
    this.enableForm(); // deal with asyn inputs
  }
  get canFile(): boolean { return this._canFile }
  protected _canFile = false;

  @Output() created = new EventEmitter<string>();
  @Output() updated = new EventEmitter<boolean>();
  @Output() deleted = new EventEmitter<boolean>();
  @Output() close = new EventEmitter<boolean>();

  subscriptions: Subscription[] = [];

  constructor(
    protected router: Router,
    protected mainService: any,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService
  ) {}

  ngOnInit() {
    this.onLoading();
    this.form = this.buildForm();
    this.loadControls();
    this.subscriptions.push(
      this.mainService.doc$.subscribe(
        doc => {
          this.populateForm(doc);
        },
        error => {
          this.onError(error, error.message);
          this.onLoaded();
        }
      )
    );
  }

  /**
   * Builds reactive form for component
   */
  abstract buildForm(): FormGroup;

  /**
   * Load other controls, like autocomplete, lists and others
   */
  abstract loadControls(): void;

  /**
   * Populate form from doc
   * @param doc 
   */
  abstract populateForm(data: any): void;

  /**
   * Save button click handler.
   * 
   * @param raw: Allow to include or not disabled fields
   */
  onSave(raw: boolean = false) {
    this.onLoading();

    // Check if save rawForm or not
    let promise;
    if (raw) {
      promise = this.mainService.save(this.form.getRawValue());
    } else {
      promise = this.mainService.save(this.form.value);
    }
    
    // Run save
    promise.then(doc => {
      // If creation
      if (undefined !== doc) {
        this.mainService.id = doc.id;
        this.created.emit(doc.id);
        this.onLoaded('Creado!');
      // If edition
      } else {
        this.updated.emit(true);
        this.onLoaded('Guardado!');
      }
      this.form.markAsUntouched();
      this.form.markAsPristine();
    })
    .catch(error => {
      this.onError(error, error.message);
      this.onLoaded();
    });
  }

  /**
   * Delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmDialogComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar este registro?'},
    });

    this.subscriptions.push(
      confirm.afterClosed().subscribe(result => {
        if (result) {
          this.onLoading();
          this.mainService.delete(this.id.value)
            .then(() => { this.deleted.emit() })
            .catch(error => { this.onError(error); });
        }
      })
    );
  }

  /**
   * Close button click (only on popup mode)
   */
  onClose() {
    if (!this.form.dirty) {
      this.close.emit(true);
    } else {
      const confirm = this.confirm.open(ConfirmDialogComponent, {
        width: '80%',
        data: {content: 'Hay cambios sin guardar ¿Seguro que quiere cerrar?'}
      });
  
      this.subscriptions.push(
        confirm.afterClosed().subscribe(result => {
          if (result) {
            this.onLoading();
            this.close.emit(true);
          }
        })
      );
    }
  }

  /**
   * Navigate to grid
   */
  navigateToGrid() {
    this.router.navigate([this.slug]);
  }

  /**
   * Data loading event
   */
  onLoading(): void {
    this.loading.loading();
  }

  /**
   * On request loaded handler
   */
  onLoaded(msg = ''): void {
    if ('' != msg) {
      this.notify.update(msg, 'info');
    }
    this.loading.loaded();
  }

  /**
   * Enable form fields after loading process
   * This allows to overwrite this function to enable/disable fields
   * 
   * @todo: Se deshabilita el código. Cada formulario particular debería de
   *  hacer su implementación de enable form. Por lo pronto queda comentado.
   * 
   *  enableForm corre despuésd del estado 'loading', para poner el formulario
   *  activo nuevamente. Aquí es donde se habilitan/deshabilitan campos.
   *  Y también corre cuando ocurre algún setter de permisos, para refrescar los
   *  permisos de los campos durante el ciclo de carga.
   */
  enableForm() {
    /*if (undefined !== this.form) {
      this.form.enable();
    }*/
  }

  /**
   * Disable form edition during loading
   * 
   * @todo: Validar cuando se usa esto. El lío es que si se deshabilita el formulario durante
   *  el guardado, los valores no se guardan, debido a que todos funcionan con form.value y
   *  para poder utilizar esto deben funcionar con form.rawValue.
   */
  disableForm() {
    this.form.disable();
  }
  
  /**
   * Error handler
   */
  onError(error, msg = '', showInConsole = false) {
    this.errorLog.log(error, showInConsole);
    if ('' != msg) {
      this.notify.update(msg, 'error');
    }
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  /**
   * Form getters & setters
   */

  get id() {
    return this.form.get('id');
  }

  set id(id) {
    this.id.setValue(id);
  }

  get fCreado() {
    return this.form.get('fCreado');
  }

  set fCreado(fCreado: any) {
    if (null != fCreado) {
      const d = new Date(fCreado.seconds * 1000);
      this.fCreado.setValue(new Date(fCreado.seconds * 1000));
    } else {
      this.fCreado.setValue(null);
    }
  }

  get creadorId() {
    return this.form.get('creadorId');
  }

  set creadorId(creadorId) {
    this.creadorId.setValue(creadorId);
  }

  get fModificado() {
    return this.form.get('fModificado');
  }

  set fModificado(fModificado: any) {
    if (null != fModificado) {
      const d = new Date(fModificado.seconds * 1000);
      this.fModificado.setValue(d);
    } else {
      this.fModificado.setValue(null);
    }
  }

  get modificadorId() {
    return this.form.get('modificadorId');
  }

  set modificadorId(modificadorId) {
    this.modificadorId.setValue(modificadorId);
  }
}
