import { Injectable, OnDestroy } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from "@angular/fire/firestore";
import { DocumentReference } from '@firebase/firestore-types';
import * as firebase from 'firebase/app';
import 'firebase/firestore'
import { BehaviorSubject, Observable, combineLatest, of, Subscription } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { AuthService } from 'src/app/auth/shared/auth.service';

@Injectable({
  providedIn: 'root'
})
export abstract class AbsFirestoreService implements OnDestroy {

  /** 
   * Strategy used to separate data by instancia.
   *  none: None instancia asociated.
   *    This for collections that don't require any instancia control
   *  field: Add afield conaining a unique instanciaId.
   *    This for collections where only one instancia should be set.
   *  arrayField: Adds an array containing a list of IDs from asociated instancias.
   *    This for collections where documents can belong to several instancias.
   *  separatedCollection: Creates a separated collection named collectionName_instanciaId like personas_default.
   *    This for collections that required to be isolated (due to expected grouth or separation of data).
   */
  set instanciaStrategy(instanciaStrategy: InstanciaStrategy) { this._instanciaStrategy$.next(instanciaStrategy) }
  get instanciaStrategy(): InstanciaStrategy { return this._instanciaStrategy$.value }
  protected _instanciaStrategy$: BehaviorSubject<InstanciaStrategy> = new BehaviorSubject('none');

  /**
   * Property to define default strategy, in case it's not defined in database.
   * This, in case this service requires to pass value, not avaliable in database.
   */
  defaultInstanciaStrategy: InstanciaStrategy = 'arrayField';

  /** 
   * Collection prefix to handle how to point to nested collections. 
   * Example 'empresas/Axm1234fdsMrjha' to point to nested 'sedes' collection.
   */
  set collectionPrefix(collectionPrefix: string|null) { this._collectionPrefix = collectionPrefix }
  get collectionPrefix(): string|null { return this._collectionPrefix }
  private _collectionPrefix: string|null = null;

  /** Collection queried by service */
  set collectionName(collectionName: string|null) { 
    this._collectionName = collectionName;
  }
  get collectionName(): string|null {
    let colName: string;

    // If separated collection strategy, build path
    // For example: instancias/{instancia}/personas/{persona}
    // All in the same collection, but separated in nested collection by instancia
    if ('separatedCollection' === this.instanciaStrategy) {
      colName = 'instancias/' + this.auth.instancia + '/' + this._collectionName;

    // Otherwise, just add prefix
    } else {
      // Add collection prefix (nested collections)
      if (null === this.collectionPrefix || '' === this.collectionPrefix) {
        colName = this._collectionName;
      } else {
        colName = this._collectionPrefix + '/' + this._collectionName;
      }
    }
    return colName;
  }
  protected _collectionName: string|null = null;

  /**
   * ***********************************************
   * ************ doc$ input parameters ************
   * ***********************************************
   */

    /** Current object id */
    set id(id: string) { this._id$.next(id) }
    get id() { return this._id$.value }
    private _id$: BehaviorSubject<string> = new BehaviorSubject('0');
  /** ******** END doc$ input parameters ******** */

  /**
   * ***********************************************
   * ************ col$ input parameters ************
   * ***********************************************
   */

    /** Array name to filter col$ by array-contains */
    set containsArray(containsArray: string) { this._containsArray$.next(containsArray) }
    get containsArray(): string|null { return this._containsArray$.value }
    protected _containsArray$: BehaviorSubject<string|null> = new BehaviorSubject(null);

    /** value to filter col$ by array-contains */
    set containsValue(containsValue: string) { this._containsValue$.next(containsValue) }
    get containsValue(): string|null { return this._containsValue$.value }
    protected _containsValue$: BehaviorSubject<string|null> = new BehaviorSubject(null);

    /** Filter field for col$. (where(filterBy = filter)) clause. */
    set filterBy(filterBy: string) { this._filterBy$.next(filterBy) }
    get filterBy() { return this._filterBy$.value }
    protected _filterBy$: BehaviorSubject<string|null> = new BehaviorSubject(null);

    /** Filter value for col$. (where(filterBy = filter)) clause. */
    set filter(filter: string) { this._filter$.next(filter) }
    get filter() { return this._filter$.value }
    protected _filter$: BehaviorSubject<string|null> = new BehaviorSubject(null);

    /** 
     * Instancia name (from auth service).
     * @deprecated: Instancia can be obtained automatically from auth service!
     */
    set instancia(instancia: boolean) { this._instancia$.next(instancia) }
    get instancia(): boolean { return this._instancia$.value }
    protected _instancia$: BehaviorSubject<boolean|null> = new BehaviorSubject(true);

    /** Paginate data or not */
    public paginate: boolean = true;

    /** Page index for col$ */
    set pageIndex(pageIndex: number) { this._pageIndex$.next(pageIndex) }
    get pageIndex() { return this._pageIndex$.value }
    protected _pageIndex$: BehaviorSubject<number|null> = new BehaviorSubject(0);

    /** Page size for col$ */
    set pageSize(pageSize: number) { this._pageSize$.next(pageSize) }
    get pageSize() { return this._pageSize$.value }
    protected _pageSize$: BehaviorSubject<number|null> = new BehaviorSubject(50);

    /** Search string to filter col$. */
    set searchString(searchString: string) { this._searchString$.next(searchString) }
    get searchString() { return this._searchString$.value }
    protected _searchString$: BehaviorSubject<string|null> = new BehaviorSubject('');

    /** Sort field for col$. */
    set sort(sort: string) { this._sort$.next(sort) }
    get sort() { return this._sort$.value }
    protected _sort$: BehaviorSubject<string|null> = new BehaviorSubject(null);

    /** Sort value for col$. */
    set sortDir(sortDir: string) { this._sortDir$.next(sortDir) }
    get sortDir() { return this._sortDir$.value }
    protected _sortDir$: BehaviorSubject<string|null> = new BehaviorSubject(null);

    /** Cursor for pagination. */
    public cursor: Array<any> = [];

    /** itemCount for pagination. */
    public itemCount: number = 100;

  /** ******** END col$ input parameters ******** */

  /** Current loaded collection. */
  abstract col$: Observable<any[]>;

  /** Current loaded document. */
  abstract doc$: BehaviorSubject<any | null>;

  /** Array to hold subscriptions */
  public subscriptions: Subscription[] = [];

  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService
  ) {
    // Get collection strategy from config.
    // This defines how data is separated by instancia
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        this.instanciaStrategy = (undefined !== config.modulos[this._collectionName] && config.modulos[this._collectionName].strategy) ? config.modulos[this._collectionName].strategy : this.defaultInstanciaStrategy;
      })
    );
  }

  /**
   * Implements common items.map method
   * @param items 
   */
  abstract mapItems(items);

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  abstract getDocumentReference(id): AngularFirestoreDocument;

  /**
   * Return an empty object (Service main object)
   */
  abstract createEmpty();

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  abstract extract(fd);

  /**
   * Init current collection
   */
  initCol() {
    // Get item count from instancia config
    if (this.paginate) {
      this.subscriptions.push(
        this.auth.config.subscribe(config => {
          this.itemCount = (undefined !== config.modulos[this._collectionName] && config.modulos[this._collectionName].count) ? config.modulos[this._collectionName].count : 0;
        })
      );
    }

    this.col$ = combineLatest(
      this._searchString$,
      this._pageIndex$,
      this._pageSize$,
      this._sort$,
      this._sortDir$,
      this._filterBy$,
      this._filter$,
      this._instanciaStrategy$,
      this._containsArray$,
      this._containsValue$
    ).pipe(
      switchMap(([searchString, pageIndex, pageSize, sort, sortDir, filterBy, filter, instanciaStrategy, containsArray, containsValue]) =>
        this.afs.collection(this.collectionName, ref => {
          let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
          
          // Filter data, depending on instancia strategy
          // But avoid filtering by arrayField, when an 'array-contains' filter is set for another service
          if (('arrayField' !== this.instanciaStrategy) || ('arrayField' === this.instanciaStrategy && !(containsArray && containsValue))) {
            query = this.setInstanciaFilter(query);
          }

          // Filter by array contains only if instanciaStrategy is not arrayField
          // Can´t filter by more than one array-contains clause
          if (containsArray && containsValue) {
            query = query.where(containsArray, 'array-contains', containsValue);
          }

          // Filter by
          if (filter && filterBy) { query = query.where(filterBy, '==', filter) }

          // Sort data by one field
          if (sort) {
            if ('asc' === sortDir) {
              query = query.orderBy(sort, 'asc');
            } else if ('desc' === sortDir) {
              query = query.orderBy(sort, 'desc');
            } else {
              query = query.orderBy(sort, 'asc');
            }
          }

          // Search by exact match
          if (searchString && '' !== searchString) {
            query = this.setSearchQuery(query, searchString, pageSize);
          }

          // Set paging
          if (this.paginate) {
            if (pageIndex) { query = query.startAfter(this.cursor[pageIndex]) }
            if (pageSize) { query = query.limit(pageSize) }
          }
          
          return query;

        }).snapshotChanges().pipe(
          map(items => {
            let data = this.mapItems(items);

            // Build next page cursor
            if (this.paginate) {
              if (items.length > 0) {
                this.cursor[pageIndex + 1] = items[items.length - 1].payload.doc;
              }
            }
            
            return data;
          }) // map
        ) // pipe 2
      )
    ); // pipe 1
  }

  /**
   * Init current document
   */
  initDoc() {
    this.subscriptions.push(
      combineLatest(
        this._id$
      ).pipe(
        switchMap(([id]) => {
          if ('0' === id) {
            return of(this.createEmpty());
          } else {
            return this.getDocumentReference(id).valueChanges().pipe(
              map(doc => { 
                if (undefined != doc) {
                  return {id, ...doc};
                } else {
                  return doc;
                }
              })
            );
          }
        })
      ).subscribe(doc => {
        this.doc$.next(doc);
      })
    );
  }

  /**
   * Filter current collection by field and value
   * @param filterBy 
   * @param filter 
   */
  filterColBy(filterBy: string, filter: string): void {
    // Move to first page
    this.pageIndex = 0;
    // Filter
    this.filterBy = filterBy;
    this.filter = filter;
  }

  /**
   * Return collection filtered by string (for autocomplete)
   * @param searchStr 
   */
  search(searchStr: string, field: string, filterBy: string = '', filter: string = ''): Observable<any[]> {
    const start: string = searchStr;
    const end: string = start + '\uf8ff';
    return this.afs.collection(this.collectionName, ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

      // Filter data, depending on instancia strategy
      query = this.setInstanciaFilter(query);

      // Filter data by
      if ('' !== filterBy && '' !== filter) { query = query.where(filterBy, '==', filter) }

      // Set search query
      query = query.orderBy(field);
      query = query.limit(5);
      query = query.startAt(start);
      query = query.endAt(end);
      return query;

    }).snapshotChanges().pipe(
      map( items => this.mapItems(items) )
    );
  }

  /**
   * Returns a document by id
   * @param id 
   */
  getDoc(id: string) {
    return this.getDocumentReference(id).valueChanges().pipe(
      map(doc => ({id, ...doc }))
    );
  }

  /**
   * Returns a collection filtered by
   * If not filters, then returns the whole collection
   * If sort and sortDir defined, it returns sorted collection
   * @param filterBy 
   * @param filter 
   * @param sort
   * @param sortDir
   */
  getCollectionBy(filterBy: string = null, filter: string = null, sort: string = null, sortDir: string = null) {
    return this.afs.collection(this.collectionName, ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

      // Filter data, depending on instancia strategy
      query = this.setInstanciaFilter(query);

      // Filter data by
      if (filter && filterBy) {
        if (filter && filterBy) { query = query.where(filterBy, '==', filter) }
      }

      // Sort
      if (sort && sortDir) {
        if ('asc' === sortDir) {
          query = query.orderBy(sort, 'asc');
        } else if ('desc' === sortDir) {
          query = query.orderBy(sort, 'desc');
        } else {
          query = query.orderBy(sort, 'asc');
        }
      }

      return query;
    }).snapshotChanges().pipe(
      map( items => this.mapItems(items))
    );
  }

  /**
   * Save (create/update) a document
   * @param data 
   */
  save(data: any): Promise<any> {
    const collectionRef = this.getCollectionReference();
    const docData: any = this.extract(data);

    // If edition
    if ('0' !== data.id && undefined !== data.id) {
      return collectionRef.doc(data.id).set(docData);
    // Otherwise, creation
    } else {
      return collectionRef.add(docData);
    }
  }

  /**
   * Delete a document
   * @param id 
   */
  delete(id: string): Promise<any> {
    const collectionRef = this.getCollectionReference();
    return collectionRef.doc(id).delete();
  }

  /**
   * Delete an array of documents
   * @param dataArray 
   */
  deleteCollection(dataArray): Promise<any> {
    // Create batch
    const batch = firebase.firestore().batch();

    // Add each document to batch and commit
    dataArray.forEach(element => {
      const docRef = this.getBatchDocument(element.id);
      batch.delete(docRef);
    });
    return batch.commit();
  }

  /**
   * Builds search query.
   * Can be overwritten from child service
   * @param query 
   * @param searchString 
   * @param pageSize 
   */
  protected setSearchQuery(query: firebase.firestore.CollectionReference | firebase.firestore.Query, 
    searchString: string, pageSize: number): firebase.firestore.CollectionReference | firebase.firestore.Query {
  
  const start: string = searchString;
  const end: string = start + '\uf8ff';
  query = query.limit(pageSize);
  query = query.startAt(start);
  query = query.endAt(end);
  return query;
}

  /**
   * Add required filter for instancia, depending on instanciaStrategy
   * @param query 
   */
  protected setInstanciaFilter(query) {
    // Filter data, depending on instancia strategy
    switch (this.instanciaStrategy) {
      case 'field':
        if (this.auth.instancia) { 
          query = query.where('instancia', '==', this.auth.instancia);
        } else {
          /** @todo: Throw error instancia doesn´t exist.*/
        }
        break;

      case 'arrayField':
        if (this.auth.instancia) { 
          query = query.where('instancias', 'array-contains', this.auth.instancia);
        } else {
          /** @todo: Throw error instancia doesn´t exist.*/
        }
        break;

      case 'none':
      case 'separatedCollection':
        break;
    }
    return query;
  }

  /**
   * Returns an AngularFirestoreCollection reference
   */
  protected getCollectionReference(): AngularFirestoreCollection {
    return this.afs.collection(this.collectionName);
  }

  /**
   * Returns a DocumentReference from document id
   * @param documentId 
   */
  protected getBatchDocument(documentId): DocumentReference {
    return firebase.firestore().doc(this.collectionName + '/' + documentId);
  }

  /**
   * Get server timestamp
   */
  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  /**
   * Merge a string with a time value into a date and returns merged date
   * @param d (date)
   * @param t (time)
   */
  mergeDateAndTime(d: Date, t: string|null = null): Date { 
    // If time is not valid, return date as it is
    if (null === t || '' === t) {
      return d;
    }

    // Create new date to avoid weird data behaviour
    let newDate = new Date();

    // Extract time
    const parts = t.match(/(\d+)\:(\d+) (\w+)/);

    // Extract hour and minutes
    const hours = /am/i.test(parts[3]) ?
      function(am) {return am < 12 ? am : 0}(parseInt(parts[1], 10)) :
      function(pm) {return pm < 12 ? pm + 12 : 12}(parseInt(parts[1], 10));

    const minutes = parseInt(parts[2], 10);

    // Set values to date
    newDate.setFullYear(d.getFullYear());
    newDate.setMonth(d.getMonth());
    newDate.setDate(d.getDate());
    newDate.setHours(hours);
    newDate.setMinutes(minutes);

    return newDate;
  }

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}