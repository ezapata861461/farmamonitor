import { Injectable }                 from '@angular/core';
import { AngularFirestore, 
  AngularFirestoreDocument,
  AngularFirestoreCollection }        from "@angular/fire/firestore";
import * as firebase                  from 'firebase/app';
import 'firebase/firestore'
import { DocumentReference }          from '@firebase/firestore-types';
import { BehaviorSubject, Observable, 
  combineLatest }                     from 'rxjs';
import { switchMap, map }             from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractFirestoreService {

  // Collection name
  abstract collectionName: string;
  // Current loaded collection
  abstract col$: Observable<any[]>;
  // Current loaded document
  abstract doc$: BehaviorSubject<any | null>;

  // Current object / grid parameters
  private _id$: BehaviorSubject<string> = new BehaviorSubject('0');
  set id(id: string) { this._id$.next(id) }
  get id() { return this._id$.value }

  private _searchString$: BehaviorSubject<string|null> = new BehaviorSubject('');
  set searchString(searchString: string) { this._searchString$.next(searchString) }
  get searchString() { return this._searchString$.value }

  private _pageIndex$: BehaviorSubject<number|null> = new BehaviorSubject(0);
  set pageIndex(pageIndex: number) { this._pageIndex$.next(pageIndex) }
  get pageIndex() { return this._pageIndex$.value }

  private _pageSize$: BehaviorSubject<number|null> = new BehaviorSubject(50);
  set pageSize(pageSize: number) { this._pageSize$.next(pageSize) }
  get pageSize() { return this._pageSize$.value }

  private _sort$: BehaviorSubject<string|null> = new BehaviorSubject(null);
  set sort(sort: string) { this._sort$.next(sort) }
  get sort() { return this._sort$.value }

  private _sortDir$: BehaviorSubject<string|null> = new BehaviorSubject(null);
  set sortDir(sortDir: string) { this._sortDir$.next(sortDir) }
  get sortDir() { return this._sortDir$.value }

  private _filterBy$: BehaviorSubject<string|null> = new BehaviorSubject(null);
  set filterBy(filterBy: string) { this._filterBy$.next(filterBy) }
  get filterBy() { return this._filterBy$.value }

  private _filter$: BehaviorSubject<string|null> = new BehaviorSubject(null);
  set filter(filter: string) { this._filter$.next(filter) }
  get filter() { return this._filter$.value }

  // Cursor and itemsCount count for pagination
  cursor: Array<any> = [];
  itemsCount: number = 100;

  constructor(
    protected afs: AngularFirestore,
  ) {}

  /**
   * Implements common items.map method
   * @param items 
   */
  abstract mapItems(items);

  /**
   * Return Angular Firestore Document (Service main object)
   * @param id 
   */
  abstract getDocumentReference(id): AngularFirestoreDocument;

  /**
   * Return an empty object (Service main object)
   */
  abstract createEmpty();

  /**
   * Extracts data from form or other object
   * @param fd (Form data)
   */
  abstract extract(fd);

  /**
   * Init current collection
   */
  initCol() {
    this.col$ = combineLatest(
      this._searchString$,
      this._pageIndex$,
      this._pageSize$,
      this._sort$,
      this._sortDir$,
      this._filterBy$,
      this._filter$,
    ).pipe(
      switchMap(([searchString, pageIndex, pageSize, sort, sortDir, filterBy, filter]) =>
        this.afs.collection(this.collectionName, ref => {
          let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

          // Filter by
          if (filter && filterBy) { query = query.where(filterBy, '==', filter) }

          // Sort data by one field
          if (sort) {
            if ('asc' === sortDir) {
              query = query.orderBy(sort, 'asc');
            } else if ('desc' === sortDir) {
              query = query.orderBy(sort, 'desc');
            } else {
              query = query.orderBy(sort, 'asc');
            }
          }

          // Search by exact match
          if (searchString && '' !== searchString) {
            query = this.buildSearchQuery(query, searchString, pageSize);
          }

          // Set paging
          if (pageIndex) { query = query.startAfter(this.cursor[pageIndex]) }
          if (pageSize) { query = query.limit(pageSize) }

          return query;

        }).snapshotChanges().pipe(
          map(items => {
            let data = this.mapItems(items);

            // If items count is less than pageSize
            if (items.length < pageSize) {
              // Check if items is empty to avoid showing more than one last empty page
              if (0 === items.length) {
                this.itemsCount = pageSize * pageIndex;
                return data;
              
              // Otherwise, is last page then limit itemsCount to this last items
              } else {
                this.itemsCount = (pageSize * pageIndex) + items.length;
                return data;
              }
            }

            // If items count is equal to pageSize, then it could be another page
            if (items.length === pageSize) {
              // Add itemCount to show more pages if required
              if (this.itemsCount <= (pageSize * (pageIndex + 1))) {
                this.itemsCount = (pageSize * (pageIndex + 2));
              }
            }

            // Build next page cursor
            this.cursor[pageIndex + 1] = items[items.length - 1].payload.doc;

            return data;
          }) // map
        ) // pipe 2
      )
    ); // pipe 1
  }

  /**
   * Builds search query.
   * Can be overwritten from child service
   * @param query 
   * @param searchString 
   * @param pageSize 
   */
  buildSearchQuery(query: firebase.firestore.CollectionReference | firebase.firestore.Query, 
      searchString: string, pageSize: number): firebase.firestore.CollectionReference | firebase.firestore.Query {
    
    const start: string = searchString;
    const end: string = start + '\uf8ff';
    query = query.limit(pageSize);
    query = query.startAt(start);
    query = query.endAt(end);
    return query;
  }

  /**
   * Init current document
   */
  initDoc() {
    combineLatest(
      this._id$
    ).pipe(
      switchMap(([id]) => this.getDocumentReference(id)
        .valueChanges().pipe(
          map(doc => { 
            if (undefined != doc) {
              return {id, ...doc};
            } else {
              return doc;
            }
          })
        )
      )
    ).subscribe(doc => {
      if (undefined != doc) {
        this.doc$.next(doc);
      } else {
        this.doc$.next(this.createEmpty());
      }
    });
  }

  /**
   * Filter current collection by field and value
   * @param filterBy 
   * @param filter 
   */
  filterColBy(filterBy: string, filter: string): void {
    // Move to first page
    this.pageIndex = 0;
    // Filter
    this.filterBy = filterBy;
    this.filter = filter;
  }

  /**
   * Return collection filtered by string (for autocomplete)
   * @param searchStr 
   */
  search(searchStr: string, field: string): Observable<any[]> {
    const start: string = searchStr;
    const end: string = start + '\uf8ff';
    return this.afs.collection(this.collectionName, ref => 
      ref
        .orderBy(field)
        .limit(5)
        .startAt(start)
        .endAt(end)
    ).snapshotChanges().pipe(
      map( items => this.mapItems(items) )
    );
  }

  /**
   * Returns a document by id
   * @param id 
   */
  getDoc(id: string) {
    return this.getDocumentReference(id).valueChanges().pipe(
      map(doc => ({id, ...doc }))
    );
  }

  /**
   * Returns a collection filtered by
   * If not filters, then returns the whole collection
   * If sort and sortDir defined, it returns sorted collection
   * @param filterBy 
   * @param filter 
   * @param sort
   * @param sortDir
   */
  getCollectionBy(filterBy: string = null, filter: string = null, sort: string = null, sortDir: string = null) {
    return this.afs.collection(this.collectionName, ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

      if (filter && filterBy) {
        if (filter && filterBy) { query = query.where(filterBy, '==', filter) }
      }

      if (sort && sortDir) {
        if ('asc' === sortDir) {
          query = query.orderBy(sort, 'asc');
        } else if ('desc' === sortDir) {
          query = query.orderBy(sort, 'desc');
        } else {
          query = query.orderBy(sort, 'asc');
        }
      }

      return query;
    }).snapshotChanges().pipe(
      map( items => this.mapItems(items))
    );
  }

  /**
   * Save (create/update) a document
   * @param data 
   */
  save(data: any): Promise<any> {
    const collectionRef = this.getCollectionReference();
    const docData: any = this.extract(data);

    // If edition
    if ('0' !== data.id && undefined !== data.id) {
      return collectionRef.doc(data.id).set(docData);
    // Otherwise, creation
    } else {
      return collectionRef.add(docData);
    }
  }

  /**
   * Delete a document
   * @param id 
   */
  delete(id: string): Promise<any> {
    const collectionRef = this.getCollectionReference();
    return collectionRef.doc(id).delete();
  }

  /**
   * Delete an array of documents
   * @param dataArray 
   */
  deleteCollection(dataArray): Promise<any> {
    // Create batch
    const batch = firebase.firestore().batch();

    // Add each document to batch and commit
    dataArray.forEach(element => {
      const docRef = this.getBatchDocument(element.id);
      batch.delete(docRef);
    });
    return batch.commit();
  }

  /**
   * Returns an AngularFirestoreCollection reference
   */
  protected getCollectionReference(): AngularFirestoreCollection {
    return this.afs.collection(this.collectionName);
  }

  /**
   * Returns a DocumentReference from document id
   * @param documentId 
   */
  protected getBatchDocument(documentId): DocumentReference {
    return firebase.firestore().doc(this.collectionName + '/' + documentId);
  }

  /**
   * Get server timestamp
   */
  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }
}