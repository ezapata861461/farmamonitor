import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from "rxjs";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";

import { LoadingService } from "../shared/loading.service";
import { CargandoDialogComponent } from '../cargando-dialog/cargando-dialog.component';

@Component({
  selector: 'win-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

  private dialoRef: MatDialogRef<any>;

  // Subscriptions
  private loadingSub: Subscription;

  constructor(
    private loading: LoadingService,
    private _dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loadingSub = this.loading.loadingState.subscribe(loadingState => {
      if (loadingState) {
        this.dialoRef = this._dialog.open(CargandoDialogComponent, {
          width: '150px',
          position: { top: '15px'},
          disableClose: true,
          panelClass: 'win-loading-dialog',
          backdropClass: 'backdrop-class',
        })
      } else {
        try {
          this.dialoRef.close();
        } catch (error) { }
        this.dialoRef = null;
      }
    });
  }

  ngOnDestroy() {
    this.loadingSub.unsubscribe();
  }
}