import { Component, OnInit } from '@angular/core';

import { LoadingService } from "../shared/loading.service";

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(
    private loading: LoadingService
  ) { }

  ngOnInit() {
    this.loading.loaded();
  }
}
