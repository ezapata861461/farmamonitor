import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { AuthService } from "../../auth/shared/auth.service";
import { LoadingService } from '../shared/loading.service';
import { Instancia } from 'src/app/instancias/shared/instancia';
import { EmpresaAuthoService } from 'src/app/empresas/shared/empresa-autho.service';
import { PersonaAuthoService } from 'src/app/personas/shared/persona-autho.service';
import { SedeAuthoService } from 'src/app/sedes/shared/sede-autho.service';
import { SedeTipoAuthoService } from 'src/app/sede-tipos/shared/sede-tipo-autho.service';
import { UsuarioAuthoService } from 'src/app/usuarios/shared/usuario-autho.service';
import { InstanciaAuthoService } from 'src/app/instancias/shared/instancia-autho.service';
import { Modulo } from 'src/app/instancias/shared/modulo';

@Component({
  selector: 'win-app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit, OnDestroy {
  
  displayName: string = '';
  photoURL: string = '';
  instancia: Instancia;
  modulos: { [a: string]: Modulo};

  // Aux autho vars
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;
  showInstanciaSwitch: boolean = false;

  subscriptions: Subscription[] = [];

  constructor(
    public auth: AuthService,
    protected router: Router,
    private loading: LoadingService,
    public instanciaAutho: InstanciaAuthoService,
    public usuarioAutho: UsuarioAuthoService,
    public empresaAutho: EmpresaAuthoService,
    public personaAutho: PersonaAuthoService,
    public sedeAutho: SedeAuthoService,
    public sedeTipoAutho: SedeTipoAuthoService
  ) { }

  ngOnInit() {
    // Get username and photo
    this.subscriptions.push(
      this.auth.user.subscribe(user => {
        if(user) {
          this.displayName = user.displayName;
          this.photoURL = user.photoURL;

          // Init autho vars
          this.isSuperUser = this.usuarioAutho.hasRol(['webmaster', 'admin']);
          this.isWebmaster = this.usuarioAutho.hasRol(['webmaster']);
          if (user.instancias.length > 1) {
            this.showInstanciaSwitch = true;
          }
        }
      })
    );

    // Get module config
    // Get form config and hide/show form parts
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        this.instancia = config;
        this.modulos = config.modulos;
      })
    );
  }

  /**
   * Logout menu option
   */
  onLogout() {
    this.loading.loading();
    this.auth.signOut();
  }

  /**
   * turn to onLoading state
   */
  onLoading($event) {
    if (!this.router.isActive($event.srcElement.pathname, true)) {
      this.loading.loading();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
