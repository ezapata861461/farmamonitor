# Dashboard Module

Dashboard module for Angular App.

## Install

Include module into an angular app as a subtree like this (Must be in app root folder. Right outside src folder.):

```
git remote add dashboard https://JabbarSahid@bitbucket.org/webintegral/dashboard.git
git subtree add --prefix=src/app/dashboard dashboard master
```

## Pull or Push Changes from/to Repository

After a commit, push changes to this repository like this:

```
git subtree push --prefix=src/app/dashboard dashboard master
```

To pull changes to local app, use:

```
git subtree pull --prefix=src/app/dashboard dashboard master
```

To push changes in parent project, just do it as usual:

```
git push -u origin master
```