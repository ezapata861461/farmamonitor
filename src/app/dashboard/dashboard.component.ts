import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../core/shared/loading.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private loading: LoadingService
  ) { }

  ngOnInit() {
    this.loading.loaded();
  }

}
