# Changelog

## [0.0.2] - 2019-11-26

### Added
* Add missing dependencies.

## [0.0.1] - 2019-10-10

* First commit.