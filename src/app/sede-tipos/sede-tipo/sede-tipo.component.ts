import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { map } from "rxjs/operators";

import { SedeTipoService }  from "../shared/sede-tipo.service";
import { NotifyService } from 'src/app/core/shared/notify.service';
import { AuthService } from 'src/app/auth/shared/auth.service';
import { SedeTipoAuthoService } from '../shared/sede-tipo-autho.service';

@Component({
  selector: 'app-sede-tipo',
  templateUrl: './sede-tipo.component.html',
  styleUrls: ['./sede-tipo.component.scss']
})
export class SedeTipoComponent implements OnInit, OnDestroy {

  // Route slug
  slug: string = 'sede-tipos';
  id: string = '0';

  // Sidebar menu definition
  sidebar = [
    { name: 'Tipos de Sede', icon: 'arrow_back', link: '../', disabled: true, allow: this.sedeTipoAutho },
    { name: 'Detalles', icon: 'create', link: './detalles' , disabled: true, allow: this.sedeTipoAutho }
  ];

  // Subscriptions
  subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private sedeTipoAutho: SedeTipoAuthoService,
    private mainService: SedeTipoService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    // Watch id parameter changes in route
    this.subscriptions.push(
      combineLatest(this.route.params)
        .pipe(map(([params]) => params['id']))
        .subscribe(id => {
          this.id = id;
          this.mainService.id = id;
          this.setSidebar(id);
        })
    );

    // Navigate to list view if non existing document
    this.subscriptions.push(
      this.mainService.doc$.subscribe(doc => {
        if (undefined === doc) {
          this.notify.update('El registro solicitado no existe o ha sido borrado.', 'error');
          this.router.navigate([this.auth.instancia, this.slug]);
        }
      })
    );
  }

  /**
   * Sets sidebar permissions depending if creation or edition
   * @param id
   */
  setSidebar(id) {
    this.subscriptions.push(
      this.auth.user.subscribe(() => {
        for (let i = 0; i < this.sidebar.length; i++) {
          if ('0' === id) {
            if (i > 1) {
              this.sidebar[i].disabled = true;
            } else {
              this.sidebar[i].disabled = !this.sidebar[i].allow.canRead;
            }
          } else {
            this.sidebar[i].disabled = !this.sidebar[i].allow.canRead;
          }
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
