import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeTipoComponent } from './sede-tipo.component';

describe('SedeTipoComponent', () => {
  let component: SedeTipoComponent;
  let fixture: ComponentFixture<SedeTipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeTipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeTipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
