import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeTipoGridComponent } from './sede-tipo-grid.component';

describe('SedeTipoGridComponent', () => {
  let component: SedeTipoGridComponent;
  let fixture: ComponentFixture<SedeTipoGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeTipoGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeTipoGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
