import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeTipoListComponent } from './sede-tipo-list.component';

describe('SedeTipoListComponent', () => {
  let component: SedeTipoListComponent;
  let fixture: ComponentFixture<SedeTipoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeTipoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeTipoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
