import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth/shared/auth.service';
import { SedeTipoAuthoService } from '../shared/sede-tipo-autho.service';
import { LoadingService } from '../../core/shared/loading.service';

@Component({
  selector: 'app-sede-tipo-list',
  templateUrl: './sede-tipo-list.component.html',
  styleUrls: ['./sede-tipo-list.component.scss']
})
export class SedeTipoListComponent {

  // Route slug
  slug: string = 'sede-tipos';

  // Sidebar menu
  sidebar = [
    { name: 'Escritorio', icon: 'arrow_back', link: '../admin', disabled: false },
    { name: 'Lista de Tipos de Sede', icon: 'turned_in_not', link: './' , disabled: false }
  ]

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: SedeTipoAuthoService,
    private loading: LoadingService
  ) { }

  /**
   * Grid create event handler
   * @param $event 
   */
  onCreate() {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, 0]);
  }

  /**
   * Grid row click event handler
   * @param $event 
   */
  onSelect($event) {
    this.loading.loading();
    this.router.navigate([this.auth.instancia, this.slug, $event.id]);
  }
}
