import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeTipoSelectComponent } from './sede-tipo-select.component';

describe('SedeTipoSelectComponent', () => {
  let component: SedeTipoSelectComponent;
  let fixture: ComponentFixture<SedeTipoSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeTipoSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeTipoSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
