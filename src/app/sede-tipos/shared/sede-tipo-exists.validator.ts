import { map, take, debounceTime } from "rxjs/operators";
import { FormGroup, AbstractControl } from "@angular/forms";
import { AngularFirestore } from "@angular/fire/firestore";
import * as  _  from "lodash";

import { AuthService } from 'src/app/auth/shared/auth.service';
import { SedeTipoId } from "../shared/sede-tipo";

/**
 * Validates 'persona' is unique by email
 */
export class SedeTipoExistsValidator {
  static tipo(auth: AuthService, afs: AngularFirestore, form: FormGroup) {
    return (control: AbstractControl) => {
      const tipo = _.trim(control.value);
      return afs.collection('sedeTipos', ref => ref.where('tipo', '==', tipo).where('instancias', 'array-contains', auth.instancia))
        .snapshotChanges().pipe(
          debounceTime(500),
          take(1),
          map(items => {
            let data = items.map(a => {
              const data = a.payload.doc.data() as SedeTipoId;
              const id = a.payload.doc.id;
              return { id, ...data };
            });

            if (data.length && '' !== form.value.tipo && (0 === form.value.id || data[0].id !== form.value.id)) {
              return { sedeTipoExists: data[0].tipo };
            }
            return null;
          })
        );
    }
  }
}