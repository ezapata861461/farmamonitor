import { TestBed } from '@angular/core/testing';

import { SedeTipoService } from './sede-tipo.service';

describe('SedeTipoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SedeTipoService = TestBed.get(SedeTipoService);
    expect(service).toBeTruthy();
  });
});
