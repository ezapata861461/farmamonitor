import { TestBed } from '@angular/core/testing';

import { SedeTipoAuthoService } from './sede-tipo-autho.service';

describe('SedeTipoAuthoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SedeTipoAuthoService = TestBed.get(SedeTipoAuthoService);
    expect(service).toBeTruthy();
  });
});
