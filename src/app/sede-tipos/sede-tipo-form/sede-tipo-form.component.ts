import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';

import { AbsFormComponent } from '../../core/abstract/abs-form.component';
import { SedeTipoAuthoService } from "../shared/sede-tipo-autho.service";
import { SedeTipoService } from "../shared/sede-tipo.service";
import { SedeTipoExistsValidator } from '../shared/sede-tipo-exists.validator';

import { ErrorLogService } from '../../core/shared/error-log.service';
import { NotifyService } from '../../core/shared/notify.service';
import { LoadingService } from '../../core/shared/loading.service';
import { AuthService } from '../../auth/shared/auth.service';

@Component({
  selector: 'win-sede-tipo-form',
  templateUrl: './sede-tipo-form.component.html',
  styleUrls: ['./sede-tipo-form.component.scss']
})
export class SedeTipoFormComponent extends AbsFormComponent implements OnInit, OnDestroy {
  // Base route
  slug: string = 'sedeTipos'

  // Aux Autho vars
  canCreateOrEdit: boolean = false;
  isSuperUser: boolean = false;
  isWebmaster: boolean = false;

  // Form parts show/hide flags
  parts = {
    instancias: false,
    registro: false
  }

  constructor(
    protected router: Router,
    public mainService: SedeTipoService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService,
    private afs: AngularFirestore,
    private fb: FormBuilder,
    private auth: AuthService,
    public autho: SedeTipoAuthoService
  ) {
    super(router, mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    // Init Autho vars
    this.canCreateOrEdit = (this.autho.canCreate && '0' === this.mainService.id) || (this.autho.canEdit && '0' !== this.mainService.id);
    this.isSuperUser = this.autho.hasRol(['webmaster', 'admin']);
    this.isWebmaster = this.autho.hasRol(['webmaster']);

    // Get form config and hide/show form parts
    this.subscriptions.push(
      this.auth.config.subscribe(config => {
        const conf = config.modulos['sedeTipos'].formParts;
        this.parts = {
          instancias: this.isWebmaster || conf.instancias,
          registro: this.isWebmaster || conf.registro
        }
      })
    );

    super.ngOnInit();
  }

  /**
   * Builds reactive form for component
   */
  buildForm() {
    return this.fb.group({
      id: ['0'],
      tipo: [{ value: '', disabled: !this.canCreateOrEdit }, [
        Validators.required
      ]],
      fCreado: [{ value: null, disabled: !this.isSuperUser }],
      creadorId: [{ value: '', disabled: !this.isSuperUser }],
      fModificado: [{ value: null, disabled: !this.isSuperUser }],
      modificadorId: [{ value: '', disabled: !this.isSuperUser }],
      instancia: [{ value: '', disabled: !this.isSuperUser }],
      instancias: this.fb.array([])
    });
  }

  /**
   * Load controls
   */
  loadControls(): void {}

  /**
   * Unsuscribe on destroy
   */
  ngOnDestroy() { super.ngOnDestroy() }

  /**
   * Populate form data
   * @param data 
   */
  populateForm(data) {
    if (data) {
      this.id = data.id;
      this.tipo = data.tipo;

      this.fCreado = data.fCreado;
      this.creadorId = data.creadorId;
      this.fModificado = data.fModificado;
      this.modificadorId = data.modificadorId;

      this.instancia = data.instancia;
      
      this.instancias.clear();
      if (data.instancias && data.instancias.length > 0) {
        data.instancias.forEach(element => {
          this.addInstancia(element);
        });
      }
    }

    this.onLoaded();
  }

  /**
   * Form getters and Setters
   */
  get tipo() {
    return this.form.get('tipo');
  }

  set tipo(tipo) {
    this.tipo.setAsyncValidators(SedeTipoExistsValidator.tipo(this.auth, this.afs, this.form));
    this.tipo.setValue(tipo);
  }

  get instancia() {
    return this.form.get('instancia');
  }

  set instancia(instancia) {
    this.instancia.setValue(instancia);
  }

  get instancias(): FormArray {
    return this.form.get('instancias') as FormArray;
  }

  addInstancia(instancia = '') {
    const element = this.fb.group({
      instancia: [instancia, [
        Validators.required
      ]]
    });
    this.instancias.push(element);
  }

  deleteInstancia(i) {
    this.instancias.removeAt(i);
    this.form.markAsTouched();
  }
}
