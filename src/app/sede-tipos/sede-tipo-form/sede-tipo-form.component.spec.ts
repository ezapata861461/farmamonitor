import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeTipoFormComponent } from './sede-tipo-form.component';

describe('SedeTipoFormComponent', () => {
  let component: SedeTipoFormComponent;
  let fixture: ComponentFixture<SedeTipoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeTipoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeTipoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
