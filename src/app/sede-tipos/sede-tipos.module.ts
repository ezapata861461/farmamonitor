import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule }      from "@angular/forms";

import { SedeTiposRoutingModule } from './sede-tipos-routing.module';
import { SedeTiposComponent } from './sede-tipos.component';
import { SedeTipoComponent } from './sede-tipo/sede-tipo.component';
import { SedeTipoDetailsComponent } from './sede-tipo-details/sede-tipo-details.component';
import { SedeTipoFormComponent } from './sede-tipo-form/sede-tipo-form.component';
import { SedeTipoGridComponent } from './sede-tipo-grid/sede-tipo-grid.component';
import { SedeTipoListComponent } from './sede-tipo-list/sede-tipo-list.component';
import { SedeTipoSelectComponent } from './sede-tipo-select/sede-tipo-select.component';

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { InstanciasModule } from '../instancias/instancias.module';

import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { UsuarioGridComponent } from '../usuarios/usuario-grid/usuario-grid.component';
import { InstanciaGridComponent } from '../instancias/instancia-grid/instancia-grid.component';

@NgModule({
  declarations: [
    SedeTiposComponent, 
    SedeTipoComponent, 
    SedeTipoDetailsComponent, 
    SedeTipoFormComponent, 
    SedeTipoGridComponent, 
    SedeTipoListComponent, 
    SedeTipoSelectComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    SedeTiposRoutingModule,
    CoreModule,
    SharedModule,
    AuthModule,
    UsuariosModule,
    InstanciasModule
  ],
  exports: [
    SedeTipoGridComponent,
    SedeTipoFormComponent,
    SedeTipoSelectComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    UsuarioGridComponent,
    InstanciaGridComponent
  ]
})
export class SedeTiposModule { }
