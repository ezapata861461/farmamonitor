# SedeTipos Module

SedeTipos module for Angular App.

## Install

Include module into an angular app as a subtree like this (Must be in app root folder. Right outside src folder.):

```
git remote add sede-tipos https://JabbarSahid@bitbucket.org/webintegral/sede-tipos.git
git subtree add --prefix=src/app/sede-tipos sede-tipos master
```

## Pull or Push Changes from/to Repository

After a commit, push changes to this repository like this:

```
git subtree push --prefix=src/app/sede-tipos sede-tipos master
```

To pull changes to local app, use:

```
git subtree pull --prefix=src/app/sede-tipos sede-tipos master
```

To push changes in parent project, just do it as usual:

```
git push -u origin master
```