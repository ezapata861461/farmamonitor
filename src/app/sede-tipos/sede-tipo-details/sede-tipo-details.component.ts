import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { SedeTipoAuthoService } from "../shared/sede-tipo-autho.service";
import { AuthService } from 'src/app/auth/shared/auth.service';

@Component({
  selector: 'app-sede-tipo-details',
  templateUrl: './sede-tipo-details.component.html',
  styleUrls: ['./sede-tipo-details.component.scss']
})
export class SedeTipoDetailsComponent {

  // Route slug
  slug: string = 'sede-tipos';

  constructor(
    private router: Router,
    private auth: AuthService,
    public autho: SedeTipoAuthoService,
  ) { }

  /**
   * On form creation event
   * @param $event 
   */
  onCreated($event) {
    this.router.navigate([this.auth.instancia, this.slug, $event]);
  }

  /**
   * On form update event
   * @param $event 
   */
  onUpdated($event) {}

  /**
   * On form delete event
   * @param $event 
   */
  onDeleted() {
    this.router.navigate([this.auth.instancia, this.slug]);
  }
}
