import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeTipoDetailsComponent } from './sede-tipo-details.component';

describe('SedeTipoDetailsComponent', () => {
  let component: SedeTipoDetailsComponent;
  let fixture: ComponentFixture<SedeTipoDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeTipoDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeTipoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
