import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedeTiposComponent } from './sede-tipos.component';

describe('SedeTiposComponent', () => {
  let component: SedeTiposComponent;
  let fixture: ComponentFixture<SedeTiposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedeTiposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedeTiposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
