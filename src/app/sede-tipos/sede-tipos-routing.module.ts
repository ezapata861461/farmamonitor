import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SedeTiposComponent } from "./sede-tipos.component";
import { SedeTipoListComponent } from "./sede-tipo-list/sede-tipo-list.component";
import { SedeTipoComponent } from "./sede-tipo/sede-tipo.component";
import { SedeTipoDetailsComponent } from "./sede-tipo-details/sede-tipo-details.component";

import { RoleGuard } from './shared/role.guard';

const routes: Routes = [
  {
    path: '',
    component: SedeTiposComponent,
    canActivate: [RoleGuard],
    children: [
      {
        path: '',
        component: SedeTipoListComponent,
      },
      {
        path: ':id',
        component: SedeTipoComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles',
          },
          {
            path: 'detalles',
            component: SedeTipoDetailsComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SedeTiposRoutingModule { }
