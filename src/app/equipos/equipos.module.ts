import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** componentes utilizable en cualquier componente */
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";
import { ReactiveFormsModule }      from "@angular/forms";

/** */

import { EquiposRoutingModule } from './equipos-routing.module';
import { EquiposComponent } from './equipos.component';
import { EquipoComponent } from './equipo/equipo.component';

/** componentes de otros modulos, abstrat, para manejo de usu,auth,instacias */

import { CoreModule } from "../core/core.module";
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { InstanciasModule } from '../instancias/instancias.module';

import { ConfirmDialogComponent } from '../core/confirm-dialog/confirm-dialog.component';
import { UsuarioGridComponent } from '../usuarios/usuario-grid/usuario-grid.component';
import { InstanciaGridComponent } from '../instancias/instancia-grid/instancia-grid.component';
import { EquipoDetailsComponent } from './equipo-details/equipo-details.component';
import { EquipoFormComponent } from './equipo-form/equipo-form.component';
import { EquipoGridComponent } from './equipo-grid/equipo-grid.component';
import { EquipoListComponent } from './equipo-list/equipo-list.component';
import { EquipoSelectComponent } from './equipo-select/equipo-select.component';


@NgModule({
  declarations: [EquiposComponent, EquipoComponent, EquipoDetailsComponent, EquipoFormComponent, EquipoGridComponent, EquipoListComponent, EquipoSelectComponent],
  imports: [
    CommonModule,
    EquiposRoutingModule,
    /**desde aca */
    FlexLayoutModule,
    AppMaterialModule,
    ReactiveFormsModule,
    CoreModule,
    SharedModule,
    AuthModule,
    UsuariosModule,
    InstanciasModule
  ],
  exports:[
  
  ],

  /** componentes de entrada */
  
  entryComponents: [
    ConfirmDialogComponent,
    UsuarioGridComponent,
    InstanciaGridComponent
  ]
})
export class EquiposModule { }
