import { TestBed } from '@angular/core/testing';

import { EquipoAuthoService } from './equipo-autho.service';

describe('EquipoAuthoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EquipoAuthoService = TestBed.get(EquipoAuthoService);
    expect(service).toBeTruthy();
  });
});
