/**representa un equipo instalado en alguna parte */

export interface Equipo { 
    /**seraila del procesador del equipo*/
    serial: string;   
}

export interface EquipoId extends Equipo {
    id: string;
}