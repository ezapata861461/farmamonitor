import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

import { AbsGridComponent } from "../../core/abstract/abs-grid.component";
import { ConfirmDialogComponent } from "../../core/confirm-dialog/confirm-dialog.component";

import { EquipoAuthoService } from "../shared/equipo-autho.service";
import { EquipoService } from "../shared/equipo.service";
import { EquipoId } from "../shared/equipo";
import { ErrorLogService } from 'src/app/core/shared/error-log.service';
import { NotifyService } from 'src/app/core/shared/notify.service';
import { LoadingService } from 'src/app/core/shared/loading.service';

@Component({
  selector: 'win-equipo-grid',
  templateUrl: './equipo-grid.component.html',
  styleUrls: ['./equipo-grid.component.scss']
})
export class EquipoGridComponent extends AbsGridComponent implements OnInit, OnDestroy {

  // Mat table definition
  dataSource: MatTableDataSource<EquipoId>;
  selection = new SelectionModel<EquipoId>(true, []);

  // Selected ítems emmiter
  @Output() 
  select = new EventEmitter<EquipoId[] | EquipoId>();

  constructor(
    public mainService: EquipoService,
    public autho: EquipoAuthoService,
    protected confirm: MatDialog,
    protected errorLog: ErrorLogService,
    protected notify: NotifyService,
    protected loading: LoadingService
  ) {
    super(mainService, confirm, errorLog, notify, loading);
  }

  ngOnInit() {
    super.ngOnInit();

    // Mat table column definition
    this.columns = [ 'mobileView', 'serial' ];

    // Setup main service
    this.mainService.pageSize = 50;
    if (null === this.mainService.sort) { this.mainService.sort = 'serial' }
    if (null === this.mainService.sortDir) { this.mainService.sortDir = 'asc' }
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  /**
   * Set grid filter
   * @param filter 
   */
  setFilter(filter) {
   
  }

  /**
   * Delete button click handler
   */
  onDelete() {
    const confirm = this.confirm.open(ConfirmDialogComponent, {
      width: '80%',
      data: {content: '¿Seguro que quieres borrar estas sedes?'},
    });

    this.subscriptions.push(
      confirm.afterClosed().subscribe(result => {
        if (result) {
          this.onLoading();
          this.mainService.deleteCollection(this.selection.selected)
            .then(x => {
              this.selection.clear();
              this.onLoaded('Borrado!');
              this.deleted.emit(true);
            })
            .catch(error => {
              this.onError(error, 'No fue posible borrar. Por favor intente de nuevo.');
              this.onLoaded();
            });
        }
      })
    );
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
