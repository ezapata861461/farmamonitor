import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoSelectComponent } from './equipo-select.component';

describe('EquipoSelectComponent', () => {
  let component: EquipoSelectComponent;
  let fixture: ComponentFixture<EquipoSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipoSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipoSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
