import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EquiposComponent } from "./equipos.component";
import { RoleGuard } from "./shared/role.guard";
import { EquipoListComponent } from "./equipo-list/equipo-list.component";
import { EquipoDetailsComponent } from "./equipo-details/equipo-details.component";

/**se copia desde aca */
const routes: Routes = [
  {
    path: '',
    component: EquiposComponent,
    canActivate: [RoleGuard],
    children: [
      {
        path: '',
        component: EquipoListComponent,
      },
      {
        path: ':id',
        component: EquiposComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles',
          },
          {
            path: 'detalles',
            component: EquipoDetailsComponent
          }
        ]
      }
    ]
  }
];
/**se copia hasta aca */

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquiposRoutingModule { }
