# Changelog

## [0.0.3] - 2019-11-26

* Clean code.

## [0.0.2] - 2019-11-21

### Added
* Layout for admin.

## [0.0.1] - 2019-10-10

* First commit.