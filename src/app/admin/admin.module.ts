import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppMaterialModule } from "../app-material/app-material.module";

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';

import { CoreModule } from "../core/core.module";


@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AppMaterialModule,
    AdminRoutingModule,
    CoreModule
  ]
})
export class AdminModule { }
