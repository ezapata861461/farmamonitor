import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { LoadingService } from "../core/shared/loading.service";
import { AuthService } from '../auth/shared/auth.service';
import { Roles } from '../usuarios/shared/roles';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  role: Roles;

  // Subscriptions
  userSub: Subscription;

  constructor(
    private loading: LoadingService,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.loading.loaded();
  } 

  signOut() {
    this.auth.signOut();
  }

  onLoading() {
    this.loading.loading();
  }

  onLoaded() {
    this.loading.loaded();
  }

  ngOnDestroy() {
    if (this.userSub) { this.userSub.unsubscribe() }
  }

}