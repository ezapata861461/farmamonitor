import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './core/not-found/not-found.component';
import { LoginComponent } from './auth/login/login.component';
import { InstanciaSwitchComponent } from './auth/instancia-switch/instancia-switch.component';
import { AuthGuard } from './auth/guard/auth.guard';
import { InstanciaGuard } from './auth/guard/instancia.guard';

/**
 * @todo: Crear EnabledGuard, que valide que el módulo esté activado antes de llevar a la ruta.
 */

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./inicio/inicio.module').then(m => m.InicioModule)
  },
  {
    path: 'instancia-switch',
    component: InstanciaSwitchComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: ':instancia/admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/empleados',
    loadChildren: () => import('./empleados/empleados.module').then(m => m.EmpleadosModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/empresas',
    loadChildren: () => import('./empresas/empresas.module').then(m => m.EmpresasModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/equipos',
    loadChildren: () => import('./equipos/equipos.module').then(m => m.EquiposModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/instancias',
    loadChildren: () => import('./instancias/instancias.module').then(m => m.InstanciasModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/medidas',
    loadChildren: () => import('./medidas/medidas.module').then(m => m.MedidasModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/personas',
    loadChildren: () => import('./personas/personas.module').then(m => m.PersonasModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/sedes',
    loadChildren: () => import('./sedes/sedes.module').then(m => m.SedesModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/sede-tipos',
    loadChildren: () => import('./sede-tipos/sede-tipos.module').then(m => m.SedeTiposModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: ':instancia/usuarios',
    loadChildren: () => import('./usuarios/usuarios.module').then(m => m.UsuariosModule),
    canActivate: [AuthGuard, InstanciaGuard]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes, 
    /** Fix problem with route params (idHilo,...) not loading in HusoModule 
     * @check https://github.com/angular/angular/issues/31778#issuecomment-513784646 
     */
    {paramsInheritanceStrategy: 'always'}
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
