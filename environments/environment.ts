// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBe4weR_bGdnA-7ooPs2o3wIcZvJg7juSo",
    authDomain: "farmamonitor-dev.firebaseapp.com",
    databaseURL: "https://farmamonitor-dev.firebaseio.com",
    projectId: "farmamonitor-dev",
    storageBucket: "farmamonitor-dev.appspot.com",
    messagingSenderId: "935675195895",
    appId: "1:935675195895:web:1caa75e6d9f196ca1d7ef2"

  },
  /** google Maps API */
  gmaps: {
    apiKey: ''
  },
/**where use is redirected is taken after login (by default) */
  defaultRedirects:{
    webmaster: 'admin',
    admin: 'admin',
    manager: 'dashbord',
    operations: 'dashbord',
    sales: 'dashbord',
    accounting: 'dashbord',
    client: 'dashbord',
    direct: 'dashbord',
    guest: 'dashbord',
  },
    /**Active Modules */
  modules: [
    { values: 'empresas', viewvalue:"Empresas", icon: '' },
    { values: 'instancias', viewvalue:"Instancias", icon: '' },
    { values: 'personas', viewvalue:"Personas", icon: '' },
    { values: 'sedesTipos', viewvalue:"Sede Tipos", icon: '' },
    { values: 'users', viewvalue:"Usuarios", icon: '' },
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrownpmn.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
