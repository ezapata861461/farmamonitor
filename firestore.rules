rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {

    match /errors/{error} {
      allow read, update: if isWebmaster();
      // @todo: Validate that type is Error before creation.
      allow create: if isAuthenticated() && request.resource.data;
    }

    match /empresas/{empresa} {
      allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
    }
    match /equipos/{equipo} {
      allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
    }
    match /personas/{instancia} {
      allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
    }

    match /sedes/{sede} {
      allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
    }

    match /sedeTipos/{sedeTipo} {
      allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
    }

    match /users/{uid} {
      // Regular user can only read it's own user.
      allow get: if isWebmaster() || isAdmin() || request.auth.uid == uid;
      allow list: if isAdmin() || isWebmaster();
      allow write: if isAdmin() || isWebmaster();
    }

    // ********* INSTANCIAS ********* //
    match /instancias/{instancia} {
      allow get: if (isAuthenticated() && userHasInstancia(instancia)) || isWebmaster();
      allow list: if isWebmaster();
      allow write: if isWebmaster();

      match /apis/{api} {
        allow read: if (isAuthenticated() && userHasInstancia(instancia)) || isWebmaster();
        allow write: if isWebmaster();
      }

      match /modulos/{modulo} {
        allow read: if (isAuthenticated() && userHasInstancia(instancia)) || isWebmaster();
        allow write: if isWebmaster();
      }

        // ********* NESTED COLLECTIONS ********* //
        match /errors/{error} {
          allow read, update: if isWebmaster();
          // @todo: Validate that type is Error before creation.
          allow create: if isAuthenticated() && request.resource.data;
        }

        match /empresas/{empresa} {
          allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
        }

        match /equipos/{equipo} {
          allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
        }
        match /personas/{instancia} {
          allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
        }

        match /sedes/{sede} {
          allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
        }

        match /sedeTipos/{sedeTipo} {
          allow read, write: if isWebmaster() || isAdmin() || isManager() || isOperations() || isSales() || isAccounting();
        }

        match /users/{uid} {
          // Regular user can only read it's own user.
          allow get: if isWebmaster() || isAdmin() || request.auth.uid == uid;
          allow list: if isAdmin() || isWebmaster();
          allow write: if isAdmin() || isWebmaster();
        }
        // ********* NESTED COLLECTIONS END ********* //
    }

    // ********* RULES END ********* //
    
    // Check if user has instancia
    function userHasInstancia(instancia) {
      return instancia in get(/databases/$(database)/documents/users/$(request.auth.uid)).data.instancias;
    }

    // Check if user han any of instancias
    // @todo: Test.
    // @check: https://firebase.google.com/docs/reference/rules/rules.List.html
    function userHasAnyInstancia(instancias) {
      return get(/databases/$(database)/documents/users/$(request.auth.uid)).data.instancias.hasAny(instancias)
    }

    function isAuthenticated() {
      return request.auth != null;
    }

    function isWebmaster() {
     return request.auth.token.webmaster == true;
    }

    function isAdmin() {
      return request.auth.token.admin == true;
    }

    function isManager() {
      return request.auth.token.manager == true;
    }

    function isOperations() {
      return request.auth.token.operations == true;
    }

    function isSales() {
      return request.auth.token.sales == true;
    }
    
    function isAccounting() {
      return request.auth.token.accounting == true;
    }
    
    function isClient() {
      return request.auth.token.client == true;
    }
    
    function isDirect() {
      return request.auth.token.direct == true;
    }
    
    function isGuest() {
      return request.auth.token.guest == true;
    }
  }
}