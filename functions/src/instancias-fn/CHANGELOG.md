# Changelog

## [0.0.6] - 2019-12-08

### Added
* Add api and module interfaces.
* Improve onDelete and onCreate functions.

### Changed
* Update instancia interface to latest model.

## [0.0.5] - 2019-11-21

### Changed
* Update instancia model.

## [0.0.4] - 2019-10-19

### Added
* Add woocommerce API to config.

## [0.0.3] - 2019-10-17

### Fixed
* Fix property names in getEmptyInstancia method.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.
* Add collection count increment on create.
* Add collection count decrement on delete.

### Changed
* Improve interface code and docs.

## [0.0.1] - 2019-10-10

* First commit.