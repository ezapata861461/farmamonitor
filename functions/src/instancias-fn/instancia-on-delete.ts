import * as functions from 'firebase-functions';
import { decrementCollectionCount } from '../shared-fn/collection-count-functions'

/**
 * Decrement count of 'instancias' in each instancia avaliable in app
 */
export const instanciaOnDelete = functions.firestore
.document('instancias/{instanciaId}').onDelete(async (snap, context) => {
  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Get 'instancias' count
  const instanciasCol = await admin.firestore().collection('instancias').get();
  const instancias = instanciasCol.docs;
  
  // Update instancia count on each instancia
  const instanciasArr: string[] = [];
  instancias.forEach(instanciaD => {
    instanciasArr.push(instanciaD.id);
  });
  return decrementCollectionCount(instanciasArr, 'instancias')
});
