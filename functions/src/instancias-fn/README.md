# Instancias Cloud Functions

Instancias cloud functions for Firebase.

## Install

Include module into a functions folder as a subtree like this (Must be in app root folder. Right outside functions folder.):

```
git remote add instancias-fn https://JabbarSahid@bitbucket.org/webintegral/instancias-fn.git
git subtree add --prefix=functions/src/instancias-fn instancias-fn master
```

## Pull or Push Changes from/to Repository

After a commit, push changes to this repository like this:

```
git subtree push --prefix=functions/src/instancias-fn instancias-fn master
```

To pull changes to local app, use:

```
git subtree pull --prefix=functions/src/instancias-fn instancias-fn master
```

To push changes in parent project, just do it as usual:

```
git push -u origin master
```