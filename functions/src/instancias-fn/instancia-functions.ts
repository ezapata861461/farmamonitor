import { Instancia } from "./shared/instancia";
import { Modulo } from './shared/modulo';

/**
 * Returns an empty instancia
 */
export function getEmptyInstancia(): Instancia {
  const instancia: Instancia = {
    nombre: 'Default',
    empresaId: '',
    sedeId: '',
    fCreado: null,
    creadorId: '',
    fModificado: null,
    modificadorId: ''
  }
  return instancia;
}

/**
 * Return an array with all the empty modules to create
 */
export function getBasicModules(): Array<Modulo> {
  return [
    {
      nombre: 'empresas',
      count: 0,
      enabled: true,
      formParts: {
        deprecated: false,
        instancias: false,
        interacciones: true,
        registro: true,
        relacion: true
      },
      strategy: 'separatedCollection',
      params: {}
    },
    {
      nombre: 'instancias',
      count: 0,
      enabled: true,
      formParts: {},
      strategy: 'none',
      params: {}
    },
    {
      nombre: 'personas',
      count: 0,
      enabled: true,
      formParts: {
        deprecated: false,
        instancias: false,
        interacciones: true,
        registro: true,
        relacion: true,
        tags: true
      },
      strategy: 'separatedCollection',
      params: {}
    },
    {
      nombre: 'sedeTipos',
      count: 0,
      enabled: true,
      formParts: {
        instancias: false,
        registro: true
      },
      strategy: 'separatedCollection',
      params: {}
    },
    {
      nombre: 'sedes',
      count: 0,
      enabled: true,
      formParts: {
        deprecated: false,
        instancias: false,
        interacciones: true,
        registro: true
      },
      strategy: 'separatedCollection',
      params: {}
    },
    {
      nombre: 'users',
      count: 0,
      enabled: true,
      formParts: {
        disable: true,
        instancias: false,
        password: true,
        registro: true,
        roles: true
      },
      strategy: 'none',
      params: {}
    },
  ]
}
