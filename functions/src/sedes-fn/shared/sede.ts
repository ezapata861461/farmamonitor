import * as admin from 'firebase-admin';
try { admin.initializeApp(); } catch(e) {};
import { OwnerAwareInterface } from "../../shared-fn/shared/ownerAwareInterface";
import { Telefono } from '../../shared-fn/shared/telefono';
import { SedeTipoId } from "../../sede-tipos-fn/shared/sede-tipo";

type Fecha = admin.firestore.Timestamp | admin.firestore.FieldValue | null;
type Coordenadas = admin.firestore.GeoPoint | null;
type TipoSede = "" | "cerrada" | "fisica" | "virtual";

/**
 * Representa una sede de una empresa
 */
export interface Sede extends OwnerAwareInterface {
  /** Altitud del punto */
  alt: number | null;
  /** Ciudad de la sede. */
  ciudad: string;
  /** @deprecated Coordenadas de la sede. (Reemplazado por punto). */
  coords: Coordenadas;
  /** Dirección de la sede. */
  direccion: string;
  /** @deprecated email de la sede. (Reemplazado por emails)*/
  email: string;
  /** Array con todos los emails de la empresa. */
  emails: Array<string>;
  /** Nombre de la Empresa asociada. */
  empresa: string;
  /** ID de la Empresa asociada. */
  empresaId: string;
  /** Fecha de la última compra recibida por esta empresa. */
  fUltimaCompra: Fecha;
  /** @deprecated Fecha de la última reserva hecha. (Reemplazado por fUltimaCompra) */
  fUltimaReserva: Fecha;
  /** Fecha de la última visita comercial realizada. */
  fUltimaVisita: Fecha;
  /** Nombre de la sede. */
  nombre: string;
  /** Observaciones generales sobre la sede. */
  observaciones: string;
  /** Si la sede es principal. */
  principal: boolean;
  /** GeoPoint de la sede */
  punto: Coordenadas;
  /** Array con los teléfonos de la sede. */
  telefonos: Array<Telefono>;
  /** @deprecated Tipo de sede. (Reemplazado por tipos.) */
  tipo: TipoSede;
  /** Array con los tipos de sede asociados. */
  tipos: Array<SedeTipoId>;
  /** Zoom en el mapa. */
  zoom: number;
  /** @deprecated Fecha de Creación. */
  creado: Fecha;
  /** @deprecated Id del usuario creador. */
  creador: string;
  /** @deprecated Fecha de de la última modificación */
  modificado: Fecha;
  /** @deprecated ID del último usuario que modificó. */
  modificador: string;
}

export interface SedeId extends Sede {
  id: string;
}
