import * as functions from 'firebase-functions';
import { Sede } from './shared/sede';
import { incrementCollectionCount } from '../shared-fn/collection-count-functions'

export const sedeOnCreate = functions.firestore
.document('sedes/{sedeId}').onCreate(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedes';
  const snapDoc = snap.data() as Sede;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return incrementCollectionCount(instancias, collectionName);
});

export const sedeSubcollectionOnCreate = functions.firestore
.document('instancias/{instanciaId}/sedes/{sedeId}').onCreate(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedes';
  const snapDoc = snap.data() as Sede;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return incrementCollectionCount(instancias, collectionName);
});

