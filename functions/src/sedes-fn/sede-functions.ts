import { EmpresaId } from "../empresas-fn/shared/empresa";
import { Sede } from "./shared/sede";

export function getEmptySede(empresa: EmpresaId|null = null) {
 
  const sede: Sede = {
    alt: null,
    ciudad: '',
    coords: null,
    direccion: '',
    /** @deprecated */
    email: '',
    emails: [],
    empresa: (null !== empresa) ? empresa.razonSocial : '',
    empresaId: (null !== empresa) ? empresa.id: '',
    fUltimaCompra: null,
    /** @deprecated */
    fUltimaReserva: null,
    fUltimaVisita: null,
    nombre: 'Principal',
    observaciones: 'Esta sede fué creada automáticamente al crear la empresa.',
    principal: true,
    punto: null,
    telefonos: [],
    tipo: '',
    tipos: [],
    zoom: 10,
    fCreado: (null !== empresa) ? empresa.fCreado : null,
    creadorId: (null !== empresa) ? empresa.creadorId : '',
    fModificado: (null !== empresa) ? empresa.fModificado : null,
    modificadorId: (null !== empresa) ? empresa.modificadorId : '',
    instancia: (null !== empresa) ? empresa.instancia : '',
    instancias: (null !== empresa) ? empresa.instancias : [],
    /** @deprecated */
    creado: null,
    /** @deprecated */
    creador: '',
    /** @deprecated */
    modificado: null,
    /** @deprecated */
    modificador: ''
  };

  return sede;
}
