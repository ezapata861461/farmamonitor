# Changelog

## [0.0.3] - 2019-12-08

### Added
* Add onCreate and onDelete functions for subcollection.
* Add function to build empty sede.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.
* Add collection count decrement on delete.

### Changed
* Improve interface code and docs.

## [0.0.1] - 2019-10-10

* First commit.