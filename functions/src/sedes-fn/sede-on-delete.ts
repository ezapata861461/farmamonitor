import * as functions from 'firebase-functions';
import { Sede } from './shared/sede';
import { decrementCollectionCount } from '../shared-fn/collection-count-functions'

export const sedeOnDelete = functions.firestore
.document('sedes/{sedeId}').onDelete(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedes';
  const snapDoc = snap.data() as Sede;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return decrementCollectionCount(instancias, collectionName);
});

export const sedeSubcollectionOnDelete = functions.firestore
.document('instancias/{instanciaId}/sedes/{sedeId}').onDelete(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedes';
  const snapDoc = snap.data() as Sede;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return decrementCollectionCount(instancias, collectionName);
});
