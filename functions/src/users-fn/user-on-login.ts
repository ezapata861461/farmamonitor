import * as functions from 'firebase-functions';
import { getRolesArray, grantRole } from "./shared/role-functions";
import { initApp } from "../init-fn/init";
import { User } from './shared/user';

/**
 * Update (or create) user on login and grant default roles
 */
export const userOnLogin = functions.https.onCall(async (data, context) => {
  // Validate data
  if (undefined === data.email || '' === data.email || undefined === data.uid || '' === data.uid) {
    return { error: "Malformed data." };
  }
  
  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Clean role and uid to avoid malicius data updates
  const cleanData: User = {
    displayName: data.displayName || '',
    photoURL: data.photoURL || '',
    lastLogin: admin.firestore.FieldValue.serverTimestamp()
  };

  // If users exists, update it.
  const userS = await admin.firestore().collection('users').doc(data.uid).get();
  if (userS.exists) {
    return userS.ref.update(cleanData);

  // If user doesn't exist, create it and set 'guest' role and 'default' instancia.
  } else {
    // Check if first user
    const email = data.email;
    const firstUserS = await admin.firestore().collection('users').get();
    const firstUser = firstUserS.empty;

    // Set uid, role, instancias and email for new user.
    cleanData.uid = data.uid;
    cleanData.roles = [ 'guest' ];
    cleanData.instancias = [ 'default' ];
    cleanData.email = email;

    // Set creation data (created by user, himself).
    cleanData.fCreado = admin.firestore.FieldValue.serverTimestamp();
    cleanData.creadorId = data.uid;
    cleanData.fModificado = admin.firestore.FieldValue.serverTimestamp();
    cleanData.modificadorId = data.uid;

    /**
     * ¡'users' collection must be empty for this to work! 
     * Make 'mario@webintegral.com.co' a 'webmaster' on first login.
     */
    if ('ezapata@effitech.co' === email && firstUser) {

      // Init app
      await initApp();

      // Create mario@webintegral.com.co user (first user)
      cleanData.roles = getRolesArray('webmaster');
      cleanData.instancias = [ 'default' ];
      await userS.ref.set(cleanData);

      // Grant mario@webintegral.com.co webmaster pemissions.
      return grantRole(email, 'webmaster').then(() => {
        return {
          result: `Request fullfilled! ${email} is now a webmaster.`
        }
      });
    
    // All other users
    } else {
      await userS.ref.set(cleanData);
      return grantRole(email, 'guest').then(() => {
        return {
          result: `Request fullfilled! ${email} is now a guest.`
        }
      });
    }
  }
});