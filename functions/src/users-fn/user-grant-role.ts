import * as functions from 'firebase-functions';
import { getRolesArray, grantRole } from "./shared/role-functions";

/**
 * Add role (array of roles) to user
 * Restricted to webmaster
 * 
 * * @todo: Reject if user is admin, but user that's being updated is webmaster
 */
export const userGrantRole = functions.https.onCall(async(data, context) => {
  // Allow this only to webmaster and admin role
  if (undefined !== context.auth && false === context.auth.token.webmaster && false === context.auth.token.admin) {
    return {
      error: "Solicitud no autorizada. El usuario no tiene suficientes privilegios."
    }
  }

  // Validate data
  if (undefined === data.email || '' === data.email || undefined === data.uid || '' === data.uid || undefined === data.role || '' === data.role) {
    return { error: "Malformed data." };
  }

  // Load data
  const uid = data.uid;
  const email = data.email;
  const role = data.role;

  // Do not allow an 'admin' user to upgrade any user to 'webmaster'.
  if (undefined !== context.auth && false === context.auth.token.webmaster && 'webmaster' === role) {
    return { error: "Solicitud no autorizada. Un usuario con rol 'admin' no puede convertir un usuario en 'webmaster'." };
  }

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Update roles on database
  const userR = await admin.firestore().collection('users').doc(uid);
  const roles = getRolesArray(role);
  await userR.update({roles: roles})

  // Gran roles on custom claims
  return grantRole(email, role).then(() => {
    return {
      result: `Request fullfilled! ${email} is now a ${role}.`
    }
  });
});


