import * as functions from 'firebase-functions';

/**
 * Change user password
 */
export const userChangePassword = functions.https.onCall(async(data, context) => {

  /**
   * @todo: Allow password update if user is owner
   * @todo: Reject if user is admin, but user that's being updated is webmaster
   */

  // Allow this only to webmaster and admin role
  if (undefined !== context.auth && false === context.auth.token.webmaster && false === context.auth.token.admin) {
    return {
      error: "Solicitud no autorizada. El usuario no tiene suficientes privilegios."
    }
  }

  // Validate data
  if (undefined === data.uid || '' === data.uid || undefined === data.password || '' === data.password) {
    return { error: "Malformed data." };
  }

  // Load data
  const uid = data.uid;
  const password = data.password;

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Kill all user tokens
  await admin.auth().revokeRefreshTokens(uid);

  // Update password
  return admin.auth().updateUser(uid, { password: password });
});