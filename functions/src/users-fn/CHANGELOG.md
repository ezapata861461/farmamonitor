# Changelog

## [0.0.6] - 2019-12-13

### Fixed
* Fix user role validation on userDisabledToggle, userGrantRole and userChangePassword to allow admin.

## [0.0.5] - 2019-11-18

### Changed
* Kill user tokens after disable, password change or role change.

## [0.0.4] - 2019-10-18

## Fixed
* Fix email not creating during user login.

## [0.0.3] - 2019-10-17

### Fixed
* Fix property names on userOnLogin function.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.
* Add collection decrement on delete.

### Changed
* Improve interface docs.

## [0.0.1] - 2019-10-10

* First commit.