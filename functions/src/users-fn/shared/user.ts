import * as admin from 'firebase-admin';
try { admin.initializeApp(); } catch(e) {};

type Fecha = admin.firestore.Timestamp | admin.firestore.FieldValue | null;

/**
 * Representa un usuario de la aplicación
 */
export interface User {
  /** User Id arrojado por el servicio de autenticación de firebase. */
  uid?: string;
  /** Usuario habilitado o deshabilitado. */
  disabled?: boolean;
  /** Nombre del usuario, del servicio de autenticación. */
  displayName?: string;
  /** Email, del servicio de autenticación. Null, en caso de autenticación anónima. */
  email?: string | null;
  /** URL de la foto del usuario (del servicio de autenticación). */
  photoURL?: string;
  /** Persona asociada al usuario. */
  personaId?: string;
  /** Array de roles para los que está autorizado el usuario. Esto permite consultar los usuarios por rol. */
  roles?: Array<string>;
  /** Fecha de último login. */
  lastLogin?: Fecha;

  /** Fecha de Creación. */
  fCreado?: Fecha;
  /** Id del usuario creador. */
  creadorId?: string;
  /** Fecha de de la última modificación */
  fModificado?: Fecha;
  /** ID del último usuario que modificó. */
  modificadorId?: string;
  /** Array con los Ids de las instancias a las que pertenece el documento. */
  instancias?: Array<string>;
}

export interface UserId extends User {
  id: string;
}
