/**
 * Adds required role (or roles) to user custom claims
 * @param email 
 * @param role 
 */
export async function grantRole(email: string, role: string = 'guest'): Promise<void> {
  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Get roles
  const roles = getRolesObject(role);

  // Update custom claims
  const user = await admin.auth().getUserByEmail(email);
  return admin.auth().setCustomUserClaims(user.uid, roles);
}

/**
 * Returns roles object by role name.
 * This is used to set roles in custom claims.
 * @param role 
 */
export function getRolesObject(role: string = 'guest'): Object {
  let roles: { [ s: string ]: boolean } = {};
  switch (role) {
    case 'webmaster':
      roles = { webmaster: true, admin: true, manager: true, operations: true, sales: true, accounting: true, client: true, direct: true, guest: true };
      break;
    case 'admin':
      roles = { webmaster: false, admin: true, manager: true, operations: true, sales: true, accounting: true, client: true, direct: true, guest: true };
      break;
    case 'manager':
      roles = { webmaster: false, admin: false, manager: true, operations: true, sales: true, accounting: true, client: true, direct: true, guest: true };
      break;
    case 'operations':
      roles = { webmaster: false, admin: false, manager: false, operations: true, sales: false, accounting: false, client: true, direct: true, guest: true };
      break;
    case'sales':
      roles = { webmaster: false, admin: false, manager: false, operations: false, sales: true, accounting: false, client: true, direct: true, guest: true };
      break;
    case 'accounting':
      roles = { webmaster: false, admin: false, manager: false, operations: false, sales: false, accounting: true, client: false, direct: false, guest: true };
      break;
    case 'client':
      roles = { webmaster: false, admin: false, manager: false, operations: false, sales: false, accounting: false, client: true, direct: false, guest: true };
      break;
    case 'direct':
      roles = { webmaster: false, admin: false, manager: false, operations: false, sales: false, accounting: false, client: false, direct: true, guest: true };
      break;
    case 'guest':
    default:
      roles = { webmaster: false, admin: false, manager: false, operations: false, sales: false, accounting: false, client: false, direct: false, guest: true };
      break;
  }
  return roles;
}

/**
 * Returns roles array by role name.
 * This is used to set database roles.
 * @param role 
 */
export function getRolesArray(role: string = 'guest'): Array<string> {
  let roles: Array<string> = [];
  switch (role) {
    case 'webmaster':
      roles = [ 'webmaster', 'admin', 'manager', 'operations', 'sales', 'accounting', 'client', 'direct', 'guest' ];
      break;
    case 'admin':
      roles = [ 'admin', 'manager', 'operations', 'sales', 'accounting', 'client', 'direct', 'guest' ];
      break;
    case 'manager':
      roles = [ 'manager', 'operations', 'sales', 'accounting', 'client', 'direct', 'guest' ];
      break;
    case 'operations':
      roles = [ 'operations', 'client', 'direct', 'guest' ];
      break;
    case'sales':
      roles = [ 'sales', 'client', 'direct', 'guest' ];
      break;
    case 'accounting':
      roles = [ 'accounting', 'guest' ];
      break;
    case 'client':
      roles = [ 'client', 'guest' ];
      break;
    case 'direct':
      roles = [ 'direct', 'guest' ];
      break;
    case 'guest':
    default:
      roles = [ 'guest' ];
      break;
  }
  return roles;
}