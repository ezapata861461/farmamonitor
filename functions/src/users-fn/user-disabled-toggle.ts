import * as functions from 'firebase-functions';

/**
 * Change user password
 */
export const userDisabledToggle = functions.https.onCall(async(data, context) => {

  /**
   * @todo: Allow password update if user is owner
   * @todo: Reject if user is admin, but user that's being updated is webmaster
   */

  // Allow this only to webmaster and admin roles
  if (undefined !== context.auth && false === context.auth.token.webmaster && false === context.auth.token.admin) {
    return {
      error: "Solicitud no autorizada. El usuario no tiene suficientes privilegios."
    }
  }

  // Validate data
  if (undefined === data.uid || '' === data.uid || undefined === data.disabled) {
    return { error: "Malformed data." };
  }

  // Load data
  const uid = data.uid;
  const disabled = !!data.disabled;

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Kill all user tokens
  await admin.auth().revokeRefreshTokens(uid);

  // Update disabled state
  await admin.auth().updateUser(uid, { disabled: disabled }).catch(e => {console.error(e)});
  return admin.firestore().collection('users').doc(uid).update({ disabled: disabled });
});