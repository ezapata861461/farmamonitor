import * as functions from 'firebase-functions';
import { User } from './shared/user';
import { decrementCollectionCount } from '../shared-fn/collection-count-functions'

/**
 * On user deletion
 */
export const userOnDelete = functions.firestore
.document('users/{userId}').onDelete(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'users';
  const snapD = snap.data() as User;
  const instancias = snapD.instancias ? snapD.instancias : [];

  // Add 1 to collection count
  decrementCollectionCount(instancias, collectionName).catch(e => {console.log(e)});

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Delete user from firebase auth
  admin.auth().deleteUser(snap.id).catch(e => {console.error(e)});
})