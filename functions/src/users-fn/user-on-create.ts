import * as functions from 'firebase-functions';
import { User } from './shared/user';
import { incrementCollectionCount } from '../shared-fn/collection-count-functions'
import { grantRole } from "./shared/role-functions";

export const userOnCreate = functions.firestore
.document('users/{userId}').onCreate(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'users';
  const snapD = snap.data() as User;
  const instancias = snapD.instancias ? snapD.instancias : [];

  // Add 1 to collection count
  incrementCollectionCount(instancias, collectionName).catch(e => {console.log(e)});

  // If user created by admin (not by login. Shouldn't have uid defined)
  if ((undefined === snapD.uid || null === snapD.uid || '' === snapD.uid) 
  && undefined !== snapD.email && null !== snapD.email && '' !== snapD.email) {
    // Load/init admin
    const admin = await import('firebase-admin');
    try { admin.initializeApp() } catch(e) {}

    try {
      // Create user in firebase auth
      const newUser = await admin.auth().createUser({ uid: snap.id, email: snapD.email });
      // If success, update uid and grant guest role (default)
      if (newUser.email) {
        await snap.ref.update({ uid: snap.id });
        await grantRole(newUser.email, 'guest');
      }
    } catch (error) {
      /**
       * @todo: What to do with this error?
       */
      console.error(error);
    }
  }
});
