# Changelog

## [0.0.4] - 2019-12-08

### Added
* Add onCreate and onDelete functions for subcollection.

## [0.0.3] - 2019-10-17

### Fixed
* Fix missing properties on 'sede principal' creation.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.
* Add collection count decrement on delete.

### Changed
* Improve interface docs.

## [0.0.1] - 2019-10-10

* First commit.