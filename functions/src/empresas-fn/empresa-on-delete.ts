import * as functions from 'firebase-functions';
import { Empresa } from './shared/empresa';
import { decrementCollectionCount } from '../shared-fn/collection-count-functions'

export const empresaOnDelete = functions.firestore
.document('empresas/{empresaId}').onDelete(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'empresas';
  const snapD = snap.data() as Empresa;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  decrementCollectionCount(instancias, collectionName).catch(error => { console.log(error) });

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Retrieve asociated sedes
  const sedesS = await admin.firestore().collection('sedes').where('empresaId', '==', snap.id).get();
  const sedes = sedesS.docs;

  // Delete asociated sedes
  const promises: Array<any> = [];
  sedes.forEach(sede => {
    promises.push(admin.firestore().doc(`sedes/${sede.id}}`).delete());
  })

  return Promise.all(promises);
});

export const empresaSubcollectionOnDelete = functions.firestore
.document('instancias/{instanciaId}/empresas/{empresaId}').onDelete(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'empresas';
  const snapD = snap.data() as Empresa;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  decrementCollectionCount(instancias, collectionName).catch(error => { console.log(error) });

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Retrieve asociated sedes
  const sedesS = await admin.firestore().collection(`instancias/${snapD.instancia}/sedes`).where('empresaId', '==', snap.id).get();
  const sedes = sedesS.docs;

  // Delete asociated sedes
  const promises: Array<any> = [];
  sedes.forEach(sede => {
    promises.push(admin.firestore().doc(`instancias/${snapD.instancia}/sedes/${sede.id}`).delete());
  })

  return Promise.all(promises);
});
