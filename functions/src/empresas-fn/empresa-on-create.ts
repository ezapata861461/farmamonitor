import * as functions from 'firebase-functions';
import * as _ from "lodash";
import { Empresa } from './shared/empresa';
import { incrementCollectionCount } from '../shared-fn/collection-count-functions'
import { getEmptySede } from '../sedes-fn/sede-functions';

export const empresaOnCreate = functions.firestore
.document('empresas/{empresaId}').onCreate(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'empresas';
  const snapD = snap.data() as Empresa;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  incrementCollectionCount(instancias, collectionName)
    .catch(error => { console.log(error) });

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Create sede principal
  const id = snap.id;
  const newSede = getEmptySede({id,... snapD});

  // Create new sede
  return admin.firestore().collection('sedes').add(newSede);
});

export const empresaSubcollectionOnCreate = functions.firestore
.document('instancias/{instanciaId}/empresas/{empresaId}').onCreate(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'empresas';
  const snapD = snap.data() as Empresa;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  incrementCollectionCount(instancias, collectionName)
    .catch(error => { console.log(error) });

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Create sede principal
  const id = snap.id;
  const newSede = getEmptySede({id,... snapD});

  // Create new sede
  return admin.firestore().collection(`instancias/${snapD.instancia}/sedes`).add(newSede);
});
