# Empresas Cloud Functions

Empresas cloud functions for Firebase.

## Install

Include module into a functions folder as a subtree like this (Must be in app root folder. Right outside functions folder.):

```
git remote add empresas-fn https://JabbarSahid@bitbucket.org/webintegral/empresas-fn.git
git subtree add --prefix=functions/src/empresas-fn empresas-fn master
```

## Pull or Push Changes from/to Repository

After a commit, push changes to this repository like this:

```
git subtree push --prefix=functions/src/empresas-fn empresas-fn master
```

To pull changes to local app, use:

```
git subtree pull --prefix=functions/src/empresas-fn empresas-fn master
```

To push changes in parent project, just do it as usual:

```
git push -u origin master
```