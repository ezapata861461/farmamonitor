import * as admin from 'firebase-admin';
try { admin.initializeApp(); } catch(e) {};
import { OwnerAwareInterface } from '../../shared-fn/shared/ownerAwareInterface';
import { Telefono } from '../../shared-fn/shared/telefono';

type Fecha = admin.firestore.Timestamp | admin.firestore.FieldValue | null;
type Cargo = "" | "administrativo"| "comercial" | "gerente" | "jefeOficina" | "operativo" | "internet" | "otro";
type Relacion = "" | "agente" | "directo" | "freelance" | "proveedor" | "otro";
type Shopping =  "" | "recent" | "repeat" | "lapsed" | "never";
type Subscription =  "" | "suscribed"| "unsubscribed" | "nonsubscribed" | "unwanted" | "cleaned";
type TipoDocumento = "" | "CC" | "CE" | "PP" | "RC" | "TI";

/**
 * Representa una persona de la aplicación
 */
export interface Persona extends OwnerAwareInterface {
  /** Apellidos de la persona. */
  apellido: string;
  /** Cargo de la persona dentro de su organización. */
  cargo: Cargo;
  /** Ciudad donde está ubicada la persona. */
  ciudad: string;
  /** Dirección (nomenclatura) de la persona. */
  direccion: string;
  /** Número de documento de la persona. */
  documento: string;
  /** Email principal de la persona. */
  email: string;
  /** Array con todos los emails adicionales de la persona. */
  emails: Array<string>;
  /** Nombre de la empresa asociada a la persona. */
  empresa: string;
  /** ID de la empresa asociada a la persona. */
  empresaId: string;
  /** Fecha de nacimiento. */
  fNacimiento: Fecha;
  /** Fecha de la última compra de la persona. */
  fUltimaCompra: Fecha;
  /** @deprecated Fecha de la última reserva hecha. (Reemplazado por fUltimaCompra) */
  fUltimaReserva: Fecha;
  /** Fecha de la última visita. */
  fUltimaVisita: Fecha;
  /** Número del móvil principal de la persona. */
  movil: string;
  /** Nombre de la persona. */
  nombre: string;
  /** Nombre y apellido concatenados (busquedas de texto full.) */
  nombreCompleto: string;
  /** Observaciones generales. */
  observaciones: string;
  /** Relación que se tiene con la persona. */
  relacion: Relacion;
  /** ID del usuario responsable de atender esta persona. */
  responsable: string;
  /** Nombre de la sede asociada a la persona. */
  sede: string;
  /** ID de la sede asociada a la persona. */
  sedeId: string;
  /** Comportamiento actual en cuanto a las compras. */
  shopping: Shopping;
  /** Estado de la subscripción. */
  subscription: Subscription;
  /** Fecha de la última sincronización (con otras plataformas). */
  synced: Fecha;
  /** Array con los Tags que describen el comportamiento y/o los intereses de la persona. */
  tags: Array<string>;
  /** Array con los demás teléfonos de la persona. */
  telefonos: Array<Telefono>;
  /** Tipo de documento de la persona */
  tipoDocumento: TipoDocumento;
  /** User ID del usuario asociado a la persona. */
  uid: string;

  /** @deprecated Fecha de Creación. */
  creado: Fecha;
  /** @deprecated Id del usuario creador. */
  creador: string;
  /** @deprecated Fecha de de la última modificación */
  modificado: Fecha;
  /** @deprecated ID del último usuario que modificó. */
  modificador: string;
}

export interface PersonaId extends Persona {
  id: string;
}
