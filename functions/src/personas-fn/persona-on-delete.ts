import * as functions from 'firebase-functions';
import { Persona } from './shared/persona';
import { decrementCollectionCount } from '../shared-fn/collection-count-functions'

export const personaOnDelete = functions.firestore
.document('personas/{personaId}').onDelete(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'personas';
  const snapD = snap.data() as Persona;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  return decrementCollectionCount(instancias, collectionName);
});

/**
 * for separatedCollection instancia strategy
 */
export const personaSubcollectionOnDelete = functions.firestore
.document('instancias/{instanciaId}/personas/{personaId}').onDelete(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'personas';
  const snapD = snap.data() as Persona;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  return decrementCollectionCount(instancias, collectionName);
});
