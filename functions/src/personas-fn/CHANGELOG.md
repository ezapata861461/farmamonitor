# Changelog

## [0.0.6] - 2019-12-13

### Fixed
* Fix onDelete trigger.

## [0.0.5] - 2019-12-08

### Added
* Add onDelete function for subcollection.

## [0.0.4] - 2019-11-29

### Added
* Add onCreate cloud function for nested collection (for separatedCollection instancia strategy).

## [0.0.3] - 2019-11-12

### Added
* Add nombre completo to personas interface.

## [0.0.2] - 2019-10-16

### Added
* Add backwards compatibility.
* Add collection count decrement on delete.

### Changed
* Improve interface code and docs.

## [0.0.1] - 2019-10-10

* First commit.