import * as functions from 'firebase-functions';
import { Persona } from './shared/persona';
import { incrementCollectionCount } from '../shared-fn/collection-count-functions'

export const personaOnCreate = functions.firestore
.document('personas/{personaId}').onCreate(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'personas';
  const snapD = snap.data() as Persona;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  return incrementCollectionCount(instancias, collectionName);
});

/**
 * for separatedCollection instancia strategy
 */
export const personaSubcollectionOnCreate = functions.firestore
.document('instancias/{instanciaId}/personas/{personaId}').onCreate(async (snap, context) => {
  // Retrieve document and its instancias array
  const collectionName = 'personas';
  const snapD = snap.data() as Persona;
  const instancias = snapD.instancias;

  // Add 1 to collection count
  return incrementCollectionCount(instancias, collectionName);
});
