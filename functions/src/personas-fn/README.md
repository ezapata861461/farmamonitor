# Personas Cloud Functions

Personas cloud functions for Firebase.

## Install

Include module into a functions folder as a subtree like this (Must be in app root folder. Right outside functions folder.):

```
git remote add personas-fn https://JabbarSahid@bitbucket.org/webintegral/personas-fn.git
git subtree add --prefix=functions/src/personas-fn personas-fn master
```

## Pull or Push Changes from/to Repository

After a commit, push changes to this repository like this:

```
git subtree push --prefix=functions/src/personas-fn personas-fn master
```

To pull changes to local app, use:

```
git subtree pull --prefix=functions/src/personas-fn personas-fn master
```

To push changes in parent project, just do it as usual:

```
git push -u origin master
```