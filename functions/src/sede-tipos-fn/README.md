# SedeTipos Cloud Functions

SedeTipos cloud functions for Firebase.

## Install

Include module into a functions folder as a subtree like this (Must be in app root folder. Right outside functions folder.):

```
git remote add sede-tipos-fn https://JabbarSahid@bitbucket.org/webintegral/sede-tipos-fn.git
git subtree add --prefix=functions/src/sede-tipos-fn sede-tipos-fn master
```

## Pull or Push Changes from/to Repository

After a commit, push changes to this repository like this:

```
git subtree push --prefix=functions/src/sede-tipos-fn sede-tipos-fn master
```

To pull changes to local app, use:

```
git subtree pull --prefix=functions/src/sede-tipos-fn sede-tipos-fn master
```

To push changes in parent project, just do it as usual:

```
git push -u origin master
```