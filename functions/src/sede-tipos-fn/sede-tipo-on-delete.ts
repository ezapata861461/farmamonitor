import * as functions from 'firebase-functions';
import { SedeTipo } from './shared/sede-tipo';
import { decrementCollectionCount } from '../shared-fn/collection-count-functions'

export const sedeTipoOnDelete = functions.firestore
.document('sedeTipos/{sedeTipoId}').onDelete(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedeTipos';
  const snapDoc = snap.data() as SedeTipo;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return decrementCollectionCount(instancias, collectionName);
});

export const sedeTipoSubcollectionOnDelete = functions.firestore
.document('instancias/{instanciaId}/sedeTipos/{sedeTipoId}').onDelete(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedeTipos';
  const snapDoc = snap.data() as SedeTipo;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return decrementCollectionCount(instancias, collectionName);
});
