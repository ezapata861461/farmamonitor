import * as functions from 'firebase-functions';
import { SedeTipo } from './shared/sede-tipo';
import { incrementCollectionCount } from '../shared-fn/collection-count-functions'

export const sedeTipoOnCreate = functions.firestore
.document('sedeTipos/{sedeTipoId}').onCreate(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedeTipos';
  const snapDoc = snap.data() as SedeTipo;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return incrementCollectionCount(instancias, collectionName);
});

export const sedeTipoSubcollectionOnCreate = functions.firestore
.document('instancias/{instanciaId}/sedeTipos/{sedeTipoId}').onCreate(async (snap, context) => {
  // Retrieve empresa and its instancias array
  const collectionName = 'sedeTipos';
  const snapDoc = snap.data() as SedeTipo;
  const instancias = snapDoc.instancias;

  // Add 1 to collection count
  return incrementCollectionCount(instancias, collectionName);
});
