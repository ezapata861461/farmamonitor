import { OwnerAwareInterface } from '../../shared-fn/shared/ownerAwareInterface';

/**
 * Representa el tipo de una sede.
 */
export interface SedeTipo extends OwnerAwareInterface {
  /** Tipo de sede. */
  tipo: string;
}

export interface SedeTipoId extends SedeTipo {
  id: string;
}
