# Changelog

## [0.0.2] - 2019-12-08

### Changed
* Update init function to add modules as subCollection.

## [0.0.1] - 2019-10-10

* First commit.