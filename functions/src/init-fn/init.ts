import { Instancia } from '../instancias-fn/shared/instancia';
import { getEmptyInstancia, getBasicModules } from '../instancias-fn/instancia-functions';

/**
 * Initialize App
 */
export async function initApp() {
  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  // Create first instancia.
  const emptyInstancia: Instancia = getEmptyInstancia();
  await admin.firestore().collection('instancias').doc('default').set(emptyInstancia);

  // Create basic modules
  const promises: Array<any> = [];
  const modulos = getBasicModules();
  modulos.forEach(modulo => {
    promises.push(admin.firestore().collection('instancias/default/modulos').doc(modulo.nombre).set(modulo));
  })
  return Promise.all(promises);
}