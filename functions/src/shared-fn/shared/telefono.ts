/**
 * Interface para un teléfono.
 */
export interface Telefono {
  /**
   * Número de teléfono
   */
  numero: string;

  /**
   * Tipo de teléfono: Móvil (movil), Fijo (fijo).
   */
  tipo: string;
}
