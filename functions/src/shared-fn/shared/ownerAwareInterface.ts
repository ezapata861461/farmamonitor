import * as admin from 'firebase-admin';
try { admin.initializeApp(); } catch(e) {};

type Fecha = admin.firestore.Timestamp | admin.firestore.FieldValue | null;

/**
 * Interface that contains data from user owner.
 * Intended to be exteneded by other interfaces.
 */
export interface OwnerAwareInterface {
  /** Fecha de Creación. */
  fCreado: Fecha;
  /** Id del usuario creador. */
  creadorId: string;
  /** Fecha de de la última modificación */
  fModificado: Fecha;
  /** ID del último usuario que modificó. */
  modificadorId: string;
  /** Id de la instancia asignada durante la creación. (Para lidiar con 'field' InstanciaStrategy) */
  /** @todo: Remove optional. */
  instancia?: string;
  /** Array con los Ids de las instancias a las que pertenece el documento. */
  instancias: Array<string>;
}
