import { Modulo } from '../instancias-fn/shared/modulo';

/**
 * Add 1 to collection count for each instancia in array
 * @param instancias 
 * @param collectionName 
 */
export async function incrementCollectionCount(instancias: Array<string>, collectionName: string) {
  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  const promises: Array<any> = [];
  instancias.forEach(async (instancia) => {
    const p = admin.firestore().runTransaction(transaction => {
      const collectionRef = admin.firestore().collection(`instancias/${instancia}/modulos`);
      const query = collectionRef.where('nombre', '==', collectionName);
      return transaction.get(query).then(collectionS => {
        const collectionD = collectionS.docs;
        if (collectionD.length > 0) {
          const moduloD = collectionD[0];
          const modulo = moduloD.data() as Modulo;
          modulo.count = modulo.count + 1;
          transaction.update(collectionRef.doc(moduloD.id), modulo);
        }
      })
    });
    promises.push(p);
  });
  return Promise.all(promises).catch(error => { console.log(error) });
}

/**
 * Extract 1 to collection count for each instancia in array
 * @param instancias 
 * @param collectionName 
 */
export async function decrementCollectionCount(instancias: Array<string>, collectionName: string) {
  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch(e) {}

  const promises: Array<any> = [];
  instancias.forEach(async (instancia) => {
    const p = admin.firestore().runTransaction(transaction => {
      const collectionRef = admin.firestore().collection(`instancias/${instancia}/modulos`);
      const query = collectionRef.where('nombre', '==', collectionName);
      return transaction.get(query).then(collectionS => {
        const collectionD = collectionS.docs;
        if (collectionD.length > 0) {
          const moduloD = collectionD[0];
          const modulo = moduloD.data() as Modulo;
          modulo.count = modulo.count - 1;
          transaction.update(collectionRef.doc(moduloD.id), modulo);
        }
      })
    });
    promises.push(p);
  });
  return Promise.all(promises).catch(error => { console.log(error) });
}
