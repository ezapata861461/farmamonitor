# App Base

App base para un proyecto de Angular. Se asume que ya se tiene instalado node.js, angular cli y git bash.

## Instalar

Clonar:

1. Crear un folder y meterse dentro (usar git bash):

```
cd /d/angular
mkdir nombreProjecto
cd nombreProyecto
```

2. Ejecutar el comando para clonar:

```
git clone https://JabbarSahid@bitbucket.org/webintegral/farm.git .
```

Remover el origen del proyecto y cambiarlo por un repositorio propio:

```
git remote rm origin
git remote add origin https://...
```

Para ver los proyectos de git vinculados, usar:

```
git remote -v
```

3. Instalar los módulos en la raíz de la aplicación y dentro del folder 'functions':

```
npm install
cd functions
npm install
```

Si no se tiene ya instalado o está desactualizado, instalar firebase CLI:

```
npm install -g firebase-tools
```

4. Si no se ha hecho, iniciar sesión en firebase-tools:

```
firebase login
```

5. Crear un alias para el proyecto de firebase (se asume que ya se tienen creados una cuenta y un proyecto en firebase).

```
firebase use --add
```

En Los alias existentes pueden verificarse con 

```
firebase use
```

Y pueden eliminarse con 

```
firebase use --unalias nombreDelAlias
```

Para seleccionar un alias:

```
firebase use nombreDelAlias
```

6. Crear una base de datos en firestore (ver consola de firebase). Hacer deploy de 'rules' y de los 'indexes':

```
firebase deploy --only firestore
```

7. Hacer deploy de las cloud functions (es neceario estar dentro del folder 'functions'):

```
cd functions
npm run-script build
firebase deploy --only functions
```

8. Habilitar los provedores de identidad 'Google' y email/password en firebase.

9. Cambiar en la línea 54 del archivo 'functions\src\users-fn\user-on-login.ts', por un email de gmail propio. Esto permite el registro automático de este usuario, durante el primer login, con un rol de 'webmaster' y crea una instancia 'default' para la aplicación.

10. Servir la aplicación y hacer el primer login con el usuario creado.

```
ng serve --open
```

10. Cuando vaya a hacerse deploy del hosting, en el archivo firebase.json, actualizar la línea 13, con el nombre de la carpeta en la que está el proyecto, así:

```
    "public": "dist/nombreCarpeta",
```

Esto para definir donde quedará el proyecto en el alojamiento.
